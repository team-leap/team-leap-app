<?xml version="1.0" encoding="UTF-8"?>
<project xmlns="http://maven.apache.org/POM/4.0.0"
         xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
         xsi:schemaLocation="http://maven.apache.org/POM/4.0.0 http://maven.apache.org/xsd/maven-4.0.0.xsd">
    <modelVersion>4.0.0</modelVersion>

    <groupId>de.teamleap</groupId>
    <artifactId>rocket-deployer-backend</artifactId>
    <version>1.0-SNAPSHOT</version>

    <properties>
        <maven.compiler.source>11</maven.compiler.source>
        <maven.compiler.target>11</maven.compiler.target>
        <project.build.sourceEncoding>UTF-8</project.build.sourceEncoding>

        <spring.boot.version>2.5.0</spring.boot.version>
        <spring-context.version>5.3.7</spring-context.version>

        <!-- define CSV list of numerical IDs in Maven settings.xml to prevent them being added to VCS -->
        <app.admincards></app.admincards>
        <app.admincards.addon></app.admincards.addon>
    </properties>

    <profiles>
        <profile>
            <id>local</id>
            <properties>
                <app.prod.flag>false</app.prod.flag>
                <!-- insert the local database uri jdbc:postgresql://localhost:5432/postgres?user=postgres&password= -->
                <app.database.local.uri></app.database.local.uri>
                <app.database.service.id></app.database.service.id>
                <app.admincards.addon>admincard</app.admincards.addon>
            </properties>
        </profile>
        <profile>
            <id>dev</id>
            <properties>
                <app.prod>false</app.prod>
                <app.database.local.uri></app.database.local.uri>
                <app.database.service.id>rocket-deployer-postgresql-db-dev</app.database.service.id>
                <app.admincards.addon>826147478973, 696960687585, 703551648429, 966815980377, 827117280577, 670586616598</app.admincards.addon>
            </properties>
        </profile>
        <profile>
            <id>prod</id>
            <properties>
                <app.prod>true</app.prod>
                <app.database.local.uri></app.database.local.uri>
                <app.database.service.id>rocket-deployer-postgresql-db</app.database.service.id>
                <app.admincards.addon>826147478973, 696960687585, 703551648429, 966815980377, 827117280577, 670586616598</app.admincards.addon>
            </properties>
        </profile>
    </profiles>

    <dependencies>
        <!-- Spring Boot Starter Web - includes Tomcat and Spring MVC -->
        <dependency>
            <groupId>org.springframework.boot</groupId>
            <artifactId>spring-boot-starter-web</artifactId>
            <version>${spring.boot.version}</version>
        </dependency>
        <!-- https://mvnrepository.com/artifact/org.springframework.boot/spring-boot-starter-validation -->
        <dependency>
            <groupId>org.springframework.boot</groupId>
            <artifactId>spring-boot-starter-validation</artifactId>
            <version>2.5.0</version>
        </dependency>
        <dependency>
            <groupId>org.springframework</groupId>
            <artifactId>spring-context</artifactId>
            <version>${spring-context.version}</version>
        </dependency>

        <dependency>
            <groupId>org.springframework</groupId>
            <artifactId>spring-websocket</artifactId>
            <version>5.3.0</version>
        </dependency>
        <dependency>
            <groupId>org.springframework</groupId>
            <artifactId>spring-messaging</artifactId>
            <version>5.3.0</version>
        </dependency>

        <dependency>
            <groupId>org.postgresql</groupId>
            <artifactId>postgresql</artifactId>
            <version>42.6.0</version>
        </dependency>

        <dependency>
            <groupId>org.json</groupId>
            <artifactId>json</artifactId>
            <version>20220924</version>
        </dependency>

        <dependency>
            <groupId>org.apache.logging.log4j</groupId>
            <artifactId>log4j-slf4j-impl</artifactId>
            <version>2.17.2</version>
        </dependency>

        <!-- https://mvnrepository.com/artifact/org.junit.jupiter/junit-jupiter-engine -->
        <dependency>
            <groupId>org.junit.jupiter</groupId>
            <artifactId>junit-jupiter-engine</artifactId>
            <version>5.9.3</version>
            <scope>test</scope>
        </dependency>
        <dependency>
            <groupId>io.zonky.test</groupId>
            <artifactId>embedded-postgres</artifactId>
            <version>2.0.3</version>
            <scope>test</scope>
        </dependency>
    </dependencies>

    <build>
        <finalName>rocket-deployer-backend</finalName>

        <resources>
            <resource>
                <directory>src/main/resources</directory>
                <includes>
                    <include>**/*.properties</include>
                </includes>
                <filtering>true</filtering>
            </resource>

            <resource>
                <directory>src/main/resources</directory>
                <includes>
                    <include>**/*.sql</include>
                </includes>
                <filtering>false</filtering>
            </resource>
        </resources>

        <plugins>
            <plugin>
                <groupId>org.apache.maven.plugins</groupId>
                <artifactId>maven-surefire-plugin</artifactId>
                <version>3.0.0</version>
            </plugin>

            <!-- Spring Boot Maven Plugin - creates an executable JAR with all dependencies -->
            <plugin>
                <groupId>org.springframework.boot</groupId>
                <artifactId>spring-boot-maven-plugin</artifactId>
                <version>${spring.boot.version}</version>
                <executions>
                    <execution>
                        <goals>
                            <goal>repackage</goal>
                        </goals>
                    </execution>
                </executions>
                <configuration>
                    <mainClass>de.teamleap.AppEntryPoint</mainClass>
                </configuration>
            </plugin>
            <plugin>
                <groupId>org.apache.maven.plugins</groupId>
                <artifactId>maven-compiler-plugin</artifactId>
                <configuration>
                    <source>11</source>
                    <target>11</target>
                </configuration>
            </plugin>
        </plugins>
    </build>

</project>
