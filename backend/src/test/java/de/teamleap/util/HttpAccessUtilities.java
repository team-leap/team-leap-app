package de.teamleap.util;

import java.net.URI;
import java.net.http.HttpClient;
import java.net.http.HttpRequest;
import java.net.http.HttpResponse;
import java.util.Map;

public abstract class HttpAccessUtilities {

    public static String makeGETRequestToLocalhost8080(String path) {
        return makeGETRequestToLocalhost8080(path, Map.of());
    }

    public static String makeGETRequestToLocalhost8080(String path, Map<String, String> headers) {
        final String[] headerArray = new String[headers.size() * 2];
        int i = 0;
        for (Map.Entry<String, String> entry : headers.entrySet()) {
            headerArray[i++] = entry.getKey();
            headerArray[i++] = entry.getValue();
        }

        final HttpClient client = HttpClient.newHttpClient();
        final HttpRequest.Builder builder = HttpRequest.newBuilder()
                .uri(URI.create("http://localhost:8080" + (path.startsWith("/") ? path : "/" + path)));
        if (headerArray.length > 0) {
            builder.headers(headerArray);
        }
        final HttpRequest request = builder.build();

        try {
            final HttpResponse<String> response = client.send(request, HttpResponse.BodyHandlers.ofString());
            return response.body();
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    public static String makePOSTRequestToLocalhost8080(String path, String body, Map<String, String> headers) {
        final String[] headerArray = new String[headers.size() * 2];
        int i = 0;
        for (Map.Entry<String, String> entry : headers.entrySet()) {
            headerArray[i++] = entry.getKey();
            headerArray[i++] = entry.getValue();
        }

        final HttpClient client = HttpClient.newHttpClient();
        final HttpRequest.Builder builder = HttpRequest.newBuilder()
                .uri(URI.create("http://localhost:8080" + (path.startsWith("/") ? path : "/" + path)))
                .POST(HttpRequest.BodyPublishers.ofString(body));
        if (headerArray.length > 0) {
            builder.headers(headerArray);
        }
        final HttpRequest request = builder.build();

        try {
            final HttpResponse<String> response = client.send(request, HttpResponse.BodyHandlers.ofString());
            return response.body();
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

}
