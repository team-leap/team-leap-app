package de.teamleap.util;

import de.teamleap.config.RDConfig;
import de.teamleap.connection.db.DataSourceDatabaseConnectionProvider;
import de.teamleap.connection.db.RDDatabaseHandler;
import io.zonky.test.db.postgres.embedded.EmbeddedPostgres;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.sql.DataSource;
import java.io.IOException;
import java.sql.Connection;
import java.sql.SQLException;

public abstract class EmbeddedPostgresDbProvider {

    private static final Logger LOG = LoggerFactory.getLogger(EmbeddedPostgresDbProvider.class);

    private static EmbeddedPostgres epg;

    public static void registerEmbeddedDb() {
        if (initializeEmbeddedDb()) {
            EmbeddedPostgresDbProvider.registerEmbeddedDb(epg);
        }
    }

    public static boolean initializeEmbeddedDb() {
        if (epg == null) {
            LOG.info("Creating EmbeddedPostgres instance");
            try {
                epg = pg();
            } catch (IOException e) {
                throw new RuntimeException("Could not create EmbeddedPostgres instance", e);
            }
            Runtime.getRuntime().addShutdownHook(new Thread(EmbeddedPostgresDbProvider::afterAllTests));
            LOG.info("EmbeddedPostgres instance created");
            return true;
        }
        return false;
    }

    public static void resetEmbeddedDb() {
        if (epg == null) {
            LOG.warn("JUnit test not started yet!");
            return;
        }
        LOG.info("Resetting EmbeddedPostgres instance");

        try {
            epg.getPostgresDatabase().getConnection().createStatement().execute("DROP SCHEMA public CASCADE");
            epg.getPostgresDatabase().getConnection().createStatement().execute("CREATE SCHEMA public");
        } catch (SQLException e) {
            throw new RuntimeException("Could not reset EmbeddedPostgres instance", e);
        }

        LOG.info("EmbeddedPostgres instance reset");
    }

    private static EmbeddedPostgres pg() throws IOException {
        final EmbeddedPostgres.Builder builder = EmbeddedPostgres.builder();
        return builder.start();
    }

    private static void registerEmbeddedDb(EmbeddedPostgres epg) {
        final DataSource database = epg.getDatabase("postgres", "postgres");
        RDDatabaseHandler.setConnectionProvider(new DataSourceDatabaseConnectionProvider(database));
    }

    public static Connection getConnection() throws SQLException {
        return RDDatabaseHandler.getConnectionProvider().connection();
    }

    public static void afterAllTests() {
        if (epg == null) {
            LOG.warn("JUnit test not started yet!");
            return;
        }
        LOG.info("Shutting down EmbeddedPostgres instance");
        try {
            epg.close();
        } catch (IOException e) {
            throw new AssertionError(e);
        }
        epg = null;
        LOG.info("EmbeddedPostgres instance shut down");
    }
}

