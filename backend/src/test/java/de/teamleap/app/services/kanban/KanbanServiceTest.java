package de.teamleap.app.services.kanban;

import de.teamleap.app.MainController;
import de.teamleap.config.RDConfig;
import de.teamleap.util.EmbeddedPostgresDbProvider;
import de.teamleap.util.HttpAccessUtilities;
import io.zonky.test.db.postgres.embedded.EmbeddedPostgres;
import org.json.JSONObject;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.boot.SpringApplication;
import org.springframework.context.ConfigurableApplicationContext;

import java.util.Map;

class KanbanServiceTest {

    @BeforeAll
    public static void beforeAll() {
        EmbeddedPostgresDbProvider.registerEmbeddedDb();
        RDConfig.setIsTestCase(true, EmbeddedPostgres.class);
    }

    @BeforeEach
    public void beforeEach() {
        EmbeddedPostgresDbProvider.resetEmbeddedDb();
    }

    @Test
    public void kanbanBoardIntegrationTest() {
        try (ConfigurableApplicationContext ignored = SpringApplication.run(MainController.class)) {

            final String createUserRepoResponse = HttpAccessUtilities.makePOSTRequestToLocalhost8080("auth/user/create", new JSONObject().put("username", "name").put("password", "pwd").toString(), Map.of());
            assertSuccessResponse(createUserRepoResponse);

            final String userTokenResponse = HttpAccessUtilities.makePOSTRequestToLocalhost8080("/auth/user/token/create", new JSONObject().put("username", "name").put("password", "pwd").toString(), Map.of());
            assertSuccessResponse(userTokenResponse);
            final String token = new JSONObject(userTokenResponse).getString("token");

            JSONObject kanbanRow = new JSONObject();
            kanbanRow.put("title", "Test Kanban Title");
            kanbanRow.put("description", "Test Kanban Description");
            kanbanRow.put("priority", 0);
            kanbanRow.put("status", 0);

            String createResponse = HttpAccessUtilities.makePOSTRequestToLocalhost8080("app/kanban/create", kanbanRow.toString(), Map.of("Authorization", token));
            assertSuccessResponse(createResponse);

            JSONObject createdKanbanRow = new JSONObject(createResponse).getJSONObject("kanbanRow");
            Assertions.assertEquals("Test Kanban Title", createdKanbanRow.getString("title"));
            Assertions.assertEquals("Test Kanban Description", createdKanbanRow.getString("description"));

            // Update the Kanban entry
            JSONObject updateKanbanRow = new JSONObject();
            updateKanbanRow.put("title", "Updated Kanban Title");
            updateKanbanRow.put("description", "Updated Kanban Description");

            String unauthorizedUpdateResponse = HttpAccessUtilities.makePOSTRequestToLocalhost8080("app/kanban/update/" + createdKanbanRow.getLong("id"), updateKanbanRow.toString(), Map.of());
            assertFailureResponse(unauthorizedUpdateResponse);

            String updateResponse = HttpAccessUtilities.makePOSTRequestToLocalhost8080("app/kanban/update/" + createdKanbanRow.getLong("id"), updateKanbanRow.toString(), Map.of("Authorization", token));
            assertSuccessResponse(updateResponse);

            // Get the updated list
            String listAfterUpdateResponse = HttpAccessUtilities.makeGETRequestToLocalhost8080("app/kanban/list", Map.of("Authorization", token));
            assertSuccessResponse(listAfterUpdateResponse);

            JSONObject listAfterUpdateJson = new JSONObject(listAfterUpdateResponse);
            JSONObject updatedKanbanRow = listAfterUpdateJson.getJSONArray("kanbanRows").getJSONObject(0);

            // Check the updated fields
            Assertions.assertEquals("Updated Kanban Title", updatedKanbanRow.getString("title"));
            Assertions.assertEquals("Updated Kanban Description", updatedKanbanRow.getString("description"));

            String deleteResponse = HttpAccessUtilities.makeGETRequestToLocalhost8080("app/kanban/delete/" + createdKanbanRow.getLong("id"), Map.of("Authorization", token));
            assertSuccessResponse(deleteResponse);

            String listAfterDeleteResponse = HttpAccessUtilities.makeGETRequestToLocalhost8080("app/kanban/list", Map.of("Authorization", token));
            assertSuccessResponse(listAfterDeleteResponse);

            JSONObject listAfterDeleteJson = new JSONObject(listAfterDeleteResponse);
            Assertions.assertEquals(0, listAfterDeleteJson.getJSONArray("kanbanRows").length());
        }
    }

    private void assertSuccessResponse(String response) {
        JSONObject jsonResponse = new JSONObject(response);
        Assertions.assertTrue(jsonResponse.getBoolean("success"));
    }

    private void assertFailureResponse(String response) {
        Assertions.assertNotNull(response, "Response is null");
        final JSONObject jsonResponse = new JSONObject(response);
        Assertions.assertFalse(jsonResponse.getBoolean("success"), jsonResponse.toString());
    }
}