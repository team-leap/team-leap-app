package de.teamleap.app.services.users;

import de.teamleap.config.RDConfig;
import de.teamleap.exception.RDRequestException;
import de.teamleap.util.EmbeddedPostgresDbProvider;
import io.zonky.test.db.postgres.embedded.EmbeddedPostgres;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.UUID;

import static org.junit.jupiter.api.Assertions.assertThrows;

class UserManagementServiceTest {

    @BeforeAll
    public static void beforeAll() {
        EmbeddedPostgresDbProvider.registerEmbeddedDb();
        RDConfig.setIsTestCase(true, EmbeddedPostgres.class);
    }

    @BeforeEach
    public void beforeEach() {
        EmbeddedPostgresDbProvider.resetEmbeddedDb();
    }

    @Test
    public void userCreationTest() {
        final UserManagementService service = new UserManagementService();

        final String username = "testuser";
        final String password = "testpassword";

        service.createUser(username, password);

        service.validateUser(username, password);

        assertThrows(RDRequestException.class, () -> service.createUser(username, password));
        assertThrows(RDRequestException.class, () -> service.createUser(username, "wrongpassword"));

        assertThrows(RDRequestException.class, () -> service.validateUser(username, "wrongpassword"));

        service.changeUserPassword(username, "newpassword");
        assertThrows(RDRequestException.class, () -> service.validateUser(username, password));

        service.validateUser(username, "newpassword");
    }

    @Test
    public void userTokenTest() {
        final UserManagementService service = new UserManagementService();

        final String username = "testuser";
        final String password = "testpassword";

        service.createUser(username, password);
        final UUID token = service.createToken(username, password);

        service.validateToken(username, token);
    }

    @Test
    public void defaultUserTest() {
        final UserManagementService service = new UserManagementService();

        final String username = "default_user";
        final String password = "default_password";
        final UUID token = UUID.fromString("825665aa-8018-44ed-bf8d-027cb83a0a35");

        service.validateToken(username, token);
        Assertions.assertEquals(token, service.createToken(username, password));

        Assertions.assertThrows(RDRequestException.class, () -> service.changeUserPassword(username, ""));
    }
}
