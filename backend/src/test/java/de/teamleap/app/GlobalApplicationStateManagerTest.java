package de.teamleap.app;

import de.teamleap.config.RDConfig;
import de.teamleap.other.ApplicationBlockType;
import de.teamleap.other.ApplicationStates;
import de.teamleap.util.EmbeddedPostgresDbProvider;
import de.teamleap.util.HttpAccessUtilities;
import io.zonky.test.db.postgres.embedded.EmbeddedPostgres;
import org.json.JSONObject;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.boot.SpringApplication;
import org.springframework.context.ConfigurableApplicationContext;

import java.util.Map;

class GlobalApplicationStateManagerTest {

    @BeforeAll
    public static void beforeAll() {
        EmbeddedPostgresDbProvider.registerEmbeddedDb();
        RDConfig.setIsTestCase(true, EmbeddedPostgres.class);
    }

    @BeforeEach
    public void beforeEach() {
        EmbeddedPostgresDbProvider.resetEmbeddedDb();
    }

    @Test
    public void deployApplicationIntegrationTest() {
        try (ConfigurableApplicationContext ignored = SpringApplication.run(MainController.class)) {
            HttpAccessUtilities.makeGETRequestToLocalhost8080("action/block/insert/admincard");

            final JSONObject registerResponse = new JSONObject(HttpAccessUtilities.makeGETRequestToLocalhost8080("monitor/register"));
            final String token = registerResponse.getString("token");
            Assertions.assertNotNull(token);

            HttpAccessUtilities.makeGETRequestToLocalhost8080("action/block/remove/admincard");

            assertSuccessResponse(HttpAccessUtilities.makeGETRequestToLocalhost8080("monitor/learning", Map.of("Authorization", token)));

            final JSONObject afterLearningStartedStatus = new JSONObject(HttpAccessUtilities.makeGETRequestToLocalhost8080("monitor/state", Map.of("Authorization", token)));
            Assertions.assertEquals(ApplicationStates.LEARNING.name(), afterLearningStartedStatus.getJSONObject("state").getString("state"));
            Assertions.assertFalse(afterLearningStartedStatus.getJSONObject("state").getBoolean("adminCard"));

            for (int i = 0; i < ApplicationBlockType.NON_ADMIN_TYPES.length; i++) {
                assertSuccessResponse(HttpAccessUtilities.makeGETRequestToLocalhost8080("action/block/insert/block-" + i));

                final JSONObject betweenLearningStatus = new JSONObject(HttpAccessUtilities.makeGETRequestToLocalhost8080("monitor/state", Map.of("Authorization", token)));
                Assertions.assertEquals(ApplicationStates.LEARNING.name(), betweenLearningStatus.getJSONObject("state").getString("state"));

                assertSuccessResponse(HttpAccessUtilities.makeGETRequestToLocalhost8080("action/block/remove/block-" + i));
            }

            final JSONObject afterRegistrationStatus = new JSONObject(HttpAccessUtilities.makeGETRequestToLocalhost8080("monitor/state", Map.of("Authorization", token)));
            Assertions.assertEquals(ApplicationStates.BUILDING_WAITING.name(), afterRegistrationStatus.getJSONObject("state").getString("state"));


            assertSuccessResponse(HttpAccessUtilities.makeGETRequestToLocalhost8080("action/block/insert/block-4"));
            assertSuccessResponse(HttpAccessUtilities.makeGETRequestToLocalhost8080("action/block/insert/block-0"));

            assertFailureResponse(HttpAccessUtilities.makeGETRequestToLocalhost8080("action/button"));

            assertSuccessResponse(HttpAccessUtilities.makeGETRequestToLocalhost8080("action/block/insert/block-2"));

            final JSONObject beforeDeployButtonOne = new JSONObject(HttpAccessUtilities.makeGETRequestToLocalhost8080("monitor/state", Map.of("Authorization", token)));
            Assertions.assertEquals(ApplicationStates.BUILDING_RUNNING.name(), beforeDeployButtonOne.getJSONObject("state").getString("state"));

            assertSuccessResponse(HttpAccessUtilities.makeGETRequestToLocalhost8080("action/key"));
            assertSuccessResponse(HttpAccessUtilities.makeGETRequestToLocalhost8080("action/button"));

            final JSONObject afterDeployButtonTwo = new JSONObject(HttpAccessUtilities.makeGETRequestToLocalhost8080("monitor/state", Map.of("Authorization", token)));
            Assertions.assertEquals(ApplicationStates.APP_DEPLOYED.name(), afterDeployButtonTwo.getJSONObject("state").getString("state"));

            assertFailureResponse(HttpAccessUtilities.makeGETRequestToLocalhost8080("action/block/insert/block-2"));

            assertSuccessResponse(HttpAccessUtilities.makeGETRequestToLocalhost8080("action/block/remove/block-0"));
            assertSuccessResponse(HttpAccessUtilities.makeGETRequestToLocalhost8080("action/block/remove/block-2"));
            assertSuccessResponse(HttpAccessUtilities.makeGETRequestToLocalhost8080("action/block/remove/block-4"));

            assertFailureResponse(HttpAccessUtilities.makeGETRequestToLocalhost8080("action/block/remove/block-4"));

            final JSONObject afterRemovingAllBlocks = new JSONObject(HttpAccessUtilities.makeGETRequestToLocalhost8080("monitor/state", Map.of("Authorization", token)));
            Assertions.assertEquals(ApplicationStates.BUILDING_WAITING.name(), afterRemovingAllBlocks.getJSONObject("state").getString("state"));

            final JSONObject builtApplicationEntry = new JSONObject(HttpAccessUtilities.makeGETRequestToLocalhost8080("appconfig/1"));
            Assertions.assertEquals(1, builtApplicationEntry.getJSONObject("appConfig").getInt("id"));
            Assertions.assertEquals(ApplicationBlockType.ADDON_CONVERSATIONAL_AI.getId(), builtApplicationEntry.getJSONObject("appConfig").getInt("addon"));
            Assertions.assertEquals(ApplicationBlockType.IMPLEMENTATION_INFORMATION.getId(), builtApplicationEntry.getJSONObject("appConfig").getInt("implementation"));
            Assertions.assertEquals(ApplicationBlockType.CONTEXT_BUSINESS.getId(), builtApplicationEntry.getJSONObject("appConfig").getInt("context"));
        }
    }

    @Test
    public void cancelDeploymentTest() {
        try (ConfigurableApplicationContext ignored = SpringApplication.run(MainController.class)) {
            HttpAccessUtilities.makeGETRequestToLocalhost8080("action/block/insert/admincard");

            final JSONObject registerResponse = new JSONObject(HttpAccessUtilities.makeGETRequestToLocalhost8080("monitor/register"));
            final String token = registerResponse.getString("token");
            Assertions.assertNotNull(token);

            HttpAccessUtilities.makeGETRequestToLocalhost8080("action/block/remove/admincard");

            assertSuccessResponse(HttpAccessUtilities.makeGETRequestToLocalhost8080("monitor/learning", Map.of("Authorization", token)));

            final JSONObject afterLearningStartedStatus = new JSONObject(HttpAccessUtilities.makeGETRequestToLocalhost8080("monitor/state", Map.of("Authorization", token)));
            Assertions.assertEquals(ApplicationStates.LEARNING.name(), afterLearningStartedStatus.getJSONObject("state").getString("state"));
            Assertions.assertFalse(afterLearningStartedStatus.getJSONObject("state").getBoolean("adminCard"));

            for (int i = 0; i < ApplicationBlockType.NON_ADMIN_TYPES.length; i++) {
                assertSuccessResponse(HttpAccessUtilities.makeGETRequestToLocalhost8080("action/block/insert/block-" + i));

                final JSONObject betweenLearningStatus = new JSONObject(HttpAccessUtilities.makeGETRequestToLocalhost8080("monitor/state", Map.of("Authorization", token)));
                Assertions.assertEquals(ApplicationStates.LEARNING.name(), betweenLearningStatus.getJSONObject("state").getString("state"));

                assertSuccessResponse(HttpAccessUtilities.makeGETRequestToLocalhost8080("action/block/remove/block-" + i));
            }

            assertSuccessResponse(HttpAccessUtilities.makeGETRequestToLocalhost8080("action/block/insert/block-4"));
            assertSuccessResponse(HttpAccessUtilities.makeGETRequestToLocalhost8080("action/block/insert/block-0"));
            assertSuccessResponse(HttpAccessUtilities.makeGETRequestToLocalhost8080("action/block/insert/block-2"));

            assertSuccessResponse(HttpAccessUtilities.makeGETRequestToLocalhost8080("action/key"));

            assertSuccessResponse(HttpAccessUtilities.makeGETRequestToLocalhost8080("action/block/remove/block-2"));

            final JSONObject afterRemovingBlockFromConfirmState = new JSONObject(HttpAccessUtilities.makeGETRequestToLocalhost8080("monitor/state", Map.of("Authorization", token)));
            Assertions.assertEquals(ApplicationStates.BUILDING_RUNNING.name(), afterRemovingBlockFromConfirmState.getJSONObject("state").getString("state"));
            Assertions.assertEquals(0, afterRemovingBlockFromConfirmState.getJSONObject("state").getInt("successiveDeployButtonPresses"));

            assertSuccessResponse(HttpAccessUtilities.makeGETRequestToLocalhost8080("action/block/insert/block-2"));

            assertSuccessResponse(HttpAccessUtilities.makeGETRequestToLocalhost8080("action/key"));
            assertSuccessResponse(HttpAccessUtilities.makeGETRequestToLocalhost8080("action/button"));
        }
    }

    @Test
    public void doubleAdminCardInsertTest() {
        try (ConfigurableApplicationContext ignored = SpringApplication.run(MainController.class)) {
            HttpAccessUtilities.makeGETRequestToLocalhost8080("action/block/insert/admincard");
            HttpAccessUtilities.makeGETRequestToLocalhost8080("action/block/insert/admincard_alt_1");

            final JSONObject registerResponse = new JSONObject(HttpAccessUtilities.makeGETRequestToLocalhost8080("monitor/register"));
            final String token = registerResponse.getString("token");
            Assertions.assertNotNull(token);

            HttpAccessUtilities.makeGETRequestToLocalhost8080("action/block/remove/admincard");

            // both should be removed now
            final JSONObject afterRemovingOne = new JSONObject(HttpAccessUtilities.makeGETRequestToLocalhost8080("monitor/state", Map.of("Authorization", token)));
            Assertions.assertEquals(0, afterRemovingOne.getJSONObject("state").getJSONArray("activeBlocks").length());
        }
    }

    private void assertSuccessResponse(String response) {
        Assertions.assertNotNull(response, "Response is null");
        final JSONObject jsonResponse = new JSONObject(response);
        Assertions.assertTrue(jsonResponse.getBoolean("success"), jsonResponse.toString());
    }

    private void assertFailureResponse(String response) {
        Assertions.assertNotNull(response, "Response is null");
        final JSONObject jsonResponse = new JSONObject(response);
        Assertions.assertFalse(jsonResponse.getBoolean("success"), jsonResponse.toString());
    }
}