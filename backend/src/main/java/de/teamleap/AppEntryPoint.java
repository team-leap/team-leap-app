package de.teamleap;

import de.teamleap.app.MainController;
import org.springframework.boot.SpringApplication;

import java.sql.DriverManager;
import java.sql.SQLException;

public class AppEntryPoint {

    public static void main(String[] args) throws ClassNotFoundException, SQLException {
        Class.forName("org.postgresql.Driver");
        DriverManager.registerDriver(new org.postgresql.Driver());
        SpringApplication.run(MainController.class, args);
    }
}
