package de.teamleap.other;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Timer;
import java.util.concurrent.CopyOnWriteArrayList;
import java.util.function.Consumer;

public class DeviceActiveManager {

    private final static long LAST_SEEN_TIMEOUT = 10 * 1000;

    public final static String DEVICE_RASPBERRY_PI = "raspberry_pi";

    private final Map<String, Long> deviceLastSeenMap = new HashMap<>();
    private final Map<String, Boolean> deviceOfflineAlreadyBroadcasted = new HashMap<>();

    private final List<Consumer<String>> onDeviceActiveCallbacks = new CopyOnWriteArrayList<>();
    private final List<Consumer<String>> onDeviceInactiveCallbacks = new CopyOnWriteArrayList<>();

    public DeviceActiveManager() {
        final Timer timer = new Timer();
        timer.scheduleAtFixedRate(new java.util.TimerTask() {
            @Override
            public void run() {
                deviceLastSeenMap.keySet().forEach(deviceId -> {
                    if (!isDeviceActive(deviceId)) {
                        if (!deviceOfflineAlreadyBroadcasted.getOrDefault(deviceId, false)) {
                            onDeviceInactiveCallbacks.forEach(callback -> callback.accept(deviceId));
                            deviceOfflineAlreadyBroadcasted.put(deviceId, true);
                        }
                    } else {
                        deviceOfflineAlreadyBroadcasted.put(deviceId, false);
                    }
                });
            }
        }, 1000, LAST_SEEN_TIMEOUT / 2);

        // add default devices as offline
        deviceLastSeenMap.put(DEVICE_RASPBERRY_PI, 0L);
        deviceOfflineAlreadyBroadcasted.put(DEVICE_RASPBERRY_PI, false);
    }

    public void onDeviceActive(Consumer<String> callback) {
        onDeviceActiveCallbacks.add(callback);
    }

    public void onDeviceInactive(Consumer<String> callback) {
        onDeviceInactiveCallbacks.add(callback);
    }

    /**
     * Makes a device active using the current timestamp.<br>
     * Calls all registered callbacks when a device becomes active.
     *
     * @param deviceId The device id to make active.
     */
    public void updateDeviceLastSeen(String deviceId) {
        if (isDeviceActive(deviceId)) {
            deviceLastSeenMap.put(deviceId, System.currentTimeMillis());
        } else {
            deviceLastSeenMap.put(deviceId, System.currentTimeMillis());
            onDeviceActiveCallbacks.forEach(callback -> callback.accept(deviceId));
        }
        deviceOfflineAlreadyBroadcasted.put(deviceId, false);
    }

    private long lastSeenSince(String deviceId) {
        final Long lastSeen = deviceLastSeenMap.get(deviceId);
        if (lastSeen == null) {
            return 0;
        }

        return System.currentTimeMillis() - lastSeen;
    }

    /**
     * Checks if a device is active.
     *
     * @param deviceId The device id to check.
     * @return True if the device is active, false otherwise.
     */
    public boolean isDeviceActive(String deviceId) {
        return lastSeenSince(deviceId) < LAST_SEEN_TIMEOUT;
    }
}
