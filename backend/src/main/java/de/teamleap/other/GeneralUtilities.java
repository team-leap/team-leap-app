package de.teamleap.other;

public abstract class GeneralUtilities {

    public final static String CATEGORY_IMPLEMENTATION = "Implementation";
    public final static String CATEGORY_CONTEXT = "Context";
    public final static String CATEGORY_ADDON = "Add-on";
    public final static String CATEGORY_ADMIN = "Admin";

    public static  <T> boolean isEqualToOneOf(T value, T... values) {
        for (T v : values) {
            if (value.equals(v)) {
                return true;
            }
        }
        return false;
    }
}
