package de.teamleap.other;

import java.util.Arrays;

public enum ApplicationBlockType {
    IMPLEMENTATION_INFORMATION(1, "Information", GeneralUtilities.CATEGORY_IMPLEMENTATION),
    IMPLEMENTATION_PLANNING(2, "Planning", GeneralUtilities.CATEGORY_IMPLEMENTATION),
    CONTEXT_BUSINESS(3, "Business", GeneralUtilities.CATEGORY_CONTEXT),
    CONTEXT_LEISURE(4, "Leisure", GeneralUtilities.CATEGORY_CONTEXT),
    ADDON_CONVERSATIONAL_AI(5, "Conversational AI", GeneralUtilities.CATEGORY_ADDON),
    ADDON_SECURITY(6, "Security", GeneralUtilities.CATEGORY_ADDON),
    ADMIN_CARD(7, "Admin Card", GeneralUtilities.CATEGORY_ADMIN);

    private final int id;
    private final String name;
    private final String type;

    ApplicationBlockType(int id, String name, String type) {
        this.id = id;
        this.name = name;
        this.type = type;
    }

    public final static ApplicationBlockType[] NON_ADMIN_TYPES = Arrays.stream(values()).filter(type -> type != ADMIN_CARD).toArray(ApplicationBlockType[]::new);

    public static ApplicationBlockType fromType(int type) {
        for (ApplicationBlockType value : values()) {
            if (value.getId() == type) {
                return value;
            }
        }

        return null;
    }

    public int getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public String getType() {
        return type;
    }

    @Override
    public String toString() {
        return "BlockType{" +
               "id=" + id +
               ", name='" + name + '\'' +
               ", type='" + type + '\'' +
               '}';
    }
}

