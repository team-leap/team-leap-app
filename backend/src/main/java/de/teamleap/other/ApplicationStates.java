package de.teamleap.other;

public enum ApplicationStates {
    INITIALIZING,
    LEARNING,
    BUILDING_WAITING,
    BUILDING_RUNNING,
    APP_DEPLOYED,
}
