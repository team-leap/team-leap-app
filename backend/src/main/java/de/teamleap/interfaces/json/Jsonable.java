package de.teamleap.interfaces.json;

import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.lang.reflect.Constructor;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.util.LinkedHashMap;
import java.util.Map;

public interface Jsonable {

    Logger LOG = LoggerFactory.getLogger(Jsonable.class);

    default JSONObject toJson() {
        return JsonableHelper.toJson(this);
    }

    Object defaultValueForMissingField(String fieldName);

    static <T extends Jsonable> T fromJson(Class<T> clazz, JSONObject json) {
        return JsonableHelper.fromJson(clazz, json);
    }

    class JsonableHelper {
        public static JSONObject toJson(Jsonable jsonable) {
            final JSONObject json = new JSONObject();
            final Map<Field, JsonField> jsonFields = getJsonFields(jsonable.getClass());

            for (Map.Entry<Field, JsonField> entry : jsonFields.entrySet()) {
                final Field field = entry.getKey();

                field.setAccessible(true);
                final Object fieldValue;
                try {
                    fieldValue = field.get(jsonable);
                } catch (IllegalAccessException e) {
                    throw new RuntimeException(e);
                }

                final JsonField jsonField = entry.getValue();
                final String name = jsonField.name();
                final FieldToJsonMapper fieldToJsonMapper = jsonField.fieldToJsonMapper();

                final Object value = fieldToJsonMapper.getMapper().apply(fieldValue);
                json.put(name, value);
            }

            return json;
        }

        private static Map<Field, JsonField> getJsonFields(Class<?> clazz) {
            final Map<Field, JsonField> jsonFields = new LinkedHashMap<>();
            final Field[] fields = clazz.getDeclaredFields();

            for (Field field : fields) {
                final JsonField jsonField = field.getAnnotation(JsonField.class);
                if (jsonField != null) {
                    jsonFields.put(field, jsonField);
                }
            }

            return jsonFields;
        }

        public static <T extends Jsonable> T fromJson(Class<T> clazz, JSONObject json) {
            final T jsonable;
            try {
                final Constructor<T> constructor = clazz.getConstructor();
                jsonable = constructor.newInstance();
            } catch (InstantiationException | IllegalAccessException | NoSuchMethodException |
                     InvocationTargetException e) {
                throw new RuntimeException("Could not instantiate Jsonable class " + clazz.getName(), e);
            }

            final Map<Field, JsonField> jsonFields = getJsonFields(clazz);

            for (Map.Entry<Field, JsonField> entry : jsonFields.entrySet()) {
                final Field field = entry.getKey();
                final JsonField jsonField = entry.getValue();

                final String name = jsonField.name();
                final JsonToFieldMapper jsonToFieldMapper = jsonField.jsonToFieldMapper();

                final Object setValue;

                if (json.has(name)) {
                    final Object value = json.get(name);
                    setValue = jsonToFieldMapper.getMapper().apply(value);
                } else {
                    setValue = jsonable.defaultValueForMissingField(name);
                }

                field.setAccessible(true);
                try {
                    field.set(jsonable, setValue);
                } catch (IllegalAccessException e) {
                    throw new RuntimeException(e);
                }
            }

            return jsonable;
        }
    }
}
