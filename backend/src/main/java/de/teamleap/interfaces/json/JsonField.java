package de.teamleap.interfaces.json;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

@Retention(RetentionPolicy.RUNTIME)
public @interface JsonField {
    String name();

    FieldToJsonMapper fieldToJsonMapper() default FieldToJsonMapper.IDENTITY;

    JsonToFieldMapper jsonToFieldMapper() default JsonToFieldMapper.IDENTITY;
}
