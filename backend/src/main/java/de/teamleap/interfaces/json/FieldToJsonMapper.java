package de.teamleap.interfaces.json;

import java.util.function.Function;

public enum FieldToJsonMapper {
    IDENTITY(o -> o),
    NULL_OR_TO_STRING(o -> o == null ? null : o.toString());

    private final Function<Object, Object> mapper;

    FieldToJsonMapper(Function<Object, Object> mapper) {
        this.mapper = mapper;
    }

    public Function<Object, Object> getMapper() {
        return mapper;
    }
}
