package de.teamleap.interfaces.json;

import java.sql.Timestamp;
import java.util.function.Function;

public enum JsonToFieldMapper {
    IDENTITY(o -> o),
    LONG(o -> {
        if (o == null) return null;
        if (o instanceof Integer) {
            return ((Integer) o).longValue();
        } else if (o instanceof Long) {
            return o;
        } else if (o instanceof String) {
            return Long.parseLong((String) o);
        } else {
            throw new IllegalArgumentException("Cannot convert " + o + " to Long");
        }
    }),
    INTEGER(o -> {
        if (o == null) return null;
        if (o instanceof Integer) {
            return o;
        } else if (o instanceof Long) {
            return ((Long) o).intValue();
        } else if (o instanceof String) {
            return Integer.parseInt((String) o);
        } else {
            throw new IllegalArgumentException("Cannot convert " + o + " to Integer");
        }
    }),
    TIMESTAMP(o -> {
        if (o == null) return null;

        // expected string: "2023-04-18 13:10:32.359"; convert to java.sql.Timestamp
        final String s = (String) o;
        return Timestamp.valueOf(s);
    }),
    UUID(o -> {
        if (o == null) {
            return null;
        }
        if (o instanceof String) {
            return java.util.UUID.fromString((String) o);
        } else {
            throw new IllegalArgumentException("Cannot convert " + o + " to UUID");
        }
    });

    private final Function<Object, Object> mapper;

    JsonToFieldMapper(Function<Object, Object> mapper) {
        this.mapper = mapper;
    }

    public Function<Object, Object> getMapper() {
        return mapper;
    }
}
