package de.teamleap.interfaces.db;

import de.teamleap.app.services.users.UserManagementService;
import de.teamleap.interfaces.db.type.ByteData;

import java.util.function.Function;

/**
 * PostgreSQL data types to Java types as returned by the JDBC driver.
 */
public enum DatabaseToFieldMapper {
    IDENTITY(o -> o),
    BYTE_DATA(o -> {
        if (o instanceof byte[]) {
            return new ByteData((byte[]) o);
        } else if (o instanceof ByteData) {
            return o;
        }else if (o instanceof String) {
            return new ByteData((String) o);
        } else {
            UserManagementService.LOG.warn("Failed to map database type to field type: {}", o.getClass());
            return null;
        }
    });

    private final Function<Object, Object> mapper;

    DatabaseToFieldMapper(Function<Object, Object> mapper) {
        this.mapper = mapper;
    }

    public Function<Object, Object> getMapper() {
        return mapper;
    }
}
