package de.teamleap.exception;

import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.nio.charset.StandardCharsets;
import java.util.Base64;
import java.util.function.Supplier;

public class ExceptionHandlingUtilities {

    private static final Logger LOG = LoggerFactory.getLogger(ExceptionHandlingUtilities.class);

    public static String exceptionResponseHandler(Supplier<JSONObject> supplier) {
        return exceptionResponseHandler(supplier, null);
    }

    public static String exceptionResponseHandler(Supplier<JSONObject> supplier, String genericErrorMessage) {
        try {
            return supplier.get().toString();
        } catch (Exception e) {
            return errorJson(e, genericErrorMessage).toString();
        }
    }

    public static JSONObject errorJson(Exception e) {
        LOG.info("Error", e);
        final JSONObject response = new JSONObject().put("success", false);

        if (e instanceof RDRequestException) {
            response.put("error", e.getMessage());
        } else {
            response.put("error", "Internal Server Error");
        }
        response.put("detail", createExceptionFingerprint(e));

        return response;
    }

    public static JSONObject errorJson(Exception e, String message) {
        final JSONObject response = errorJson(e);

        if (message != null) {
            response.put("message", message);
        }

        return response;
    }

    public static JSONObject successJson() {
        return new JSONObject().put("success", true);
    }

    private static String createExceptionFingerprint(Throwable e) {
        final Throwable rootCause = getRootCause(e);
        final StackTraceElement element = rootCause.getStackTrace()[0];
        final String detail = (element.getFileName() == null ? null : element.getFileName().replace(".java", "")) + ":" + element.getLineNumber();
        final String rot13 = rot13(detail);
        return Base64.getEncoder().encodeToString(rot13.getBytes(StandardCharsets.UTF_8));
    }

    private static String rot13(String input) {
        StringBuilder sb = new StringBuilder();
        for (int i = 0; i < input.length(); i++) {
            char c = input.charAt(i);
            if (c >= 'a' && c <= 'm') c += 13;
            else if (c >= 'A' && c <= 'M') c += 13;
            else if (c >= 'n' && c <= 'z') c -= 13;
            else if (c >= 'N' && c <= 'Z') c -= 13;
            sb.append(c);
        }
        return sb.toString();
    }

    private static Throwable getRootCause(Throwable throwable) {
        while (throwable.getCause() != null) {
            if (throwable instanceof RDRequestException) {
                break;
            }
            throwable = throwable.getCause();
        }
        return throwable;
    }

    private static String reverseExceptionFingerprint(String fingerprint) {
        // Decode the Base64 string
        byte[] decodedBytes = Base64.getDecoder().decode(fingerprint);
        String decoded = new String(decodedBytes, StandardCharsets.UTF_8);

        // Apply ROT13
        String detail = rot13(decoded);

        // Split the string into file name and line number
        String[] parts = detail.split(":", 2);
        String fileName = parts[0] + ".java";
        String lineNumber = parts.length > 1 ? parts[1] : "";

        return fileName + ":" + lineNumber;
    }
}
