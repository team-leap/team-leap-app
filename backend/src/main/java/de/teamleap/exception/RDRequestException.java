package de.teamleap.exception;

public class RDRequestException extends RuntimeException {

    public RDRequestException(String message) {
        super(message);
    }

    public RDRequestException(String message, Throwable cause) {
        super(message, cause);
    }

    public RDRequestException(Throwable cause) {
        super(cause);
    }

    public RDRequestException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}
