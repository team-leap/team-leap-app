package de.teamleap.config;

import de.teamleap.app.GlobalApplicationStateManager;
import org.springframework.core.io.ClassPathResource;

import java.io.IOException;
import java.io.InputStream;
import java.util.Arrays;
import java.util.Properties;
import java.util.UUID;

public abstract class RDConfig {

    private final static String RESOURCES_PROPERTIES_FILE = "rocket-deployer.properties";

    private final static Properties properties = getProperties();

    private static boolean isTestDb = false;

    public static void setIsTestCase(boolean isTestDb, Class<?> caller) {
        if (caller.getName().equals("io.zonky.test.db.postgres.embedded.EmbeddedPostgres") &&
            Arrays.stream(caller.getAnnotatedInterfaces()).anyMatch(i -> i.getType().getTypeName().equals("java.io.Closeable"))) {
            RDConfig.isTestDb = isTestDb;
        } else {
            throw new RuntimeException("RDConfig.setTestDb() called from wrong class: " + caller.getName());
        }
    }

    public static Properties getProperties() {
        if (properties == null) {
            final Properties properties = new Properties();

            try (final InputStream is = new ClassPathResource(RESOURCES_PROPERTIES_FILE).getInputStream()) {
                properties.load(is);
            } catch (IOException e) {
                throw new RuntimeException("Could not load properties file " + RESOURCES_PROPERTIES_FILE, e);
            }
            return properties;
        }
        return properties;
    }

    private static String getStringProperty(String key) {
        if (getProperties().getProperty(key) == null) {
            throw new RuntimeException(key + " not found in " + RESOURCES_PROPERTIES_FILE);
        }
        return getProperties().getProperty(key);
    }

    private static boolean getBooleanProperty(String key) {
        if (getProperties().getProperty(key) == null) {
            throw new RuntimeException(key + " not found in " + RESOURCES_PROPERTIES_FILE);
        }
        return Boolean.parseBoolean(getProperties().getProperty(key));
    }

    public static String getSapBtpDatabaseServiceId() {
        return RDConfig.getStringProperty("app.database.service.id");
    }

    public static String getLocalDevelopmentDatabaseUri() {
        return RDConfig.getStringProperty("app.database.local.uri");
    }

    public static UUID[] getAdminCardUUIDs() {
        final String admincardIds = RDConfig.getStringProperty("app.admincards");
        final String[] splitIds = admincardIds.split(", ?");
        final UUID[] admincardUUIDs = new UUID[splitIds.length + (isTestDb ? 2 : 0)];

        for (int i = 0; i < splitIds.length; i++) {
            if (splitIds[i].length() == 0) continue;
            admincardUUIDs[i] = GlobalApplicationStateManager.makeBlockUUID(splitIds[i]);
        }

        if (isTestDb) {
            admincardUUIDs[splitIds.length] = GlobalApplicationStateManager.makeBlockUUID("admincard");
            admincardUUIDs[splitIds.length + 1] = GlobalApplicationStateManager.makeBlockUUID("admincard_alt_1");
        }

        return admincardUUIDs;
    }
}
