package de.teamleap.app.services.newsfeed.sources;

import de.teamleap.app.services.newsfeed.parsers.BbcItemParser;
import de.teamleap.app.services.newsfeed.parsers.RssFeedItemParser;

public class BbcTopNewsFeedSource extends FeedSource {
    private final static RssFeedItemParser PARSER = new BbcItemParser();

    @Override
    public String getShortId() {
        return "bbc-top-news";
    }

    @Override
    public String getLanguage() {
        return LANGUAGE_ENGLISH;
    }

    @Override
    public String getSourceUrl() {
        return "http://feeds.bbci.co.uk/news/rss.xml";
    }

    @Override
    public RssFeedItemParser getFeedItemParser() {
        return PARSER;
    }
}
