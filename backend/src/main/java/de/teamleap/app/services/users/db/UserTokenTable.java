package de.teamleap.app.services.users.db;

import de.teamleap.interfaces.db.JdbcTable;

import java.sql.ResultSet;

public class UserTokenTable extends JdbcTable<UserTokenRow, Integer> {

    @Override
    protected UserTokenRow createInstance(ResultSet resultSet) {
        return new UserTokenRow(resultSet);
    }

    @Override
    public String getSchemaResourcePath() {
        return "db/schema/users_tokens.sql";
    }

    @Override
    public String getTableName() {
        return "users_tokens";
    }

    @Override
    protected String getPrimaryKeyName() {
        return "id";
    }
}
