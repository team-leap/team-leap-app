package de.teamleap.app.services.newsfeed.sources;

import de.teamleap.app.services.newsfeed.RssFeedEntry;
import de.teamleap.app.services.newsfeed.parsers.RssFeedItemParser;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import java.io.InputStream;
import java.net.URL;
import java.net.URLConnection;
import java.util.ArrayList;
import java.util.List;

public abstract class FeedSource {

    private static final Logger LOG = LoggerFactory.getLogger(FeedSource.class);

    private final static long CACHE_TIME = 1000 * 60 * 10;
    public final static String LANGUAGE_ENGLISH = "en";
    public final static String LANGUAGE_GERMAN = "de";

    protected final List<RssFeedEntry> cachedEntries = new ArrayList<>();
    private long lastFetchTime = 0;

    public abstract String getShortId();

    public abstract String getLanguage();

    public abstract String getSourceUrl();

    public abstract RssFeedItemParser getFeedItemParser();

    public List<RssFeedEntry> fetchFeed() throws Exception {
        synchronized (this.cachedEntries) {
            if (!this.cachedEntries.isEmpty() && System.currentTimeMillis() - this.lastFetchTime < CACHE_TIME) {
                return this.cachedEntries;
            }

            final URL url = new URL(getSourceUrl());
            LOG.info("Fetching feed from [{}]", url);
            final URLConnection connection = url.openConnection();
            final InputStream inputStream = connection.getInputStream();
            final List<RssFeedEntry> entries = this.parseEntries(inputStream);

            this.cachedEntries.clear();
            this.cachedEntries.addAll(entries);
            this.lastFetchTime = System.currentTimeMillis();

            return entries;
        }
    }

    public List<RssFeedEntry> parseEntries(InputStream inputStream) throws Exception {
        final List<RssFeedEntry> entries = new ArrayList<>();

        final DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
        final DocumentBuilder builder = factory.newDocumentBuilder();
        final Document doc = builder.parse(inputStream);
        doc.getDocumentElement().normalize();

        final NodeList nodeList = doc.getElementsByTagName("item");

        for (int i = 0; i < nodeList.getLength(); i++) {
            Node node = nodeList.item(i);
            if (node.getNodeType() == Node.ELEMENT_NODE) {
                final Element element = (Element) node;
                final RssFeedEntry entry = getFeedItemParser().parse(element);
                entries.add(entry);
            }
        }

        return entries;
    }
}

