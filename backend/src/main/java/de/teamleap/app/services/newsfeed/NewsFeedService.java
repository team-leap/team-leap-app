package de.teamleap.app.services.newsfeed;

import de.teamleap.app.services.newsfeed.sources.*;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class NewsFeedService {

    private final List<FeedSource> feedSources;

    public NewsFeedService(List<FeedSource> feedSources) {
        this.feedSources = feedSources;
    }

    public NewsFeedService() {
        this.feedSources = new ArrayList<>();
    }

    public void registerFeedSource(FeedSource feedSource) {
        this.feedSources.add(feedSource);
    }

    public Map<String, List<String>> getRegisteredFeedSourceIds() {
        final Map<String, List<String>> feedSourceIds = new HashMap<>();
        for (FeedSource feedSource : feedSources) {
            feedSourceIds.computeIfAbsent(feedSource.getLanguage(), k -> new ArrayList<>()).add(feedSource.getShortId());
        }
        return feedSourceIds;
    }

    public List<RssFeedEntry> fetchFeed(String feedSourceId) throws Exception {
        for (FeedSource feedSource : feedSources) {
            if (feedSource.getShortId().equals(feedSourceId)) {
                return feedSource.fetchFeed();
            }
        }
        return new ArrayList<>();
    }

    public void registerDefaultFeedSources() {
        this.registerFeedSource(new TagesschauFeedSource());
        this.registerFeedSource(new BbcEuropeFeedSource());
        this.registerFeedSource(new BbcTopNewsFeedSource());
        this.registerFeedSource(new TheGuardianFeedSource());
        this.registerFeedSource(new FazFeedSource());
    }
}
