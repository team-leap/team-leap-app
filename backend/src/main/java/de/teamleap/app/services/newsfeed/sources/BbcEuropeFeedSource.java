package de.teamleap.app.services.newsfeed.sources;

import de.teamleap.app.services.newsfeed.parsers.BbcItemParser;
import de.teamleap.app.services.newsfeed.parsers.RssFeedItemParser;

public class BbcEuropeFeedSource extends FeedSource {
    private final static RssFeedItemParser PARSER = new BbcItemParser();

    @Override
    public String getShortId() {
        return "bbc-europe";
    }

    @Override
    public String getLanguage() {
        return LANGUAGE_ENGLISH;
    }

    @Override
    public String getSourceUrl() {
        return "http://feeds.bbci.co.uk/news/world/europe/rss.xml";
    }

    @Override
    public RssFeedItemParser getFeedItemParser() {
        return PARSER;
    }
}
