package de.teamleap.app.services.newsfeed.parsers;

import de.teamleap.app.services.newsfeed.RssFeedEntry;
import org.w3c.dom.Element;

import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;

public class TagesschauItemParser implements RssFeedItemParser {

    @Override
    public RssFeedEntry parse(Element element) {
        final RssFeedEntry entry = new RssFeedEntry();

        entry.setTitle(getTagStringValue("title", element));
        entry.setLink(getTagStringValue("link", element));
        entry.setDescription(getTagStringValue("description", element));
        entry.setGuid(getTagStringValue("guid", element));

        final String pubDateStr = getTagStringValue("pubDate", element);
        entry.setPubDate(ZonedDateTime.parse(pubDateStr, DateTimeFormatter.RFC_1123_DATE_TIME));

        entry.setDetailedDescription(getTagStringValue("content:encoded", element));

        return entry;
    }
}
