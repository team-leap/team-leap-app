package de.teamleap.app.services.appconfig.db;

import de.teamleap.interfaces.db.JdbcTable;

import java.sql.ResultSet;

public class AppConfigTable extends JdbcTable<AppConfigRow, Integer> {

    @Override
    protected AppConfigRow createInstance(ResultSet resultSet) {
        return new AppConfigRow(resultSet);
    }

    @Override
    public String getSchemaResourcePath() {
        return "db/schema/appconfig.sql";
    }

    @Override
    public String getTableName() {
        return "appconfig";
    }

    @Override
    protected String getPrimaryKeyName() {
        return "id";
    }
}
