package de.teamleap.app.services.learnblock.db;

import de.teamleap.interfaces.db.JdbcTable;

import java.sql.ResultSet;
import java.util.UUID;

public class LearnedBlockTable extends JdbcTable<LearnedBlockRow, UUID> {

    @Override
    protected LearnedBlockRow createInstance(ResultSet resultSet) {
        return new LearnedBlockRow(resultSet);
    }

    @Override
    public String getSchemaResourcePath() {
        return "db/schema/learned_blocks.sql";
    }

    @Override
    public String getTableName() {
        return "learned_blocks";
    }

    @Override
    protected String getPrimaryKeyName() {
        return "id";
    }
}
