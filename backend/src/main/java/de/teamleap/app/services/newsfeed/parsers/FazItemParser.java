package de.teamleap.app.services.newsfeed.parsers;

import de.teamleap.app.services.newsfeed.RssFeedEntry;
import org.w3c.dom.Element;

import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;

public class FazItemParser implements RssFeedItemParser {

    @Override
    public RssFeedEntry parse(Element element) {
        final RssFeedEntry entry = new RssFeedEntry();

        entry.setTitle(getTagStringValue("title", element));
        entry.setLink(getTagStringValue("link", element));
        entry.setDescription(cleanDescription(getTagStringValue("description", element)));
        entry.setGuid(getTagStringValue("guid", element));

        final String pubDateStr = getTagStringValue("pubDate", element);
        entry.setPubDate(ZonedDateTime.parse(pubDateStr, DateTimeFormatter.RFC_1123_DATE_TIME));

        // <media:content url="https://media1.faz.net/ppmedia/aktuell/sport/2202184710/1.8938779/default/36f79122-017d-11ee-9eb2.jpg" type="image/jpeg" medium="image" height="627" width="940"/>
        final String mediaUrl = element.getElementsByTagName("media:content").item(0).getAttributes().getNamedItem("url").getNodeValue();
        entry.setThumbnail(mediaUrl);

        return entry;
    }
}
