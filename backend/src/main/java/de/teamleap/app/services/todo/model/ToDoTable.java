package de.teamleap.app.services.todo.model;

import de.teamleap.interfaces.db.JdbcTable;

import java.sql.ResultSet;

public class ToDoTable extends JdbcTable<ToDoRow, Long> {

    @Override
    protected ToDoRow createInstance(ResultSet resultSet) {
        return new ToDoRow(resultSet);
    }

    @Override
    public String getSchemaResourcePath() {
        return "db/schema/todo.sql";
    }

    @Override
    public String getTableName() {
        return "todo";
    }

    @Override
    protected String getPrimaryKeyName() {
        return "id";
    }
}
