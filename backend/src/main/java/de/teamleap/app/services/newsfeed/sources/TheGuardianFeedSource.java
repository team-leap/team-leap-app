package de.teamleap.app.services.newsfeed.sources;

import de.teamleap.app.services.newsfeed.parsers.RssFeedItemParser;
import de.teamleap.app.services.newsfeed.parsers.TheGuardianItemParser;

public class TheGuardianFeedSource extends FeedSource {
    private final static RssFeedItemParser PARSER = new TheGuardianItemParser();

    @Override
    public String getShortId() {
        return "the-guardian";
    }

    @Override
    public String getLanguage() {
        return LANGUAGE_ENGLISH;
    }

    @Override
    public String getSourceUrl() {
        return "https://www.theguardian.com/uk/rss";
    }

    @Override
    public RssFeedItemParser getFeedItemParser() {
        return PARSER;
    }
}
