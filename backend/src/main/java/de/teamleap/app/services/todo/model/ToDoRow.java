package de.teamleap.app.services.todo.model;

import de.teamleap.interfaces.db.Column;
import de.teamleap.interfaces.db.JdbcRow;
import de.teamleap.interfaces.db.PrimaryKey;
import de.teamleap.interfaces.json.FieldToJsonMapper;
import de.teamleap.interfaces.json.JsonField;
import de.teamleap.interfaces.json.JsonToFieldMapper;
import de.teamleap.interfaces.json.Jsonable;
import org.json.JSONObject;

import java.sql.ResultSet;
import java.sql.Timestamp;

public class ToDoRow extends JdbcRow implements Jsonable {

    @PrimaryKey
    @Column(name = "id", notNull = true, includeInInsert = false)
    @JsonField(name = "id", jsonToFieldMapper = JsonToFieldMapper.LONG)
    protected Long id;

    @Column(name = "text", notNull = true)
    @JsonField(name = "text")
    protected String text;

    @Column(name = "done", notNull = true)
    @JsonField(name = "done")
    protected Boolean done;

    @Column(name = "create_timestamp", notNull = true, includeInInsert = false)
    @JsonField(name = "create_timestamp", fieldToJsonMapper = FieldToJsonMapper.NULL_OR_TO_STRING, jsonToFieldMapper = JsonToFieldMapper.TIMESTAMP)
    protected Timestamp createTimestamp;

    @Column(name = "user_name", notNull = true)
    @JsonField(name = "user_name")
    protected String user_name;

    public ToDoRow(ResultSet resultSet) {
        super(resultSet);
    }

    public ToDoRow() {
    }

    @Override
    public String getTableName() {
        return "todo";
    }

    public void setId(Long id) {
        super.changeField("id", id);
    }

    public void setText(String text) {
        super.changeField("text", text);
    }

    public void setDone(Boolean done) {
        super.changeField("done", done);
    }

    public void setCreateTimestamp(Timestamp createTimestamp) {
        super.changeField("create_timestamp", createTimestamp);
    }

    public void setUser_name(String user_name) {
        super.changeField("user_name", user_name);
    }

    public Long getId() {
        return id;
    }

    public String getText() {
        return text;
    }

    public Boolean isDone() {
        return done;
    }

    public Timestamp getCreateTimestamp() {
        return createTimestamp;
    }

    public String getUser_name() {
        return user_name;
    }

    @Override
    public String toString() {
        return toJson().toString();
    }

    @Override
    public Object defaultValueForMissingField(String fieldName) {
        return null;
    }

    public static ToDoRow fromJson(String jsonString) {
        final JSONObject json = new JSONObject(jsonString);
        return Jsonable.fromJson(ToDoRow.class, json);
    }

    public static ToDoRow fromJson(JSONObject json) {
        return Jsonable.fromJson(ToDoRow.class, json);
    }
}
