package de.teamleap.app.services.newsfeed.parsers;

import de.teamleap.app.services.newsfeed.RssFeedEntry;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

public interface RssFeedItemParser {
    RssFeedEntry parse(Element element);

    default String getTagStringValue(String tag, Element element) {
        final NodeList nodeList = element.getElementsByTagName(tag).item(0).getChildNodes();
        Node node = nodeList.item(0);
        return node.getNodeValue();
    }

    /**
     * Converts a string with potentially html content to a clean string.<br>
     * This is done by simply removing specific html tags using regular expressions and their content.<br>
     * <p>Health of flamboyant media tycoon who led three Italian governments had deteriorated markedly in recent years</p><p>The former Italian prime minister Silvio Berlusconi has died aged 86, according to the country’s leading news agency, Ansa.</p><p>The media tycoon, who led three Italian governments between 1994 and 2011 and whose Forza Italia party is a junior partner in the current ruling coalition, had been suffering from leukaemia for some time.</p> <a href="https://www.theguardian.com/world/2023/jun/12/silvio-berlusconi-former-italian-prime-minister-dies">Continue reading...</a>
     * <br>to<br>
     * Health of flamboyant media tycoon who led three Italian governments had deteriorated markedly in recent yearsThe former Italian prime minister Silvio Berlusconi has died aged 86, according to the country’s leading news agency, Ansa.The media tycoon, who led three Italian governments between 1994 and 2011 and whose Forza Italia party is a junior partner in the current ruling coalition, had been suffering from leukaemia for some time.
     *
     * <p><img width="190" height="107" border="0" title="Der frühere italienische Ministerpräsident Silvio Berlusconi, hier am 26. Oktober 2022 in Rom" alt="Der frühere italienische Ministerpräsident Silvio Berlusconi, hier am 26. Oktober 2022 in Rom" src="https://media0.faz.net/ppmedia/aktuell/2312286949/1.8957442/article_teaser/der-fruehere-italienische.jpg" /></p><p>Der frühere italienische Ministerpräsident Silvio Berlusconi ist tot. Er starb am Montag im Alter von 86 Jahren. Wegbegleiter würdigen ihn als „großen Italiener“, der sich um viele Bereiche verdient gemacht habe.</p>
     * <br>to<br>
     * Der frühere italienische Ministerpräsident Silvio Berlusconi ist tot. Er starb am Montag im Alter von 86 Jahren. Wegbegleiter würdigen ihn als „großen Italiener“, der sich um viele Bereiche verdient gemacht habe.
     *
     * @param potentiallyHtmlContentString potentially html content string
     * @return cleaned string
     */
    default String cleanDescription(String potentiallyHtmlContentString) {
        return potentiallyHtmlContentString.replaceAll("<p>", "")
                .replaceAll("</p>", "")
                .replaceAll("<a.*?>[\\s\\S]*?</a>", "")
                .replaceAll("<img.*?>", "");
    }
}
