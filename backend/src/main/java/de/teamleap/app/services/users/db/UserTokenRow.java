package de.teamleap.app.services.users.db;

import de.teamleap.interfaces.db.Column;
import de.teamleap.interfaces.db.JdbcRow;
import de.teamleap.interfaces.db.PrimaryKey;
import de.teamleap.interfaces.json.FieldToJsonMapper;
import de.teamleap.interfaces.json.JsonField;
import de.teamleap.interfaces.json.JsonToFieldMapper;
import de.teamleap.interfaces.json.Jsonable;

import java.sql.ResultSet;
import java.sql.Timestamp;
import java.util.UUID;

public class UserTokenRow extends JdbcRow implements Jsonable {

    @PrimaryKey
    @Column(name = "id", notNull = true)
    @JsonField(name = "id")
    protected Integer id;

    @PrimaryKey
    @Column(name = "name", notNull = true)
    @JsonField(name = "name")
    protected String name;

    @Column(name = "token", notNull = true)
    @JsonField(name = "token", jsonToFieldMapper = JsonToFieldMapper.UUID)
    protected UUID token;

    @Column(name = "created_at", notNull = true, includeInInsert = false)
    @JsonField(name = "created_at", fieldToJsonMapper = FieldToJsonMapper.NULL_OR_TO_STRING, jsonToFieldMapper = JsonToFieldMapper.TIMESTAMP)
    protected Timestamp created_at;

    public UserTokenRow(ResultSet resultSet) {
        super(resultSet);
    }

    public UserTokenRow(String name) {
        this.name = name;
        this.token = UUID.randomUUID();
    }

    public UserTokenRow() {
    }

    public String getName() {
        return name;
    }

    public Timestamp getCreated_at() {
        return created_at;
    }

    public UUID getToken() {
        return token;
    }

    public Integer getId() {
        return id;
    }

    public void setCreated_at(Timestamp created_at) {
        super.changeField("created_at", created_at);
    }

    public void setToken(UUID token) {
        super.changeField("token", token);
    }

    public void setName(String name) {
        super.changeField("name", name);
    }

    public void setId(Integer id) {
        super.changeField("id", id);
    }

    @Override
    public String getTableName() {
        return "users_tokens";
    }

    @Override
    public Object defaultValueForMissingField(String fieldName) {
        return null;
    }
}
