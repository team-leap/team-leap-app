package de.teamleap.app.services.kanban.model;

import de.teamleap.interfaces.db.Column;
import de.teamleap.interfaces.db.JdbcRow;
import de.teamleap.interfaces.db.PrimaryKey;
import de.teamleap.interfaces.json.FieldToJsonMapper;
import de.teamleap.interfaces.json.JsonField;
import de.teamleap.interfaces.json.JsonToFieldMapper;
import de.teamleap.interfaces.json.Jsonable;
import org.json.JSONObject;

import java.sql.ResultSet;
import java.sql.Timestamp;

public class KanbanRow extends JdbcRow implements Jsonable {

    @PrimaryKey
    @Column(name = "id", notNull = true)
    @JsonField(name = "id", jsonToFieldMapper = JsonToFieldMapper.LONG)
    protected Long id;

    @Column(name = "title", notNull = true)
    @JsonField(name = "title")
    protected String title;

    @Column(name = "description")
    @JsonField(name = "description")
    protected String description;

    @Column(name = "status", notNull = true)
    @JsonField(name = "status", jsonToFieldMapper = JsonToFieldMapper.INTEGER)
    protected Integer status;

    @Column(name = "priority", notNull = true)
    @JsonField(name = "priority", jsonToFieldMapper = JsonToFieldMapper.INTEGER)
    protected Integer priority;

    @Column(name = "user_name", notNull = true)
    @JsonField(name = "user_name")
    protected String userName;

    @Column(name = "create_timestamp", notNull = true, includeInInsert = false)
    @JsonField(name = "create_timestamp", fieldToJsonMapper = FieldToJsonMapper.NULL_OR_TO_STRING, jsonToFieldMapper = JsonToFieldMapper.TIMESTAMP)
    protected Timestamp createdTimestamp;

    public KanbanRow(ResultSet resultSet) {
        super(resultSet);
    }

    public KanbanRow() {
    }

    @Override
    public String getTableName() {
        return "kanban";
    }

    public void setId(Long id) {
        super.changeField("id", id);
    }

    public void setTitle(String title) {
        super.changeField("title", title);
    }

    public void setDescription(String description) {
        super.changeField("description", description);
    }

    public void setStatus(Integer status) {
        super.changeField("status", status);
    }

    public void setUserName(String userName) {
        super.changeField("user_name", userName);
    }

    public void setCreatedTimestamp(Timestamp createdTimestamp) {
        super.changeField("create_timestamp", createdTimestamp);
    }

    public void setPriority(Integer priority) {
        super.changeField("priority", priority);
    }

    public Long getId() {
        return id;
    }

    public String getTitle() {
        return title;
    }

    public String getDescription() {
        return description;
    }

    public Integer getStatus() {
        return status;
    }

    public String getUserName() {
        return userName;
    }

    public Timestamp getCreatedTimestamp() {
        return createdTimestamp;
    }

    public Integer getPriority() {
        return priority;
    }

    @Override
    public String toString() {
        return toJson().toString();
    }

    @Override
    public Object defaultValueForMissingField(String fieldName) {
        return null;
    }

    public static KanbanRow fromJson(String jsonString) {
        final JSONObject json = new JSONObject(jsonString);
        return Jsonable.fromJson(KanbanRow.class, json);
    }

    public static KanbanRow fromJson(JSONObject json) {
        return Jsonable.fromJson(KanbanRow.class, json);
    }

    public enum KanbanStatus {
        TODO("To Do"),
        IN_PROGRESS("In Progress"),
        DONE("Done");

        private final String name;

        KanbanStatus(String name) {
            this.name = name;
        }

        public String getName() {
            return name;
        }

        public static KanbanStatus fromName(String name) {
            for (KanbanStatus status : KanbanStatus.values()) {
                if (status.getName().equals(name)) {
                    return status;
                }
            }
            return null;
        }
    }

    public enum KanbanPriority {
        LOW("Low"),
        MEDIUM("Medium"),
        HIGH("High");

        private final String name;

        KanbanPriority(String name) {
            this.name = name;
        }

        public String getName() {
            return name;
        }

        public static KanbanPriority fromName(String name) {
            for (KanbanPriority priority : KanbanPriority.values()) {
                if (priority.getName().equals(name)) {
                    return priority;
                }
            }
            return null;
        }
    }
}
