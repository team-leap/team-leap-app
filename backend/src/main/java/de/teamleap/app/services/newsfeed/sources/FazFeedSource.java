package de.teamleap.app.services.newsfeed.sources;

import de.teamleap.app.services.newsfeed.parsers.FazItemParser;
import de.teamleap.app.services.newsfeed.parsers.RssFeedItemParser;

public class FazFeedSource extends FeedSource {
    private final static RssFeedItemParser PARSER = new FazItemParser();

    @Override
    public String getShortId() {
        return "faz";
    }

    @Override
    public String getLanguage() {
        return LANGUAGE_GERMAN;
    }

    @Override
    public String getSourceUrl() {
        return "https://www.faz.net/rss/aktuell/";
    }

    @Override
    public RssFeedItemParser getFeedItemParser() {
        return PARSER;
    }
}
