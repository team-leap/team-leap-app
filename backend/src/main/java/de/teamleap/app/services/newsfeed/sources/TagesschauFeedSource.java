package de.teamleap.app.services.newsfeed.sources;

import de.teamleap.app.services.newsfeed.parsers.RssFeedItemParser;
import de.teamleap.app.services.newsfeed.parsers.TagesschauItemParser;

public class TagesschauFeedSource extends FeedSource {

    private final static RssFeedItemParser PARSER = new TagesschauItemParser();

    @Override
    public String getShortId() {
        return "tagesschau";
    }

    @Override
    public String getLanguage() {
        return LANGUAGE_GERMAN;
    }

    @Override
    public String getSourceUrl() {
        return "https://www.tagesschau.de/index~rss2.xml";
    }

    @Override
    public RssFeedItemParser getFeedItemParser() {
        return PARSER;
    }
}

