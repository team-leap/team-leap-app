package de.teamleap.app.services.users.db;

import org.json.JSONObject;
import org.springframework.util.ObjectUtils;

import java.util.UUID;

public class InputUser {
    private final String name;
    private final String password;
    private final String newPassword;
    private final UUID token;

    public InputUser(JSONObject input) {
        this.name = firstNonNull(input.optString("name", null), input.optString("username", null));
        this.password = input.optString("password");
        this.newPassword = input.optString("newPassword");
        final String strToken = input.optString("token", null);
        this.token = strToken == null ? null : UUID.fromString(strToken);
    }

    public String getName() {
        return name;
    }

    public String getPassword() {
        return password;
    }

    public String getNewPassword() {
        return newPassword;
    }

    public UUID getToken() {
        return token;
    }

    private static <T> T firstNonNull(T... args) {
        for (T arg : args) {
            if (!ObjectUtils.isEmpty(arg)) {
                return arg;
            }
        }
        return null;
    }
}
