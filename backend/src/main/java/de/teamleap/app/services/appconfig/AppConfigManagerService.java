package de.teamleap.app.services.appconfig;

import de.teamleap.app.services.appconfig.db.AppConfigRow;
import de.teamleap.app.services.appconfig.db.AppConfigTable;
import de.teamleap.other.ApplicationBlockType;
import org.json.JSONArray;
import org.json.JSONObject;

import java.sql.SQLException;
import java.util.List;

public class AppConfigManagerService {

    private final AppConfigTable appConfigTable = new AppConfigTable();

    @Deprecated
    public AppConfigTable getAppConfigTable() {
        return appConfigTable;
    }

    public AppConfigRow registerApplication(ApplicationBlockType context, ApplicationBlockType implementation, ApplicationBlockType addon) throws SQLException {
        final AppConfigRow row = new AppConfigRow();

        row.setContext(context.getId());
        row.setImplementation(implementation.getId());
        row.setAddon(addon == null ? -1 : addon.getId());

        return appConfigTable.insert(row);
    }

    public JSONArray getDashboardData() throws SQLException {
        final List<AppConfigRow> allRows = appConfigTable.getAll();

        final JSONArray result = new JSONArray();
        for (AppConfigRow row : allRows) {
            result.put(new JSONObject()
                    .put("context", row.getContext())
                    .put("implementation", row.getImplementation())
                    .put("addon", row.getAddon())
                    .put("created_at", row.getCreatedAt())
            );
        }

        return result;
    }
}
