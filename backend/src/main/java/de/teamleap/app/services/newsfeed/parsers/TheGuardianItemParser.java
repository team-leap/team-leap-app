package de.teamleap.app.services.newsfeed.parsers;

import de.teamleap.app.services.newsfeed.RssFeedEntry;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;

import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;

public class TheGuardianItemParser implements RssFeedItemParser {

    @Override
    public RssFeedEntry parse(Element element) {
        final RssFeedEntry entry = new RssFeedEntry();

        entry.setTitle(getTagStringValue("title", element));
        entry.setLink(getTagStringValue("link", element));
        entry.setDescription(cleanDescription(getTagStringValue("description", element)));
        entry.setGuid(getTagStringValue("guid", element));

        final String pubDateStr = getTagStringValue("pubDate", element);
        entry.setPubDate(ZonedDateTime.parse(pubDateStr, DateTimeFormatter.RFC_1123_DATE_TIME));

        // <media:content width="460" url="https://i.guim.co.uk/img/media/0202ee415a2842c0454d698a262ac54727baf5f5/0_0_5116_3070/master/5116.jpg?width=460&amp;quality=85&amp;auto=format&amp;fit=max&amp;s=c513e6aaaf8c672af68fb9015b07d533">
        //   <media:credit scheme="urn:ebu">Photograph: Bloomberg/Getty Images</media:credit>
        // </media:content>
        final List<Element> mediaContentElements = getElementsByTagName("media:content", element);
        int maxWidth = 0;
        String thumbnailUrl = null;
        for (Element mediaContentElement : mediaContentElements) {
            final int width = Integer.parseInt(mediaContentElement.getAttribute("width"));
            if (width > maxWidth) {
                maxWidth = width;
                thumbnailUrl = mediaContentElement.getAttribute("url");
            }
        }
        entry.setThumbnail(thumbnailUrl);

        return entry;
    }

    private List<Element> getElementsByTagName(String tagName, Element parent) {
        final List<Element> elements = new ArrayList<>();
        final NodeList nodeList = parent.getElementsByTagName(tagName);
        for (int i = 0; i < nodeList.getLength(); i++) {
            elements.add((Element) nodeList.item(i));
        }
        return elements;
    }
}
