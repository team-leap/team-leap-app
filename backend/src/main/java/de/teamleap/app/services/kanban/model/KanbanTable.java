package de.teamleap.app.services.kanban.model;

import de.teamleap.app.services.todo.model.ToDoRow;
import de.teamleap.interfaces.db.JdbcTable;

import java.sql.ResultSet;

public class KanbanTable extends JdbcTable<KanbanRow, Long> {

    @Override
    protected KanbanRow createInstance(ResultSet resultSet) {
        return new KanbanRow(resultSet);
    }

    @Override
    public String getSchemaResourcePath() {
        return "db/schema/kanban.sql";
    }

    @Override
    public String getTableName() {
        return "kanban";
    }

    @Override
    protected String getPrimaryKeyName() {
        return "id";
    }
}
