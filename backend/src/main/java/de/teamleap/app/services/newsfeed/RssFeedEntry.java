package de.teamleap.app.services.newsfeed;

import de.teamleap.interfaces.json.JsonField;
import de.teamleap.interfaces.json.Jsonable;

import java.time.ZonedDateTime;

public class RssFeedEntry implements Jsonable {

    @JsonField(name = "title")
    private String title;
    @JsonField(name = "link")
    private String link;
    @JsonField(name = "description")
    private String description;
    @JsonField(name = "detailedDescription")
    private String detailedDescription;
    @JsonField(name = "pubDate")
    private ZonedDateTime pubDate;
    @JsonField(name = "guid")
    private String guid;
    @JsonField(name = "thumbnail")
    private String thumbnail;

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getLink() {
        return link;
    }

    public void setLink(String link) {
        this.link = link;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public ZonedDateTime getPubDate() {
        return pubDate;
    }

    public void setPubDate(ZonedDateTime pubDate) {
        this.pubDate = pubDate;
    }

    public String getGuid() {
        return guid;
    }

    public void setGuid(String guid) {
        this.guid = guid;
    }

    public String getDetailedDescription() {
        return detailedDescription;
    }

    public void setDetailedDescription(String detailedDescription) {
        this.detailedDescription = detailedDescription;
    }

    public String getThumbnail() {
        return thumbnail;
    }

    public void setThumbnail(String mediaUrl) {
        this.thumbnail = mediaUrl;
    }

    @Override
    public Object defaultValueForMissingField(String fieldName) {
        return null;
    }
}

