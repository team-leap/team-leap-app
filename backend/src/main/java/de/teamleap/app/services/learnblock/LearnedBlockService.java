package de.teamleap.app.services.learnblock;

import de.teamleap.app.services.learnblock.db.LearnedBlockRow;
import de.teamleap.app.services.learnblock.db.LearnedBlockTable;
import de.teamleap.exception.RDRequestException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.sql.SQLException;
import java.util.List;
import java.util.UUID;

public class LearnedBlockService {

    private static final Logger LOG = LoggerFactory.getLogger(LearnedBlockService.class);

    private final LearnedBlockTable learnedBlockTable = new LearnedBlockTable();

    public boolean registerBlockAsLearned(UUID blockId, int type) {
        LOG.info("Registering block [{}] with type [{}] as learned", blockId, type);

        final boolean alreadyLearned;
        try {
            alreadyLearned = learnedBlockTable.getByPrimaryKey(blockId).isPresent();
        } catch (SQLException e) {
            throw new RDRequestException("Could not check if block is already registered as learned", e);
        }

        if (alreadyLearned) {
            try {
                learnedBlockTable.update(new LearnedBlockRow(blockId, type));
            } catch (SQLException e) {
                throw new RDRequestException("Could not update block as learned", e);
            }

        } else {
            try {
                learnedBlockTable.insert(new LearnedBlockRow(blockId, type));
            } catch (SQLException e) {
                throw new RDRequestException("Could not register block as learned", e);
            }
        }

        return alreadyLearned;
    }

    public LearnedBlockRow getLearnedBlock(UUID blockId) {
        try {
            return learnedBlockTable.getByPrimaryKey(blockId).orElse(null);
        } catch (SQLException e) {
            throw new RDRequestException("Could not get learned block", e);
        }
    }

    public List<LearnedBlockRow> getLearnedBlocks() {
        try {
            return learnedBlockTable.getAll();
        } catch (SQLException e) {
            throw new RDRequestException("Could not get learned blocks", e);
        }
    }
}
