package de.teamleap.app.services.appconfig.db;

import de.teamleap.interfaces.db.Column;
import de.teamleap.interfaces.db.JdbcRow;
import de.teamleap.interfaces.db.PrimaryKey;
import de.teamleap.interfaces.json.FieldToJsonMapper;
import de.teamleap.interfaces.json.JsonField;
import de.teamleap.interfaces.json.JsonToFieldMapper;
import de.teamleap.interfaces.json.Jsonable;
import de.teamleap.other.ApplicationBlockType;
import de.teamleap.other.GeneralUtilities;

import java.sql.ResultSet;
import java.sql.Timestamp;

/**
 * Schema: db/schema/appconfig.sql
 */
public class AppConfigRow extends JdbcRow implements Jsonable {

    @PrimaryKey
    @Column(name = "id", notNull = true, includeInInsert = false)
    @JsonField(name = "id", jsonToFieldMapper = JsonToFieldMapper.LONG)
    protected Integer id;

    @Column(name = "context", notNull = true)
    @JsonField(name = "context")
    protected Integer context;

    @Column(name = "addon", notNull = true)
    @JsonField(name = "addon")
    protected Integer addon;

    @Column(name = "implementation", notNull = true)
    @JsonField(name = "implementation")
    protected Integer implementation;

    @Column(name = "created_at", notNull = true, includeInInsert = false)
    @JsonField(name = "created_at", fieldToJsonMapper = FieldToJsonMapper.NULL_OR_TO_STRING, jsonToFieldMapper = JsonToFieldMapper.TIMESTAMP)
    protected Timestamp created_at;

    public AppConfigRow(ResultSet resultSet) {
        super(resultSet);
    }

    public AppConfigRow() {
    }

    @Override
    public String getTableName() {
        return "appconfig";
    }

    public Integer getId() {
        return id;
    }

    public Integer getContext() {
        return context;
    }

    public Integer getAddon() {
        return addon;
    }

    public Integer getImplementation() {
        return implementation;
    }

    public Timestamp getCreatedAt() {
        return created_at;
    }

    public void setId(Integer id) {
        super.changeField("id", id);
    }

    public void setContext(Integer context) {
        if (!GeneralUtilities.isEqualToOneOf(context, ApplicationBlockType.CONTEXT_BUSINESS.getId(), ApplicationBlockType.CONTEXT_LEISURE.getId())) {
            throw new IllegalArgumentException("context must be one of " + ApplicationBlockType.CONTEXT_BUSINESS + " or " + ApplicationBlockType.CONTEXT_LEISURE + ", but was " + context);
        }
        super.changeField("context", context);
    }

    public void setAddon(Integer addon) {
        if (!GeneralUtilities.isEqualToOneOf(addon, -1, ApplicationBlockType.ADDON_CONVERSATIONAL_AI.getId(), ApplicationBlockType.ADDON_SECURITY.getId())) {
            throw new IllegalArgumentException("addon must be one of -1 or " + ApplicationBlockType.ADDON_CONVERSATIONAL_AI + " or " + ApplicationBlockType.ADDON_SECURITY + ", but was " + addon);
        }
        super.changeField("addon", addon);
    }

    public void setImplementation(Integer implementation) {
        if (!GeneralUtilities.isEqualToOneOf(implementation, ApplicationBlockType.IMPLEMENTATION_PLANNING.getId(), ApplicationBlockType.IMPLEMENTATION_INFORMATION.getId())) {
            throw new IllegalArgumentException("implementation must be one of " + ApplicationBlockType.IMPLEMENTATION_PLANNING + " or " + ApplicationBlockType.IMPLEMENTATION_INFORMATION + ", but was " + implementation);
        }
        super.changeField("implementation", implementation);
    }

    public void setCreatedAt(Timestamp created_at) {
        super.changeField("created_at", created_at);
    }

    @Override
    public Object defaultValueForMissingField(String fieldName) {
        return null;
    }
}
