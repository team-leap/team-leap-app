package de.teamleap.app.services.kanban;

import de.teamleap.app.services.kanban.model.KanbanRow;
import de.teamleap.app.services.kanban.model.KanbanTable;
import de.teamleap.app.services.users.db.UserRow;
import de.teamleap.connection.ws.SocketHandler;
import de.teamleap.exception.RDRequestException;

import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.List;
import java.util.Optional;

public class KanbanService {

    protected final KanbanTable kanbanTable;

    public KanbanService() {
        this.kanbanTable = new KanbanTable();
    }

    public List<KanbanRow> getKanbanRowsByUserName(String userName) throws SQLException {
        return kanbanTable.getByPreparedStatement(connection -> {
            try {
                final PreparedStatement statement = connection.prepareStatement("SELECT * FROM " + kanbanTable.getTableName() + " WHERE user_name = ? ORDER BY create_timestamp DESC");
                statement.setString(1, userName);
                return statement;
            } catch (SQLException e) {
                throw new RuntimeException(e);
            }
        });
    }

    public KanbanRow createKanbanRow(KanbanRow kanbanRow) throws SQLException {
        final KanbanRow inserted = kanbanTable.insert(kanbanRow);
        SocketHandler.getInstance().broadcastActionToUser(SocketHandler.KANBAN_ADDED, inserted.getUserName(), inserted.toJson());
        return inserted;
    }

    public KanbanRow updateKanbanRowById(KanbanRow kanbanRow, UserRow user) throws SQLException {
        final Optional<KanbanRow> potentiallyFoundKanbanRow = kanbanTable.getByPrimaryKey(kanbanRow.getId());

        if (potentiallyFoundKanbanRow.isPresent()) {
            final KanbanRow foundKanbanRow = potentiallyFoundKanbanRow.get();

            if (!foundKanbanRow.getUserName().equals(user.getName())) {
                throw new RDRequestException("Kanban row with id " + kanbanRow.getId() + " does not belong to user " + user.getName());
            }

            if (kanbanRow.getTitle() != null) {
                foundKanbanRow.setTitle(kanbanRow.getTitle());
            }
            if (kanbanRow.getDescription() != null) {
                foundKanbanRow.setDescription(kanbanRow.getDescription());
            }
            if (kanbanRow.getStatus() != null) {
                foundKanbanRow.setStatus(kanbanRow.getStatus());
            }
            if (kanbanRow.getPriority() != null) {
                foundKanbanRow.setPriority(kanbanRow.getPriority());
            }

            kanbanTable.update(foundKanbanRow);
            SocketHandler.getInstance().broadcastActionToUser(SocketHandler.KANBAN_CHANGED, foundKanbanRow.getUserName(), foundKanbanRow.toJson());

            return foundKanbanRow;
        } else {
            throw new RuntimeException("Kanban Row with id " + kanbanRow.getId() + " does not exist");
        }
    }

    public void removeKanbanRowById(KanbanRow kanbanRow) {
        try {
            kanbanTable.delete(kanbanRow);
            SocketHandler.getInstance().broadcastActionToUser(SocketHandler.KANBAN_REMOVED, kanbanRow.getUserName(), kanbanRow.toJson());
        } catch (SQLException e) {
            throw new RuntimeException("Failed to delete kanban row: " + e.getMessage(), e);
        }
    }

    public Optional<KanbanRow> getKanbanRowById(Long id) {
        try {
            return kanbanTable.getByPrimaryKey(id);
        } catch (SQLException e) {
            throw new RuntimeException("Failed to get kanban row: " + e.getMessage(), e);
        }
    }
}
