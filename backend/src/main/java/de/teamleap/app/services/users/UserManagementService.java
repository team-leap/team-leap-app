package de.teamleap.app.services.users;

import de.teamleap.app.services.users.db.UserRow;
import de.teamleap.app.services.users.db.UserTable;
import de.teamleap.app.services.users.db.UserTokenRow;
import de.teamleap.app.services.users.db.UserTokenTable;
import de.teamleap.exception.RDRequestException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.List;
import java.util.UUID;

public class UserManagementService {

    public static final Logger LOG = LoggerFactory.getLogger(UserManagementService.class);

    private static final String DEFAULT_USER_UUID = "825665aa-8018-44ed-bf8d-027cb83a0a35";
    private static final UserRow DEFAULT_USER_ROW = new UserRow("default_user", "default_password");

    private final UserTable userTable = new UserTable();
    private final UserTokenTable tokensTable = new UserTokenTable();

    public void createUser(String name, String password) {
        LOG.info("Creating user [{}]", name);

        if (isDefaultUserByName(name)) {
            throw new RDRequestException("User already exists");
        }

        final UserRow user = new UserRow(name, password);
        try {
            userTable.insert(user);
        } catch (Exception e) {
            throw new RDRequestException("Could not create user", e);
        }
    }

    public void validateUser(String name, String password) {
        if (isDefaultUserByName(name)) {
            if (!isDefaultUserByNameAndPassword(name, password)) {
                throw new RDRequestException("Invalid password");
            } else {
                return;
            }
        }
        try {
            final UserRow user = userTable.getByPrimaryKey(name).orElseThrow(() -> new RDRequestException("User not found"));
            if (!user.checkPassword(password)) {
                throw new RDRequestException("Invalid password");
            }
        } catch (SQLException e) {
            throw new RDRequestException("Could not validate user", e);
        }
    }

    public void validateToken(String name, UUID token) {
        if (isDefaultUserByName(name)) {
            if (!DEFAULT_USER_UUID.equals(token.toString())) {
                throw new RDRequestException("Token is not valid");
            } else {
                return;
            }
        }
        try {
            final List<UserTokenRow> userToken = tokensTable.getByPreparedStatement(connection -> {
                try {
                    final PreparedStatement statement = connection.prepareStatement("SELECT * FROM " + tokensTable.getTableName() + " WHERE name = ? AND token = ?");
                    statement.setString(1, name);
                    statement.setObject(2, token);
                    return statement;
                } catch (SQLException e) {
                    throw new RuntimeException(e);
                }
            });

            if (userToken.isEmpty()) {
                throw new RDRequestException("User not found");
            }
        } catch (SQLException e) {
            throw new RDRequestException("Could not validate user", e);
        }
    }

    public UUID createToken(String name, String password) {
        if (isDefaultUserByName(name)) {
            return UUID.fromString(DEFAULT_USER_UUID);
        }
        try {
            validateUser(name, password);

            final UserTokenRow token = new UserTokenRow(name);
            final UserTokenRow inserted = tokensTable.insert(token);

            return inserted.getToken();

        } catch (SQLException e) {
            throw new RDRequestException("Could not create token for user", e);
        }
    }

    public void changeUserPassword(String name, String newPassword) {
        if (isDefaultUserByName(name)) {
            throw new RDRequestException("Cannot change name of default user");
        }
        if (newPassword == null || newPassword.isEmpty()) {
            throw new RDRequestException("New password must not be empty");
        }
        LOG.info("Changing password for user [{}]", name);

        try {
            final UserRow user = userTable.getByPrimaryKey(name).orElseThrow(() -> new RDRequestException("User not found"));
            user.setPassword(newPassword);
            userTable.update(user);
        } catch (SQLException e) {
            throw new RDRequestException("Could not change password", e);
        }
    }

    public UserRow findUserByToken(String token) {
        if (isDefaultUserByToken(token)) {
            return DEFAULT_USER_ROW;
        }
        try {
            final List<UserTokenRow> userToken = tokensTable.getByPreparedStatement(connection -> {
                try {
                    final PreparedStatement statement = connection.prepareStatement("SELECT * FROM " + tokensTable.getTableName() + " WHERE token = ?");
                    statement.setObject(1, UUID.fromString(token));
                    return statement;
                } catch (SQLException e) {
                    throw new RuntimeException(e);
                }
            });

            if (userToken.isEmpty()) {
                throw new RDRequestException("User not found using token");
            }

            return userTable.getByPrimaryKey(userToken.get(0).getName()).orElseThrow(() -> new RDRequestException("User not found"));
        } catch (SQLException e) {
            throw new RDRequestException("Could not validate user using token", e);
        }
    }

    private boolean isDefaultUserByNameAndPassword(String user, String password) {
        return DEFAULT_USER_ROW.getName().equals(user) && DEFAULT_USER_ROW.checkPassword(password);
    }

    private boolean isDefaultUserByName(String name) {
        return DEFAULT_USER_ROW.getName().equals(name);
    }

    private boolean isDefaultUserByToken(String token) {
        return DEFAULT_USER_UUID.equals(token);
    }
}
