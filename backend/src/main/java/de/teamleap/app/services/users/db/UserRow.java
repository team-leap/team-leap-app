package de.teamleap.app.services.users.db;

import de.teamleap.exception.RDRequestException;
import de.teamleap.interfaces.db.*;
import de.teamleap.interfaces.db.type.ByteData;
import de.teamleap.interfaces.json.JsonField;
import de.teamleap.interfaces.json.Jsonable;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.sql.ResultSet;

public class UserRow extends JdbcRow implements Jsonable {

    @PrimaryKey
    @Column(name = "name", notNull = true)
    @JsonField(name = "name")
    protected String name;

    @Column(name = "password", notNull = true, resultSetToField = DatabaseToFieldMapper.BYTE_DATA, fieldToResultSet = FieldToDatabaseMapper.BYTE_DATA)
    protected ByteData password;

    public UserRow(ResultSet resultSet) {
        super(resultSet);
    }

    public UserRow(String name, String password) {
        this.name = name;
        try {
            this.password = hash(password);
        } catch (NoSuchAlgorithmException e) {
            throw new RDRequestException("Failed to hash password", e);
        }
    }

    public UserRow() {
    }

    public UserRow(String name, byte[] password) {
        this.name = name;
        this.password = new ByteData(password);
    }

    public UserRow(String name, ByteData password) {
        this.name = name;
        this.password = password;
    }

    public String getName() {
        return name;
    }

    public boolean checkPassword(String password) {
        try {
            return this.password.equals(hash(password));
        } catch (NoSuchAlgorithmException e) {
            throw new RDRequestException("Failed to hash password", e);
        } catch (Exception e) {
            throw new RDRequestException("Failed to validate password", e);
        }
    }

    public boolean checkPassword(UserRow password) {
        return this.password.equals(password.password);
    }

    private ByteData hash(String password) throws NoSuchAlgorithmException {
        final MessageDigest md = MessageDigest.getInstance("SHA-256");
        md.update(password.getBytes());

        final byte[] hashedPassword = md.digest();
        return new ByteData(hashedPassword);
    }

    public void setName(String name) {
        super.changeField("name", name);
    }

    public void setPassword(String password) {
        try {
            super.changeField("password", hash(password));
        } catch (NoSuchAlgorithmException e) {
            throw new RDRequestException("Failed to hash password", e);
        }
    }

    @Override
    public String getTableName() {
        return "users";
    }

    @Override
    public Object defaultValueForMissingField(String fieldName) {
        return null;
    }

    @Override
    public String toString() {
        return "UserRow{" +
                "name='" + name + '\'' +
                ", password=" + password +
                '}';
    }
}
