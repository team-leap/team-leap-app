package de.teamleap.app.services.users.db;

import de.teamleap.interfaces.db.JdbcTable;

import java.sql.ResultSet;
import java.util.UUID;

public class UserTable extends JdbcTable<UserRow, String> {

    @Override
    protected UserRow createInstance(ResultSet resultSet) {
        return new UserRow(resultSet);
    }

    @Override
    public String getSchemaResourcePath() {
        return "db/schema/users.sql";
    }

    @Override
    public String getTableName() {
        return "users";
    }

    @Override
    protected String getPrimaryKeyName() {
        return "name";
    }
}
