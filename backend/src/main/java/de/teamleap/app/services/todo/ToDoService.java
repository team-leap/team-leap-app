package de.teamleap.app.services.todo;

import de.teamleap.app.services.todo.model.ToDoRow;
import de.teamleap.app.services.todo.model.ToDoTable;
import de.teamleap.connection.ws.SocketHandler;

import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.List;
import java.util.Optional;

public class ToDoService {

    protected final ToDoTable todoTable;

    public ToDoService() {
        this.todoTable = new ToDoTable();
    }

    public List<ToDoRow> getTodosByUserId(String userName) throws SQLException {
        return todoTable.getByPreparedStatement(connection -> {
            try {
                final PreparedStatement statement = connection.prepareStatement("SELECT * FROM " + todoTable.getTableName() + " WHERE user_name = ? ORDER BY create_timestamp ASC");
                statement.setString(1, userName);
                return statement;
            } catch (SQLException e) {
                throw new RuntimeException(e);
            }
        });
    }

    public ToDoRow createTodo(ToDoRow todo) throws SQLException {
        final ToDoRow inserted = todoTable.insert(todo);
        SocketHandler.getInstance().broadcastActionToUser(SocketHandler.TODO_ADDED, inserted.getUser_name(), inserted.toJson());
        return inserted;
    }

    public ToDoRow updateTodoById(ToDoRow todo) throws SQLException {
        final Optional<ToDoRow> potentiallyFoundToDo = todoTable.getByPrimaryKey(todo.getId());

        if (potentiallyFoundToDo.isPresent()) {
            final ToDoRow foundToDo = potentiallyFoundToDo.get();

            if (todo.getText() != null) {
                foundToDo.setText(todo.getText());
            }
            if (todo.isDone() != null) {
                foundToDo.setDone(todo.isDone());
            }

            todoTable.update(foundToDo);
            SocketHandler.getInstance().broadcastActionToUser(SocketHandler.TODO_CHANGED, foundToDo.getUser_name(), foundToDo.toJson());

            return foundToDo;
        } else {
            throw new RuntimeException("ToDo with id " + todo.getId() + " does not exist");
        }
    }

    public void removeTodoById(ToDoRow todo) {
        try {
            todoTable.delete(todo);
            SocketHandler.getInstance().broadcastActionToUser(SocketHandler.TODO_REMOVED, todo.getUser_name(), todo.toJson());
        } catch (SQLException e) {
            throw new RuntimeException("Failed to delete todo: " + e.getMessage(), e);
        }
    }

    public Optional<ToDoRow> getTodoById(Long id) {
        try {
            return todoTable.getByPrimaryKey(id);
        } catch (SQLException e) {
            throw new RuntimeException("Failed to get todo: " + e.getMessage(), e);
        }
    }
}
