package de.teamleap.app.services.learnblock.db;

import de.teamleap.interfaces.db.Column;
import de.teamleap.interfaces.db.FieldToDatabaseMapper;
import de.teamleap.interfaces.db.JdbcRow;
import de.teamleap.interfaces.db.PrimaryKey;
import de.teamleap.interfaces.json.JsonField;
import de.teamleap.interfaces.json.JsonToFieldMapper;
import de.teamleap.interfaces.json.Jsonable;

import java.sql.ResultSet;
import java.util.UUID;

/**
 * Schema: db/schema/learned_blocks.sql
 */
public class LearnedBlockRow extends JdbcRow implements Jsonable {

    @PrimaryKey
    @Column(name = "id", notNull = true)
    @JsonField(name = "id", jsonToFieldMapper = JsonToFieldMapper.UUID)
    protected UUID id;

    @Column(name = "type", notNull = true)
    @JsonField(name = "type")
    protected Integer type;

    public LearnedBlockRow(ResultSet resultSet) {
        super(resultSet);
    }

    public LearnedBlockRow() {
    }

    public LearnedBlockRow(UUID blockId, int type) {
        this.id = blockId;
        this.type = type;
    }

    public UUID getId() {
        return id;
    }

    public Integer getType() {
        return type;
    }

    public void setId(String id) {
        super.changeField("id", id);
    }

    public void setType(String type) {
        super.changeField("type", type);
    }

    @Override
    public String getTableName() {
        return "learned_blocks";
    }

    @Override
    public Object defaultValueForMissingField(String fieldName) {
        return null;
    }
}
