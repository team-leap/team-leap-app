package de.teamleap.app;

import de.teamleap.app.services.appconfig.AppConfigManagerService;
import de.teamleap.app.services.appconfig.db.AppConfigRow;
import de.teamleap.app.services.kanban.KanbanService;
import de.teamleap.app.services.kanban.model.KanbanRow;
import de.teamleap.app.services.learnblock.LearnedBlockService;
import de.teamleap.app.services.newsfeed.NewsFeedService;
import de.teamleap.app.services.newsfeed.RssFeedEntry;
import de.teamleap.app.services.todo.ToDoService;
import de.teamleap.app.services.todo.model.ToDoRow;
import de.teamleap.app.services.users.UserManagementService;
import de.teamleap.app.services.users.db.InputUser;
import de.teamleap.app.services.users.db.UserRow;
import de.teamleap.connection.db.RDDatabaseHandler;
import de.teamleap.connection.ws.SocketHandler;
import de.teamleap.connection.ws.WebSocketConfig;
import de.teamleap.exception.ExceptionHandlingUtilities;
import de.teamleap.exception.RDRequestException;
import de.teamleap.other.DeviceActiveManager;
import org.json.JSONArray;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Import;
import org.springframework.http.HttpHeaders;
import org.springframework.web.bind.annotation.*;

import java.sql.SQLException;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

import static de.teamleap.exception.ExceptionHandlingUtilities.exceptionResponseHandler;
import static de.teamleap.exception.ExceptionHandlingUtilities.successJson;

@SpringBootApplication
@RestController
@Import({WebSocketConfig.class}) // required for WebSocket support, Spring Boot will not discover this automatically
public class MainController {

    private static final Logger LOG = LoggerFactory.getLogger(MainController.class);

    private final AppConfigManagerService appConfigManagerService;
    private final LearnedBlockService learnedBlockService;
    private final UserManagementService userManagementService;
    private final ToDoService toDoService;
    private final KanbanService kanbanService;
    private final NewsFeedService newsFeedService;

    private final GlobalApplicationStateManager stateManager;

    public MainController() {
        this.appConfigManagerService = new AppConfigManagerService();
        this.learnedBlockService = new LearnedBlockService();
        this.userManagementService = new UserManagementService();
        this.toDoService = new ToDoService();
        this.kanbanService = new KanbanService();
        this.newsFeedService = new NewsFeedService();
        this.newsFeedService.registerDefaultFeedSources();

        // must happen before state manager is initialized
        SocketHandler.initialize(this.userManagementService);

        this.stateManager = new GlobalApplicationStateManager();
        this.stateManager.applicationHasStarted();
    }

    @GetMapping("/")
    public String index() {
        return "Hello World!";
    }

    @GetMapping("/appconfig/{id}")
    @CrossOrigin(origins = "*")
    public String getAppConfigById(@PathVariable Integer id) {
        return exceptionResponseHandler(() -> {
            try {
                final AppConfigRow row = appConfigManagerService.getAppConfigTable().getByPrimaryKey(id)
                        .orElseThrow(() -> new RDRequestException("No app config found for id " + id));

                return ExceptionHandlingUtilities.successJson()
                        .put("appConfig", row.toJson());
            } catch (SQLException e) {
                throw new RDRequestException("Failed to get app config for id " + id, e);
            }
        });
    }

    @GetMapping("/action/button")
    @CrossOrigin(origins = "*")
    public String getActionButtonPressed() {
        return exceptionResponseHandler(() -> {
            stateManager.deployButtonPressed(appConfigManagerService);
            return successJson();
        }, "Failed to process button press");
    }

    @GetMapping("/action/key")
    @CrossOrigin(origins = "*")
    public String getActionKeyPressed() {
        return exceptionResponseHandler(() -> {
            stateManager.deployKeyPressed();
            return successJson();
        }, "Failed to process button press");
    }

    @GetMapping("/action/block/insert/{id}")
    @CrossOrigin(origins = "*")
    public String getActionBlockInserted(@PathVariable String id) {
        return exceptionResponseHandler(() -> {
            if (stateManager.isPrsMode()) {
                throw new RDRequestException("Cannot insert block while prs mode is active");
            }
            stateManager.addActiveBlock(id, learnedBlockService);
            return successJson();
        }, "Failed to insert block " + id);
    }

    @GetMapping("/action/block/remove/{id}")
    @CrossOrigin(origins = "*")
    public String getActionBlockRemoved(@PathVariable String id) {
        return exceptionResponseHandler(() -> {
            if (stateManager.isPrsMode()) {
                throw new RDRequestException("Cannot remove block while prs mode is active");
            }
            stateManager.removeActiveBlock(id, learnedBlockService);
            return successJson();
        }, "Failed to remove block " + id);
    }

    @GetMapping("/action/block/insertprs/{id}")
    @CrossOrigin(origins = "*")
    public String getActionBlockInsertedPrs(@PathVariable String id) {
        return exceptionResponseHandler(() -> {
            stateManager.addActiveBlock(id, learnedBlockService);
            return successJson();
        }, "Failed to insert block " + id);
    }

    @GetMapping("/action/block/removeprs/{id}")
    @CrossOrigin(origins = "*")
    public String getActionBlockRemovedPrs(@PathVariable String id) {
        return exceptionResponseHandler(() -> {
            stateManager.removeActiveBlock(id, learnedBlockService);
            return successJson();
        }, "Failed to remove block " + id);
    }

    @GetMapping("/prs/isactive")
    @CrossOrigin(origins = "*")
    public String getIsPrsActive() {
        return exceptionResponseHandler(() -> {
            return successJson()
                    .put("is_prs_active", stateManager.isPrsMode());
        }, "Failed to get is prs active");
    }

    @GetMapping("/prs/set/{active}")
    @CrossOrigin(origins = "*")
    public String getIsPrsActive(@PathVariable boolean active) {
        return exceptionResponseHandler(() -> {
            stateManager.setPrsMode(active);
            return successJson()
                    .put("is_prs_active", stateManager.isPrsMode());
        }, "Failed to get is prs active");
    }

    @GetMapping("/monitor/state")
    @CrossOrigin(origins = "*")
    public String getMonitorStateInformation(@RequestHeader(name = HttpHeaders.AUTHORIZATION, required = false) String token) {
        return exceptionResponseHandler(() -> {
            try {
                stateManager.assertTokenValid(token);
            } catch (RDRequestException e) {
                return successJson()
                        .put("state", stateManager.toJson(learnedBlockService, false))
                        .put("token_invalid_error", e.getMessage());
            }
            return successJson()
                    .put("state", stateManager.toJson(learnedBlockService, true));
        }, "Failed to get state information");
    }

    @GetMapping("/monitor/register")
    @CrossOrigin(origins = "*")
    public String getMonitorRegistration() {
        return exceptionResponseHandler(() -> {
            if (stateManager.isAdminCardActive()) {
                return successJson().put("token", stateManager.getSecretToken());
            } else {
                throw new RDRequestException("Please place the admin card on the reader to register the monitor");
            }
        }, "Failed to register monitor");
    }

    @GetMapping("/monitor/validate")
    @CrossOrigin(origins = "*")
    public String getMonitorValidateToken(@RequestHeader(name = HttpHeaders.AUTHORIZATION, required = false) String token) {
        return exceptionResponseHandler(() -> {
            stateManager.assertTokenValid(token);
            return successJson();
        }, "Failed to validate token");
    }

    @GetMapping("/monitor/learning")
    @CrossOrigin(origins = "*")
    public String getActivateLearningMode(@RequestHeader(name = HttpHeaders.AUTHORIZATION, required = false) String token) {
        return exceptionResponseHandler(() -> {
            stateManager.assertTokenValid(token);
            stateManager.activateLearningMode();
            return successJson();
        }, "Failed to activate learning mode");
    }

    @GetMapping("/monitor/learning/cancel")
    @CrossOrigin(origins = "*")
    public String getDeactivateLearningMode(@RequestHeader(name = HttpHeaders.AUTHORIZATION, required = false) String token) {
        return exceptionResponseHandler(() -> {
            stateManager.assertTokenValid(token);
            stateManager.deactivateLearningMode();
            return successJson();
        }, "Failed to deactivate learning mode");
    }

    @PostMapping("/auth/user/create")
    @CrossOrigin(origins = "*")
    public String getCreateUser(@RequestBody String requestBody) {
        return exceptionResponseHandler(() -> {
            final InputUser user = new InputUser(new JSONObject(requestBody));
            userManagementService.createUser(user.getName(), user.getPassword());
            return successJson();
        }, "Failed to create user");
    }

    @PostMapping("/auth/user/token/create")
    @CrossOrigin(origins = "*")
    public String getUserCreateAuthToken(@RequestBody String requestBody) {
        return exceptionResponseHandler(() -> {
            final InputUser user = new InputUser(new JSONObject(requestBody));
            return successJson()
                    .put("token", userManagementService.createToken(user.getName(), user.getPassword()));
        }, "Failed to create token for user");
    }

    @PostMapping("/auth/user/token/validate")
    @CrossOrigin(origins = "*")
    public String getValidateUserData(@RequestBody String requestBody) {
        return exceptionResponseHandler(() -> {
            final InputUser user = new InputUser(new JSONObject(requestBody));
            userManagementService.validateToken(user.getName(), user.getToken());
            return successJson();
        }, "Failed to validate user");
    }

    @PostMapping("/auth/user/changepw")
    @CrossOrigin(origins = "*")
    public String getChangeUserPassword(@RequestBody String requestBody) {
        return exceptionResponseHandler(() -> {
            final InputUser user = new InputUser(new JSONObject(requestBody));
            userManagementService.validateUser(user.getName(), user.getPassword());
            userManagementService.changeUserPassword(user.getName(), user.getNewPassword());
            return successJson();
        }, "Failed to validate user");
    }

    @PostMapping("/app/todo/create")
    @CrossOrigin(origins = "*")
    public String getCreateTodo(@RequestBody String requestBody, @RequestHeader(name = HttpHeaders.AUTHORIZATION, required = false) String token) {
        return exceptionResponseHandler(() -> {
            final String text;
            try {
                final JSONObject todoMessageJson = new JSONObject(requestBody);
                text = todoMessageJson.getString("text");
            } catch (Exception e) {
                throw new RDRequestException("Failed to parse request body", e);
            }

            final UserRow user = userManagementService.findUserByToken(token);
            final ToDoRow todo = new ToDoRow();
            todo.setUser_name(user.getName());
            todo.setText(text);
            todo.setDone(false);

            try {
                final ToDoRow inserted = toDoService.createTodo(todo);
                return successJson()
                        .put("todo", inserted.toJson());
            } catch (SQLException e) {
                throw new RDRequestException("Failed to create todo", e);
            }
        }, "Failed to create todo");
    }

    @GetMapping("/app/todo/delete/{id}")
    @CrossOrigin(origins = "*")
    public String getDeleteTodo(@RequestHeader(name = HttpHeaders.AUTHORIZATION, required = false) String token, @PathVariable Long id) {
        return exceptionResponseHandler(() -> {
            final UserRow user = userManagementService.findUserByToken(token);

            final ToDoRow dbRow = toDoService.getTodoById(id).orElseThrow(() -> new RDRequestException("Todo with id " + id + " not found"));
            if (!dbRow.getUser_name().equals(user.getName())) {
                throw new RDRequestException("Todo with id " + id + " does not belong to user " + user.getName());
            }

            toDoService.removeTodoById(dbRow);
            return successJson();
        }, "Failed to delete todo");
    }

    @GetMapping("/app/todo/list")
    @CrossOrigin(origins = "*")
    public String getTodoList(@RequestHeader(name = HttpHeaders.AUTHORIZATION, required = false) String token) {
        return exceptionResponseHandler(() -> {
            final UserRow user = userManagementService.findUserByToken(token);
            try {
                final List<ToDoRow> todoList = toDoService.getTodosByUserId(user.getName());
                return successJson()
                        .put("todos", todoList.stream().map(ToDoRow::toJson).collect(Collectors.toList()));
            } catch (SQLException e) {
                throw new RDRequestException("Failed to get todo list", e);
            }
        }, "Failed to get todo list");
    }

    @PostMapping("/app/todo/update/{id}")
    @CrossOrigin(origins = "*")
    public String getUpdateTodoDone(@RequestHeader(name = HttpHeaders.AUTHORIZATION, required = false) String token, @PathVariable Long id, @RequestBody String requestBody) {
        return exceptionResponseHandler(() -> {
            final UserRow user = userManagementService.findUserByToken(token);

            final ToDoRow dbRow = toDoService.getTodoById(id).orElseThrow(() -> new RDRequestException("Todo with id " + id + " not found"));
            if (!dbRow.getUser_name().equals(user.getName())) {
                throw new RDRequestException("Todo with id " + id + " does not belong to user " + user.getName());
            }

            final JSONObject todoMessageJson = new JSONObject(requestBody);
            if (todoMessageJson.has("done")) {
                final boolean done = todoMessageJson.getBoolean("done");
                dbRow.setDone(done);
            }

            if (todoMessageJson.has("text")) {
                final String text = todoMessageJson.getString("text");
                dbRow.setText(text);
            }

            try {
                toDoService.updateTodoById(dbRow);
            } catch (SQLException e) {
                throw new RDRequestException("Failed to update todo", e);
            }
            return successJson();
        }, "Failed to update todo");
    }

    @PostMapping("/app/kanban/create")
    @CrossOrigin(origins = "*")
    public String getCreateKanbanRow(@RequestBody String requestBody, @RequestHeader(name = HttpHeaders.AUTHORIZATION, required = false) String token) {
        return exceptionResponseHandler(() -> {
            KanbanRow inputRow;
            try {
                inputRow = KanbanRow.fromJson(new JSONObject(requestBody));
            } catch (Exception e) {
                throw new RDRequestException("Failed to parse request body", e);
            }

            final UserRow user = userManagementService.findUserByToken(token);
            inputRow.setUserName(user.getName());
            if (inputRow.getStatus() == null) {
                inputRow.setStatus(0);
            }

            try {
                final KanbanRow inserted = kanbanService.createKanbanRow(inputRow);
                return successJson()
                        .put("kanbanRow", inserted.toJson());
            } catch (SQLException e) {
                throw new RDRequestException("Failed to create kanban row", e);
            }
        }, "Failed to create kanban row");
    }

    @GetMapping("/app/kanban/delete/{id}")
    @CrossOrigin(origins = "*")
    public String getDeleteKanbanRow(@RequestHeader(name = HttpHeaders.AUTHORIZATION, required = false) String token, @PathVariable Long id) {
        return exceptionResponseHandler(() -> {
            final UserRow user = userManagementService.findUserByToken(token);

            final KanbanRow dbRow = kanbanService.getKanbanRowById(id).orElseThrow(() -> new RDRequestException("Kanban row with id " + id + " not found"));
            if (!dbRow.getUserName().equals(user.getName())) {
                throw new RDRequestException("Kanban row with id " + id + " does not belong to user " + user.getName());
            }

            kanbanService.removeKanbanRowById(dbRow);
            return successJson();
        }, "Failed to delete kanban row");
    }

    @GetMapping("/app/kanban/list")
    @CrossOrigin(origins = "*")
    public String getKanbanList(@RequestHeader(name = HttpHeaders.AUTHORIZATION, required = false) String token) {
        return exceptionResponseHandler(() -> {
            final UserRow user = userManagementService.findUserByToken(token);
            try {
                final List<KanbanRow> kanbanList = kanbanService.getKanbanRowsByUserName(user.getName());
                return successJson()
                        .put("kanbanRows", kanbanList.stream().map(KanbanRow::toJson).collect(Collectors.toList()));
            } catch (SQLException e) {
                throw new RDRequestException("Failed to get kanban list", e);
            }
        }, "Failed to get kanban list");
    }

    @GetMapping("/app/kanban/categories")
    @CrossOrigin(origins = "*")
    public String getKanbanCategories() {
        return exceptionResponseHandler(() -> successJson()
                        .put("columns", new JSONArray(Arrays.stream(KanbanRow.KanbanStatus.values()).map(KanbanRow.KanbanStatus::getName).collect(Collectors.toList()))),
                "Failed to get kanban categories");
    }

    @PostMapping("/app/kanban/update/{id}")
    @CrossOrigin(origins = "*")
    public String getUpdateKanbanRow(@RequestHeader(name = HttpHeaders.AUTHORIZATION, required = false) String token, @PathVariable Long id, @RequestBody String requestBody) {
        LOG.info("Update kanban with id [{}] and request body [{}]", id, requestBody);
        return exceptionResponseHandler(() -> {
            final UserRow user = userManagementService.findUserByToken(token);

            final JSONObject kanbanRowMessageJson = new JSONObject(requestBody);
            final KanbanRow inputRow = KanbanRow.fromJson(kanbanRowMessageJson);
            inputRow.setId(id);

            try {
                kanbanService.updateKanbanRowById(inputRow, user);
            } catch (SQLException e) {
                throw new RDRequestException("Failed to update kanban row", e);
            }
            return successJson();
        }, "Failed to update kanban row");
    }

    @GetMapping("/app/news/list")
    @CrossOrigin(origins = "*")
    public String getNewsProviders() {
        return exceptionResponseHandler(() -> successJson().put("newsProviders", new JSONObject(newsFeedService.getRegisteredFeedSourceIds())),
                "Failed to get news providers");
    }

    @GetMapping("/app/news/list/{providerId}")
    @CrossOrigin(origins = "*")
    public String getNewsList(@PathVariable String providerId) {
        return exceptionResponseHandler(() -> {
            try {
                final List<RssFeedEntry> newsList = newsFeedService.fetchFeed(providerId);
                final JSONArray newsListJson = new JSONArray();
                for (RssFeedEntry news : newsList) {
                    newsListJson.put(news.toJson());
                }
                return successJson()
                        .put("newsList", newsListJson);
            } catch (Exception e) {
                throw new RDRequestException("Failed to get news list", e);
            }
        }, "Failed to get news list");
    }

    @GetMapping("/app/dashboard/data")
    @CrossOrigin(origins = "*")
    public String getDashboardData() {
        return exceptionResponseHandler(() -> {
            try {
                return successJson()
                        .put("apps", this.appConfigManagerService.getDashboardData());
            } catch (SQLException e) {
                throw new RDRequestException("Failed to get dashboard data", e);
            }
        }, "Failed to get dashboard data");
    }

    @GetMapping("/device/heartbeat/{deviceId}")
    @CrossOrigin(origins = "*")
    public String getDeviceHeartbeat(@PathVariable String deviceId) {
        return exceptionResponseHandler(() -> {
            switch (deviceId) {
                case DeviceActiveManager.DEVICE_RASPBERRY_PI:
                    this.stateManager.rasPiHeartbeat();
                    break;
                default:
                    throw new RDRequestException("Unknown device id " + deviceId);
            }
            return successJson();
        }, "Failed to update device heartbeat for " + deviceId);
    }

    @GetMapping("/admin/resetdb")
    @CrossOrigin(origins = "*")
    public String getAdminDbDelete() {
        return exceptionResponseHandler(() -> {
            try {
                RDDatabaseHandler.getConnectionProvider().connection().createStatement().execute("DROP TABLE IF EXISTS learned_blocks");
                // RDDatabaseHandler.getConnectionProvider().connection().createStatement().execute("DROP TABLE IF EXISTS appconfig");
                RDDatabaseHandler.getConnectionProvider().connection().createStatement().execute("DROP TABLE IF EXISTS todo");
                RDDatabaseHandler.getConnectionProvider().connection().createStatement().execute("DROP TABLE IF EXISTS users");
                RDDatabaseHandler.getConnectionProvider().connection().createStatement().execute("DROP TABLE IF EXISTS users_tokens");
                RDDatabaseHandler.getConnectionProvider().connection().createStatement().execute("DROP TABLE IF EXISTS kanban");
            } catch (SQLException e) {
                throw new RDRequestException("Failed to delete database", e);
            }
            return successJson();
        });
    }
}
