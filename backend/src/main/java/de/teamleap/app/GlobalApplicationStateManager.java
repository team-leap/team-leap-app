package de.teamleap.app;

import de.teamleap.app.services.appconfig.AppConfigManagerService;
import de.teamleap.app.services.appconfig.db.AppConfigRow;
import de.teamleap.app.services.learnblock.LearnedBlockService;
import de.teamleap.app.services.learnblock.db.LearnedBlockRow;
import de.teamleap.config.RDConfig;
import de.teamleap.connection.ws.SocketHandler;
import de.teamleap.exception.RDRequestException;
import de.teamleap.interfaces.json.Jsonable;
import de.teamleap.other.ApplicationBlockType;
import de.teamleap.other.ApplicationStates;
import de.teamleap.other.DeviceActiveManager;
import de.teamleap.other.GeneralUtilities;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.*;
import java.util.stream.Collectors;


public class GlobalApplicationStateManager {

    private static final Logger LOG = LoggerFactory.getLogger(GlobalApplicationStateManager.class);

    private final static UUID[] ADMIN_CARD_UUIDS = RDConfig.getAdminCardUUIDs();

    private ApplicationStates currentState = ApplicationStates.INITIALIZING;

    private final String secretToken = UUID.randomUUID().toString();
    private boolean tokenAlreadyUsed = false;

    private final List<ApplicationBlockType> activeBlocks = new ArrayList<>();

    private int learningBlockStage = -1;
    private int successiveDeployButtonPresses = 0;

    private final DeviceActiveManager deviceActiveManager = new DeviceActiveManager();

    private boolean isPrsMode = false;

    public GlobalApplicationStateManager() {
        deviceActiveManager.onDeviceActive(device -> SocketHandler.getInstance().broadcastAction(SocketHandler.DEVICE_STATE_CHANGED, device, SocketHandler.DEVICE_STATE_CHANGED_CHANGE_ONLINE, true));
        deviceActiveManager.onDeviceInactive(device -> SocketHandler.getInstance().broadcastAction(SocketHandler.DEVICE_STATE_CHANGED, device, SocketHandler.DEVICE_STATE_CHANGED_CHANGE_ONLINE, false));

        deviceActiveManager.onDeviceActive(device -> LOG.info("Device [{}] is now active", device));
        deviceActiveManager.onDeviceInactive(device -> LOG.info("Device [{}] is now inactive", device));
    }

    private void setCurrentState(ApplicationStates currentState) {
        this.currentState = currentState;
        LOG.info("Switching to state [{}]", currentState);
    }

    public void activateLearningMode() {
        if (currentState == ApplicationStates.LEARNING) {
            throw new RDRequestException("Application is already in learning mode");
        } else if (currentState != ApplicationStates.BUILDING_WAITING) {
            throw new RDRequestException("Application is not in " + ApplicationStates.BUILDING_WAITING + " state");
        } else if (!activeBlocks.isEmpty()) {
            throw new RDRequestException("There are still active blocks, remove all blocks before activating learning mode");
        }

        setCurrentState(ApplicationStates.LEARNING);
        learningBlockStage = 0;

        SocketHandler.getInstance().broadcastAction(SocketHandler.SHOW_LEARNING_BLOCK, ApplicationBlockType.NON_ADMIN_TYPES[0].getId());
    }

    public void deactivateLearningMode() {
        if (currentState != ApplicationStates.LEARNING) {
            throw new RDRequestException("Application is already in learning mode");
        } else if (!activeBlocks.isEmpty()) {
            throw new RDRequestException("There are still active blocks, remove all blocks before deactivating learning mode");
        }

        throw new RDRequestException("Cannot deactivate learning mode, not yet implemented");

        // setCurrentState(ApplicationStates.BUILDING_WAITING);
        // learningBlockStage = -1;
    }

    public void applicationHasStarted() {
        if (currentState != ApplicationStates.INITIALIZING) {
            throw new RDRequestException("Application is not in " + ApplicationStates.INITIALIZING + " state");
        }

        setCurrentState(ApplicationStates.BUILDING_WAITING);
        LOG.info("Application has started, switching to state [{}]", currentState);
    }

    public void addActiveBlock(String id, LearnedBlockService learnedBlockService) {
        final UUID blockUUID = makeBlockUUID(id);

        if (isAdminCardUUID(blockUUID)) {
            if (!activeBlocks.contains(ApplicationBlockType.ADMIN_CARD)) {
                activeBlocks.add(ApplicationBlockType.ADMIN_CARD);
                LOG.info("Added block with id [{}] --> [{}], blocks: {}", id, blockUUID, activeBlocks);
            }
            SocketHandler.getInstance().broadcastAction(SocketHandler.STATE_CHANGED, SocketHandler.STATE_CHANGED_BLOCK_ADDED, ApplicationBlockType.ADMIN_CARD.getId());
            return;
        }

        if (currentState == ApplicationStates.LEARNING) {
            if (activeBlocks.size() >= 1) {
                SocketHandler.getInstance().broadcastAction(SocketHandler.MONITOR_SHOW_MESSAGE, SocketHandler.MONITOR_SHOW_MESSAGE_LEARNING_REMOVE_BLOCK_FIRST);
                throw new RDRequestException("LEARNING: Cannot learn new blocks while other blocks are active");
            }

            learningStateHandler(blockUUID, learnedBlockService);
        }

        final LearnedBlockRow learnedBlock = learnedBlockService.getLearnedBlock(blockUUID);
        if (learnedBlock != null) {
            final ApplicationBlockType block = ApplicationBlockType.fromType(learnedBlock.getType());
            if (block == null) {
                SocketHandler.getInstance().broadcastAction(SocketHandler.MONITOR_SHOW_MESSAGE, SocketHandler.MONITOR_SHOW_MESSAGE_UNKNOWN_BLOCK_TYPE);
                throw new RDRequestException("Unknown block type");
            }

            if (!activeBlocks.contains(block)) {
                activeBlocks.add(block);
            }
            if (currentState == ApplicationStates.BUILDING_WAITING) {
                setCurrentState(ApplicationStates.BUILDING_RUNNING);
            } else if (currentState == ApplicationStates.APP_DEPLOYED) {
                SocketHandler.getInstance().broadcastAction(SocketHandler.MONITOR_SHOW_MESSAGE, SocketHandler.MONITOR_SHOW_MESSAGE_APP_DEPLOYED_REMOVE_ALL_BLOCKS_FIRST);

                throw new RDRequestException("Cannot add blocks while application is deployed");
            }

            String errorMessage = "";
            try {
                assertValidConfiguration(false);
            } catch (RDRequestException e) {
                errorMessage = e.getMessage();
            }

            SocketHandler.getInstance().broadcastAction(SocketHandler.STATE_CHANGED, SocketHandler.STATE_CHANGED_BLOCK_ADDED, block.getId(), errorMessage);

            LOG.info("Added block with id [{}] --> [{}], blocks: {}", id, blockUUID, activeBlocks);

        } else {
            SocketHandler.getInstance().broadcastAction(SocketHandler.MONITOR_SHOW_MESSAGE, SocketHandler.MONITOR_SHOW_MESSAGE_BLOCK_NOT_LEARNED);
            throw new RDRequestException("Block with id [" + id + "] is not registered");
        }
    }

    public void removeActiveBlock(String id, LearnedBlockService learnedBlockService) {
        final UUID blockUUID = makeBlockUUID(id);

        if (isAdminCardUUID(blockUUID)) {
            while (activeBlocks.contains(ApplicationBlockType.ADMIN_CARD)) {
                activeBlocks.remove(ApplicationBlockType.ADMIN_CARD);
            }
            SocketHandler.getInstance().broadcastAction(SocketHandler.STATE_CHANGED, SocketHandler.STATE_CHANGED_BLOCK_REMOVED, ApplicationBlockType.ADMIN_CARD.getId());
            return;
        }

        final LearnedBlockRow learnedBlock = learnedBlockService.getLearnedBlock(blockUUID);
        if (learnedBlock != null) {
            final ApplicationBlockType block = ApplicationBlockType.fromType(learnedBlock.getType());
            if (block == null) {
                SocketHandler.getInstance().broadcastAction(SocketHandler.MONITOR_SHOW_MESSAGE, SocketHandler.MONITOR_SHOW_MESSAGE_UNKNOWN_BLOCK_TYPE);
                throw new RDRequestException("Unknown block type");
            }


            if (currentState == ApplicationStates.LEARNING && learningBlockStage >= ApplicationBlockType.NON_ADMIN_TYPES.length) {
                LOG.info("LEARNING: All blocks learned, switching to state [{}]", ApplicationStates.BUILDING_WAITING);
                setCurrentState(ApplicationStates.BUILDING_WAITING);

                SocketHandler.getInstance().broadcast(new JSONObject()
                        .put("action", "stopLearning")
                        .toString());
            }

            if (!activeBlocks.remove(block)) {
                SocketHandler.getInstance().broadcastAction(SocketHandler.MONITOR_SHOW_MESSAGE, SocketHandler.MONITOR_SHOW_MESSAGE_BLOCK_NOT_ACTIVE);
                throw new RDRequestException("Block with id [" + id + "] is not active");
            }

            while (activeBlocks.remove(block)) {
            }

            if (currentState == ApplicationStates.BUILDING_RUNNING) {
                if (successiveDeployButtonPresses > 0) {
                    SocketHandler.getInstance().broadcastAction(SocketHandler.STATE_CHANGED, SocketHandler.STATE_CHANGED_BUTTON_PRESSED, 0);
                }
                successiveDeployButtonPresses = 0;
            }

            if (activeBlocks.isEmpty() && currentState == ApplicationStates.BUILDING_RUNNING) {
                setCurrentState(ApplicationStates.BUILDING_WAITING);
            } else if (activeBlocks.isEmpty() && currentState == ApplicationStates.APP_DEPLOYED) {
                setCurrentState(ApplicationStates.BUILDING_WAITING);

                SocketHandler.getInstance().broadcastAction(SocketHandler.STATE_CHANGED, SocketHandler.STATE_CHANGED_ALL_BLOCK_REMOVED);
            }

            String errorMessage = "";
            try {
                assertValidConfiguration(false);
            } catch (RDRequestException e) {
                errorMessage = e.getMessage();
            }

            SocketHandler.getInstance().broadcastAction(SocketHandler.STATE_CHANGED, SocketHandler.STATE_CHANGED_BLOCK_REMOVED, block.getId(), errorMessage);

            LOG.info("Removed block with id [{}] --> [{}], blocks: {}", id, blockUUID, activeBlocks);

        } else {
            SocketHandler.getInstance().broadcastAction(SocketHandler.MONITOR_SHOW_MESSAGE, SocketHandler.MONITOR_SHOW_MESSAGE_BLOCK_NOT_LEARNED);
            throw new RDRequestException("Block with id [" + id + "] is not registered as learned");
        }
    }

    private void learningStateHandler(UUID uuid, LearnedBlockService learnedBlockService) {
        final ApplicationBlockType currentBlock = ApplicationBlockType.NON_ADMIN_TYPES[learningBlockStage];
        final boolean alreadyLearned = learnedBlockService.registerBlockAsLearned(uuid, currentBlock.getId());

        learningBlockStage = learningBlockStage + 1;

        if (learningBlockStage < ApplicationBlockType.NON_ADMIN_TYPES.length) {
            final ApplicationBlockType nextBlock = ApplicationBlockType.NON_ADMIN_TYPES[learningBlockStage];
            LOG.info("LEARNING: Switching to block [{}]", nextBlock);

            SocketHandler.getInstance().broadcast(new JSONObject()
                    .put("action", "showLearningBlock")
                    .put("type", nextBlock.getId())
                    .put("alreadyLearned", alreadyLearned)
                    .toString());
        }
    }

    public static UUID makeBlockUUID(String seed) {
        return UUID.nameUUIDFromBytes(seed.getBytes());
    }

    public String getSecretToken() {
        if (tokenAlreadyUsed) {
            throw new RDRequestException("Token already used, restart application to get new token");
        } else {
            tokenAlreadyUsed = true;
            return secretToken;
        }
    }

    public boolean validateToken(String token) {
        return secretToken.equals(token);
    }

    public void assertTokenValid(String token) {
        if (!validateToken(token)) {
            throw new RDRequestException("Invalid token");
        }
    }

    public boolean isAdminCardActive() {
        return activeBlocks.contains(ApplicationBlockType.ADMIN_CARD);
    }

    private boolean isAdminCardUUID(UUID uuid) {
        return Arrays.asList(ADMIN_CARD_UUIDS).contains(uuid);
    }

    public void deployButtonPressed(AppConfigManagerService appConfigService) {
        if (currentState == ApplicationStates.APP_DEPLOYED) {
            return;
        }
        try {
            assertValidConfiguration(true);
        } catch (Exception e) {
            if (successiveDeployButtonPresses > 0) {
                SocketHandler.getInstance().broadcastAction(SocketHandler.STATE_CHANGED, SocketHandler.STATE_CHANGED_BUTTON_PRESSED, 0);
            }
            successiveDeployButtonPresses = 0;
            throw e;
        }

        switch (successiveDeployButtonPresses) {
            case 0:
                SocketHandler.getInstance().broadcastAction(SocketHandler.MONITOR_SHOW_MESSAGE, SocketHandler.MONITOR_SHOW_MESSAGE_ROTATE_KEY_TO_CONFIRM);
                break;
            case 1:
                successiveDeployButtonPresses = 0;
                deployApplication(appConfigService);
                break;
            default:
                successiveDeployButtonPresses = 0;
                SocketHandler.getInstance().broadcastAction(SocketHandler.MONITOR_SHOW_MESSAGE, SocketHandler.MONITOR_SHOW_MESSAGE_INVALID_BUTTON_PRESSED_COUNT);
                SocketHandler.getInstance().broadcastAction(SocketHandler.STATE_CHANGED, SocketHandler.STATE_CHANGED_BUTTON_PRESSED, 0);
                throw new RDRequestException("Invalid button press count, resetting to 0");
        }
    }

    public void deployKeyPressed() {
        if (currentState == ApplicationStates.APP_DEPLOYED) {
            return;
        }
        try {
            assertValidConfiguration(true);
        } catch (Exception e) {
            if (successiveDeployButtonPresses > 0) {
                SocketHandler.getInstance().broadcastAction(SocketHandler.STATE_CHANGED, SocketHandler.STATE_CHANGED_BUTTON_PRESSED, 0);
            }
            successiveDeployButtonPresses = 0;
            throw e;
        }

        switch (successiveDeployButtonPresses) {
            case 0:
                successiveDeployButtonPresses = 1;
                SocketHandler.getInstance().broadcastAction(SocketHandler.STATE_CHANGED, SocketHandler.STATE_CHANGED_BUTTON_PRESSED, 1);
                break;
            case 1:
                SocketHandler.getInstance().broadcastAction(SocketHandler.MONITOR_SHOW_MESSAGE, SocketHandler.MONITOR_SHOW_MESSAGE_PRESS_BUTTON_TO_DEPLOY);
                break;
            default:
                successiveDeployButtonPresses = 0;
                SocketHandler.getInstance().broadcastAction(SocketHandler.MONITOR_SHOW_MESSAGE, SocketHandler.MONITOR_SHOW_MESSAGE_INVALID_BUTTON_PRESSED_COUNT);
                SocketHandler.getInstance().broadcastAction(SocketHandler.STATE_CHANGED, SocketHandler.STATE_CHANGED_BUTTON_PRESSED, 0);
                throw new RDRequestException("Invalid button press count, resetting to 0");
        }
    }

    private void assertValidConfiguration(boolean tellFrontend) {
        final Map<String, List<ApplicationBlockType>> blocksInCategories = getSortedActiveBlocks();

        final int implementationBlockCount = blocksInCategories.getOrDefault(GeneralUtilities.CATEGORY_IMPLEMENTATION, Collections.emptyList()).size();
        final int contextBlockCount = blocksInCategories.getOrDefault(GeneralUtilities.CATEGORY_CONTEXT, Collections.emptyList()).size();
        final int addonBlockCount = blocksInCategories.getOrDefault(GeneralUtilities.CATEGORY_ADDON, Collections.emptyList()).size();
        final int adminBlockCount = blocksInCategories.getOrDefault(GeneralUtilities.CATEGORY_ADMIN, Collections.emptyList()).size();

        if (implementationBlockCount == 0) {
            invalidConfigurationException(SocketHandler.MONITOR_INVALID_CONFIGURATION_NO_IMPLEMENTATION_BLOCK, tellFrontend);
        } else if (implementationBlockCount > 1) {
            invalidConfigurationException(SocketHandler.MONITOR_INVALID_CONFIGURATION_TOO_MANY_IMPLEMENTATION_BLOCKS, tellFrontend);
        }
        if (contextBlockCount == 0) {
            invalidConfigurationException(SocketHandler.MONITOR_INVALID_CONFIGURATION_NO_CONTEXT_BLOCK, tellFrontend);
        } else if (contextBlockCount > 1) {
            invalidConfigurationException(SocketHandler.MONITOR_INVALID_CONFIGURATION_TOO_MANY_CONTEXT_BLOCKS, tellFrontend);
        }
        if (addonBlockCount == 0) {
            invalidConfigurationException(SocketHandler.MONITOR_INVALID_CONFIGURATION_NO_ADDON_BLOCK, tellFrontend);
        } else if (addonBlockCount > 1) {
            invalidConfigurationException(SocketHandler.MONITOR_INVALID_CONFIGURATION_TOO_MANY_ADDON_BLOCKS, tellFrontend);
        }

        if (adminBlockCount > 0) {
            invalidConfigurationException(SocketHandler.MONITOR_INVALID_CONFIGURATION_TOO_MANY_ADMIN_BLOCKS, tellFrontend);
        }
    }

    private void invalidConfigurationException(String message, boolean tellFrontend) {
        if (tellFrontend) {
            SocketHandler.getInstance().broadcastAction(SocketHandler.MONITOR_INVALID_CONFIGURATION, message);
        }
        throw new RDRequestException(message);
    }

    private void deployApplication(AppConfigManagerService appConfigService) {
        final Map<String, List<ApplicationBlockType>> blocksInCategories = getSortedActiveBlocks();
        try {
            final ApplicationBlockType implementationBlock = blocksInCategories.get(GeneralUtilities.CATEGORY_IMPLEMENTATION).get(0);
            final ApplicationBlockType contextBlock = blocksInCategories.get(GeneralUtilities.CATEGORY_CONTEXT).get(0);
            final ApplicationBlockType addonBlock = blocksInCategories.get(GeneralUtilities.CATEGORY_ADDON) != null
                    ? blocksInCategories.get(GeneralUtilities.CATEGORY_ADDON).get(0)
                    : null;

            final AppConfigRow row = appConfigService.registerApplication(contextBlock, implementationBlock, addonBlock);

            SocketHandler.getInstance().broadcastAction(SocketHandler.STATE_CHANGED, SocketHandler.STATE_CHANGED_APP_DEPLOYED, row.toJson());

            setCurrentState(ApplicationStates.APP_DEPLOYED);
        } catch (Exception e) {
            throw new RDRequestException("Cannot deploy application, error while registering application", e);
        }
    }

    private Map<String, List<ApplicationBlockType>> getSortedActiveBlocks() {
        final Map<String, List<ApplicationBlockType>> blocksInCategories = new HashMap<>();
        for (ApplicationBlockType activeBlock : activeBlocks) {
            blocksInCategories.computeIfAbsent(activeBlock.getType(), e -> new ArrayList<>()).add(activeBlock);
        }
        return blocksInCategories;
    }

    public void rasPiHeartbeat() {
        deviceActiveManager.updateDeviceLastSeen(DeviceActiveManager.DEVICE_RASPBERRY_PI);
    }

    public JSONObject toJson(LearnedBlockService learnedBlockService, boolean authorized) {
        final JSONObject response = new JSONObject();

        if (authorized) {
            response.put("state", currentState);
            response.put("activeBlocks", activeBlocks.stream().map(ApplicationBlockType::getId).collect(Collectors.toList()));
            response.put("successiveDeployButtonPresses", successiveDeployButtonPresses);

            if (learningBlockStage >= 0 && learningBlockStage < ApplicationBlockType.NON_ADMIN_TYPES.length) {
                final ApplicationBlockType nextBlock = ApplicationBlockType.NON_ADMIN_TYPES[learningBlockStage];
                response.put("learningBlock", nextBlock.getId());

                final List<LearnedBlockRow> learnedBlocks = learnedBlockService.getLearnedBlocks();
                response.put("learnedBlocks", learnedBlocks.stream()
                        .map(Jsonable::toJson)
                        .collect(Collectors.toList()));

            } else {
                response.put("learningBlock", -1);
                response.put("learnedBlocks", Collections.emptyList());
            }

            String errorMessage = "";
            try {
                assertValidConfiguration(false);
            } catch (RDRequestException e) {
                errorMessage = e.getMessage();
            }
            response.put("invalidConfiguration", errorMessage);
        }

        response.put("devices", new JSONObject()
                .put(DeviceActiveManager.DEVICE_RASPBERRY_PI, new JSONObject()
                        .put("active", deviceActiveManager.isDeviceActive(DeviceActiveManager.DEVICE_RASPBERRY_PI)))
        );
        response.put("adminCard", isAdminCardActive());

        return response;
    }

    public void setPrsMode(boolean prsMode) {
        isPrsMode = prsMode;
    }

    public boolean isPrsMode() {
        return isPrsMode;
    }
}
