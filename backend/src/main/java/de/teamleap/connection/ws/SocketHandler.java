package de.teamleap.connection.ws;

import de.teamleap.app.MainController;
import de.teamleap.app.services.users.UserManagementService;
import de.teamleap.app.services.users.db.UserRow;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;
import org.springframework.web.socket.BinaryMessage;
import org.springframework.web.socket.CloseStatus;
import org.springframework.web.socket.TextMessage;
import org.springframework.web.socket.WebSocketSession;
import org.springframework.web.socket.handler.TextWebSocketHandler;

import java.io.IOException;
import java.util.*;
import java.util.concurrent.CopyOnWriteArrayList;

@Component("socketHandler")
public class SocketHandler extends TextWebSocketHandler {

    private static final Logger LOG = LoggerFactory.getLogger(MainController.class);

    private final static int HEARTBEAT_INTERVAL = 30 * 1000; // 30 seconds
    private final static String HEARTBEAT_PAYLOAD = new JSONObject().put("action", "heartbeat").toString();

    final List<WebSocketSession> sessions = new CopyOnWriteArrayList<>();
    final Map<String, WebSocketSession> userNameSessions = new LinkedHashMap<>();

    private static SocketHandler instance;
    private final UserManagementService userService;

    public SocketHandler(UserManagementService userService) {
        this.userService = userService;

        if (SocketHandler.instance == null) {
            SocketHandler.instance = this;
        } else {
            LOG.error("SocketHandler already initialized as [{}]", SocketHandler.instance);
        }

        Runtime.getRuntime().addShutdownHook(new Thread(() -> {
            LOG.info("Closing socket");
            this.close();
        }));

        // heartbeat to keep the connections alive
        final Timer heartbeat = new Timer();
        heartbeat.scheduleAtFixedRate(new TimerTask() {
            @Override
            public void run() {
                for (WebSocketSession session : sessions) {
                    try {
                        session.sendMessage(new TextMessage(HEARTBEAT_PAYLOAD));
                    } catch (IOException e) {
                        LOG.error("Error sending heartbeat", e);
                    }
                }
            }
        }, HEARTBEAT_INTERVAL, HEARTBEAT_INTERVAL);
    }

    private void close() {
        for (WebSocketSession session : this.sessions) {
            try {
                session.close();
            } catch (IOException e) {
                LOG.error("Error closing session", e);
            }
        }
    }

    public static SocketHandler initialize(UserManagementService userService) {
        if (instance == null) {
            instance = new SocketHandler(userService);
            LOG.info("SocketHandler initialized as [{}]", instance);
        }
        return instance;
    }

    public static SocketHandler getInstance() {
        if (instance == null) {
            throw new IllegalStateException("SocketHandler not initialized");
        }
        return instance;
    }

    public void broadcast(String msg) {
        for (int i = this.sessions.size() - 1; i >= 0; i--) {
            final WebSocketSession session = this.sessions.get(i);
            sendToSession(session, msg);
        }
    }

    private void broadcastToUser(String msg, String userName) {
        final WebSocketSession session = this.userNameSessions.get(userName);
        if (session != null) {
            LOG.info("Sending message to user name [{}] on session [{}]: {}", userName, session.getId(), msg);
            sendToSession(session, msg);
        } else {
            LOG.warn("No session found for user name [{}]", userName);
        }
    }

    private void sendToSession(WebSocketSession session, String msg) {
        synchronized (session) {
            try {
                session.sendMessage(new TextMessage(msg));
            } catch (IllegalStateException ignored) {
                LOG.error("Error sending message to {}, retrying", session);
                try {
                    session.sendMessage(new TextMessage(msg));
                } catch (Exception e) {
                    LOG.error("Error sending message to {}, removing session", session);
                    sessions.remove(session);
                }
            } catch (IOException ignored) {
                LOG.error("Error sending message to {}, removing session", session);
                sessions.remove(session);
            }
        }
    }

    public void broadcastAction(String action, Object... data) {
        final Map<String, Object> arguments = buildActionArguments(action, data);

        final JSONObject json = new JSONObject()
                .put("action", action);
        arguments.forEach(json::put);

        broadcast(json.toString());
    }

    public void broadcastActionToUser(String action, String userName, Object... data) {
        final Map<String, Object> arguments = buildActionArguments(action, data);

        final JSONObject json = new JSONObject()
                .put("action", action);
        arguments.forEach(json::put);

        broadcastToUser(json.toString(), userName);
    }

    public final static String SHOW_LEARNING_BLOCK = "showLearningBlock";
    public final static String STATE_CHANGED = "stateChanged";
    public final static String MONITOR_SHOW_MESSAGE = "monitorShowMessage";
    public final static String MONITOR_INVALID_CONFIGURATION = "monitorInvalidConfiguration";
    public final static String DEVICE_STATE_CHANGED = "deviceStateChanged";
    public final static String TODO_CHANGED = "todoChanged";
    public final static String TODO_REMOVED = "todoRemoved";
    public final static String TODO_ADDED = "todoAdded";
    public final static String KANBAN_ADDED = "kanbanAdded";
    public final static String KANBAN_REMOVED = "kanbanRemoved";
    public final static String KANBAN_CHANGED = "kanbanChanged";

    public final static String STATE_CHANGED_BLOCK_ADDED = "block_added";
    public final static String STATE_CHANGED_BLOCK_REMOVED = "block_removed";
    public static final String STATE_CHANGED_ALL_BLOCK_REMOVED = "all_blocks_removed";
    public static final String STATE_CHANGED_BUTTON_PRESSED = "button_pressed";
    public static final String STATE_CHANGED_APP_DEPLOYED = "app_deployed";

    public static final String MONITOR_SHOW_MESSAGE_LEARNING_REMOVE_BLOCK_FIRST = "learning_remove_block_first";
    public static final String MONITOR_SHOW_MESSAGE_UNKNOWN_BLOCK_TYPE = "unknown_block_type";
    public static final String MONITOR_SHOW_MESSAGE_APP_DEPLOYED_REMOVE_ALL_BLOCKS_FIRST = "app_deployed_remove_all_blocks_first";
    public static final String MONITOR_SHOW_MESSAGE_BLOCK_NOT_LEARNED = "block_not_learned";
    public static final String MONITOR_SHOW_MESSAGE_BLOCK_NOT_ACTIVE = "block_not_active";
    public static final String MONITOR_SHOW_MESSAGE_INVALID_BUTTON_PRESSED_COUNT = "invalid_button_pressed_count";
    public static final String MONITOR_SHOW_MESSAGE_PRESS_BUTTON_TO_DEPLOY = "press_button_to_deploy";
    public static final String MONITOR_SHOW_MESSAGE_ROTATE_KEY_TO_CONFIRM = "rotate_key_to_confirm";

    public final static String MONITOR_INVALID_CONFIGURATION_NO_IMPLEMENTATION_BLOCK = "no_implementation_block";
    public final static String MONITOR_INVALID_CONFIGURATION_TOO_MANY_IMPLEMENTATION_BLOCKS = "too_many_implementation_blocks";
    public final static String MONITOR_INVALID_CONFIGURATION_NO_CONTEXT_BLOCK = "no_context_block";
    public final static String MONITOR_INVALID_CONFIGURATION_NO_ADDON_BLOCK = "no_addon_block";
    public final static String MONITOR_INVALID_CONFIGURATION_TOO_MANY_CONTEXT_BLOCKS = "too_many_context_blocks";
    public final static String MONITOR_INVALID_CONFIGURATION_TOO_MANY_ADDON_BLOCKS = "too_many_addon_blocks";
    public final static String MONITOR_INVALID_CONFIGURATION_TOO_MANY_ADMIN_BLOCKS = "too_many_admin_blocks";

    public final static String DEVICE_STATE_CHANGED_CHANGE_ONLINE = "device_online";

    private static Map<String, Object> buildActionArguments(String action, Object[] data) {
        final Map<String, Object> arguments = new LinkedHashMap<>();

        switch (action) {
            case SHOW_LEARNING_BLOCK:
                arguments.put("type", data[0]);
                break;
            case STATE_CHANGED:
                arguments.put("change", data[0]);
                if (STATE_CHANGED_BLOCK_ADDED.equals(data[0]) || STATE_CHANGED_BLOCK_REMOVED.equals(data[0])) {
                    arguments.put("block", data[1]);
                    if (data.length >= 3) {
                        arguments.put("error", data[2]);
                    }
                } else if (STATE_CHANGED_BUTTON_PRESSED.equals(data[0])) {
                    arguments.put("count", data[1]);
                } else if (STATE_CHANGED_APP_DEPLOYED.equals(data[0])) {
                    arguments.put("appconfig", data[1]);
                } else if (STATE_CHANGED_ALL_BLOCK_REMOVED.equals(data[0]) && data.length >= 2) {
                    arguments.put("error", data[1]);
                }
                break;
            case MONITOR_SHOW_MESSAGE:
            case MONITOR_INVALID_CONFIGURATION:
                arguments.put("messageId", data[0]);
                break;
            case DEVICE_STATE_CHANGED:
                arguments.put("device", data[0]);
                arguments.put("change", data[1]);
                if (data.length >= 3) {
                    arguments.put("data", data[2]);
                }
                break;
            case TODO_CHANGED:
            case TODO_REMOVED:
            case TODO_ADDED:
                arguments.put("todo", data[0]);
                break;
            case KANBAN_ADDED:
            case KANBAN_REMOVED:
            case KANBAN_CHANGED:
                arguments.put("kanban", data[0]);
                break;
        }
        return arguments;
    }

    @Override
    public void handleTextMessage(WebSocketSession session, TextMessage message) {
        LOG.info("Received WS message: [{}]", message.getPayload());

        if (message.getPayload().startsWith("{")) {
            final JSONObject json = new JSONObject(message.getPayload());
            final String action = json.getString("action");
            switch (action) {
                case "register": {
                    final String userToken = json.getString("userToken");
                    final UserRow userByToken = this.userService.findUserByToken(userToken);
                    final String userName = userByToken.getName();
                    this.userNameSessions.put(userName, session);
                    LOG.info("Registered user [{}] with token [{}] as session [{}]", userName, userToken, session.getId());
                }
                break;
                case "unregister": {
                    final String userToken = json.getString("userToken");
                    final UserRow userByToken = this.userService.findUserByToken(userToken);
                    final String userName = userByToken.getName();
                    this.userNameSessions.remove(userName);
                    LOG.info("Unregistered user [{}] with token [{}]", userName, userToken);
                }
                break;
            }
        }
    }

    @Override
    public void afterConnectionEstablished(WebSocketSession session) {
        sessions.add(session);
    }

    @Override
    public void afterConnectionClosed(WebSocketSession session, CloseStatus status) {
        sessions.remove(session);
    }

    @Override
    protected void handleBinaryMessage(WebSocketSession session, BinaryMessage message) {
    }

    @Override
    public void handleTransportError(WebSocketSession session, Throwable exception) {
    }
}

