package de.teamleap.connection.db;

import de.teamleap.config.RDConfig;
import org.json.JSONArray;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.util.StringUtils;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

public class BtpServiceDatabaseConnectionProvider implements DatabaseConnectionProvider {

    private static final Logger LOG = LoggerFactory.getLogger(BtpServiceDatabaseConnectionProvider.class);

    private final String databaseUri;

    public BtpServiceDatabaseConnectionProvider() {
        final String databaseServiceId = RDConfig.getSapBtpDatabaseServiceId();

        final String vcapServices = System.getenv("VCAP_SERVICES");
        if (vcapServices == null) {
            throw new RuntimeException("VCAP_SERVICES not found in the environment");
        }

        final JSONObject services = new JSONObject(vcapServices);

        final JSONArray postgresDatabases = services.optJSONArray("postgresql-db");
        if (postgresDatabases == null) {
            throw new RuntimeException("No postgresql-db services found in VCAP_SERVICES");
        }

        final Optional<JSONObject> optionalCredentials = findConnectionCredentials(postgresDatabases, databaseServiceId);
        if (optionalCredentials.isEmpty()) {
            throw new RuntimeException("No postgresql-db service found with name [" + databaseServiceId + "]. Found services: " + findAvailableDatabaseServiceIds(postgresDatabases));
        }

        final JSONObject credentials = optionalCredentials.get();

        final String username = credentials.getString("username");
        final String password = credentials.getString("password");
        final String hostname = credentials.getString("hostname");
        final String dbname = credentials.getString("dbname");
        final String port = credentials.getString("port");

        this.databaseUri = "jdbc:postgresql://" + hostname + ":" + port + "/" + dbname + "?user=" + username + "&password=" + password;
    }

    private List<String> findAvailableDatabaseServiceIds(JSONArray postgresDatabases) {
        final List<String> availableDatabaseServiceIds = new ArrayList<>();

        for (int i = 0; i < postgresDatabases.length(); i++) {
            final JSONObject postgresDatabase = postgresDatabases.getJSONObject(i);
            final String databaseServiceId = postgresDatabase.optString("name", "");

            if (StringUtils.hasText(databaseServiceId)) {
                availableDatabaseServiceIds.add(databaseServiceId);
            }
        }

        return availableDatabaseServiceIds;
    }

    private Optional<JSONObject> findConnectionCredentials(JSONArray postgresDatabases, String id) {
        for (int i = 0; i < postgresDatabases.length(); i++) {
            final JSONObject postgresDatabase = postgresDatabases.getJSONObject(i);

            if (postgresDatabase.optString("name", "").equals(id)) {
                final JSONObject credentials = postgresDatabase.optJSONObject("credentials");
                if (credentials == null) {
                    throw new RuntimeException("No credentials found for postgresql-db service");
                }

                return Optional.of(credentials);
            }
        }

        return Optional.empty();
    }

    @Override
    public Connection connection() throws SQLException {
        return DriverManager.getConnection(databaseUri);
    }

    public static boolean instanceConstructionPrerequisitesPresent() {
        try {
            if (StringUtils.hasText(System.getenv("VCAP_SERVICES"))) {
                if (StringUtils.hasText(RDConfig.getSapBtpDatabaseServiceId())) {
                    return true;
                } else {
                    LOG.error("No SAP BTP database service id configured");
                }
            } else {
                LOG.info("No SAP BTP database service found in VCAP_SERVICES");
            }
            return false;
        } catch (Exception e) {
            return false;
        }
    }
}
