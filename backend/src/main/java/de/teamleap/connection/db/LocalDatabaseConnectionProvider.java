package de.teamleap.connection.db;

import de.teamleap.config.RDConfig;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.util.StringUtils;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class LocalDatabaseConnectionProvider implements DatabaseConnectionProvider {

    private static final Logger LOG = LoggerFactory.getLogger(BtpServiceDatabaseConnectionProvider.class);

    private final String databaseUri;

    public LocalDatabaseConnectionProvider() {
        this.databaseUri = RDConfig.getLocalDevelopmentDatabaseUri();
        LOG.info("Using local database URI [{}]", this.databaseUri.replaceAll("password=[^&]+", "password=****"));
    }

    @Override
    public Connection connection() throws SQLException {
        return DriverManager.getConnection(this.databaseUri);
    }

    @Override
    public String toString() {
        return "LocalDatabaseConnectionProvider{" +
               "databaseUri='" + databaseUri.replaceAll("password=[^&]+", "password=****") + '\'' +
               '}';
    }

    public static boolean instanceConstructionPrerequisitesPresent() {
        try {
            return StringUtils.hasText(RDConfig.getLocalDevelopmentDatabaseUri());
        } catch (Exception e) {
            return false;
        }
    }
}
