package de.teamleap.connection.db;

import javax.sql.DataSource;
import java.sql.Connection;
import java.sql.SQLException;

public class DataSourceDatabaseConnectionProvider implements DatabaseConnectionProvider {

    private final DataSource database;

    public DataSourceDatabaseConnectionProvider(DataSource database) {
        if (database == null) {
            throw new IllegalArgumentException("Database cannot be null");
        }
        this.database = database;
    }

    @Override
    public Connection connection() throws SQLException {
        return database.getConnection();
    }
}
