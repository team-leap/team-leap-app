package de.teamleap.connection.db;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * May be in one of the following states:
 * <ul>
 *     <li>
 *         LocalDatabaseConnectionProvider: is the case, if the getLocalDevelopmentDatabaseUri() method contains a
 *         valid URI
 *     </li>
 *     <li>
 *         BtpServiceDatabaseConnectionProvider: is the case, if the getSapBtpDatabaseServiceId() method contains a
 *         valid URI
 *     </li>
 *     <li>DataSourceDatabaseConnectionProvider: is the case, if none of the above match.</li>
 * </ul>
 * The last one (DataSourceDatabaseConnectionProvider) has to be set by the test class. Therefore, the property cannot
 * be final.
 */
public abstract class RDDatabaseHandler {

    private static final Logger LOG = LoggerFactory.getLogger(RDDatabaseHandler.class);

    private static DatabaseConnectionProvider connectionProvider = findDatabaseConnectionProvider();

    private static DatabaseConnectionProvider findDatabaseConnectionProvider() {
        try {
            if (LocalDatabaseConnectionProvider.instanceConstructionPrerequisitesPresent()) {
                LOG.info("Using local database connection provider");
                return new LocalDatabaseConnectionProvider();
            } else if (BtpServiceDatabaseConnectionProvider.instanceConstructionPrerequisitesPresent()) {
                LOG.info("Using BTP service database connection provider");
                return new BtpServiceDatabaseConnectionProvider();
            } else {
                LOG.warn("Found no database connection provider");
                LOG.info("Either waiting for [test class to set connection provider] or specify a [local database URI] or a [BTP service ID]");
                return null;
            }
        } catch (Exception e) {
            LOG.error("Error finding database connection provider", e);
            return null;
        }
    }

    public static void setConnectionProvider(DatabaseConnectionProvider connectionProvider) {
        if (connectionProvider == null) {
            throw new IllegalArgumentException("Connection provider must not be null");
        }

        LOG.info("Setting custom connection provider to {}", connectionProvider.getClass().getSimpleName());
        RDDatabaseHandler.connectionProvider = connectionProvider;
    }

    public static DatabaseConnectionProvider getConnectionProvider() {
        return connectionProvider;
    }
}
