import { HttpClient } from '@angular/common/http';
import {
  EnvironmentProviders,
  importProvidersFrom,
  inject,
  Injectable,
  makeEnvironmentProviders,
} from '@angular/core';
import {
  Translation,
  TranslocoConfig,
  translocoConfig,
  TranslocoLoader,
  TranslocoModule,
  TRANSLOCO_CONFIG,
  TRANSLOCO_LOADER,
} from '@ngneat/transloco';
import {environment} from "../environments/environment";

@Injectable({ providedIn: 'root' })
export class TranslocoHttpLoader implements TranslocoLoader {
  http = inject(HttpClient);

  getTranslation(lang: string) {
    return this.http.get<Translation>(`/assets/i18n/${lang}.json`);
  }
}

export const provideTransloco = (
  config: Partial<TranslocoConfig>
): EnvironmentProviders => {
  let currentLang = window.localStorage.getItem('lang');
  if(currentLang == null){
    window.localStorage.setItem('lang',config.defaultLang ? config.defaultLang : 'de')
  }
  return makeEnvironmentProviders([
    importProvidersFrom(TranslocoModule),
    {
      provide: TRANSLOCO_CONFIG,
      useValue: translocoConfig({
        availableLangs: config.availableLangs,
        defaultLang: currentLang != null ? currentLang : config.defaultLang,
        reRenderOnLangChange: true,
        prodMode: !environment.prod,
      }),
    },
    { provide: TRANSLOCO_LOADER, useClass: TranslocoHttpLoader },
  ]);
};
