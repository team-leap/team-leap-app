﻿import {EnvironmentProviders, importProvidersFrom, makeEnvironmentProviders} from "@angular/core";
import {
  LoggerModule,
  NGXLoggerConfigEngine,
  NGXLoggerConfigEngineFactory,
  NgxLoggerLevel, NGXLoggerMapperService,
  NGXLoggerMetadataService,
  NGXLoggerRulesService, NGXLoggerServerService, NGXLoggerWriterService,
  TOKEN_LOGGER_CONFIG,
  TOKEN_LOGGER_CONFIG_ENGINE_FACTORY,
  TOKEN_LOGGER_MAPPER_SERVICE,
  TOKEN_LOGGER_METADATA_SERVICE,
  TOKEN_LOGGER_RULES_SERVICE, TOKEN_LOGGER_SERVER_SERVICE, TOKEN_LOGGER_WRITER_SERVICE
} from "ngx-logger";
import {environment} from "../environments/environment";

export const provideNgxLogger = (): EnvironmentProviders => {
  return makeEnvironmentProviders([
    importProvidersFrom(LoggerModule),
    {
      provide: TOKEN_LOGGER_CONFIG,
      useValue: new NGXLoggerConfigEngine({
        level: environment.prod ? NgxLoggerLevel.OFF : NgxLoggerLevel.DEBUG
      }),
    },
    { provide: TOKEN_LOGGER_CONFIG_ENGINE_FACTORY, useClass: NGXLoggerConfigEngineFactory },
    { provide: TOKEN_LOGGER_METADATA_SERVICE, useClass: NGXLoggerMetadataService},
    { provide: TOKEN_LOGGER_RULES_SERVICE, useClass: NGXLoggerRulesService},
    { provide: TOKEN_LOGGER_MAPPER_SERVICE, useClass: NGXLoggerMapperService},
    { provide: TOKEN_LOGGER_WRITER_SERVICE, useClass: NGXLoggerWriterService},
    { provide: TOKEN_LOGGER_SERVER_SERVICE, useClass: NGXLoggerServerService}
  ]);
};
