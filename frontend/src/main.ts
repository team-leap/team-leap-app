import {bootstrapApplication, BrowserModule} from "@angular/platform-browser";
import {RootComponent} from "./app/components/root/root/root.component";
import {provideRouter} from "@angular/router";
import {ROOT_ROUTES} from "./app/components/root/root-routes";
import {importProvidersFrom} from "@angular/core";
import {provideHttpClient, withInterceptors} from "@angular/common/http";
import {provideTransloco} from "./provider/transloco";
import {provideNgxLogger} from "./provider/ngx-logger";
import {provideToastr} from "ngx-toastr";
import {provideAnimations} from "@angular/platform-browser/animations";
import {AuthInterceptor} from "./app/services/root/interceptor/auth-interceptor";


bootstrapApplication(RootComponent, {
  providers: [
    provideRouter(ROOT_ROUTES),
    importProvidersFrom(BrowserModule),
    provideHttpClient(withInterceptors([AuthInterceptor])),
    provideTransloco({ availableLangs: ['de', 'en'], defaultLang: 'de' }), // required for transloco (multilanguage support)
    provideNgxLogger(), // required for logger lib
    provideAnimations(), // required for toast lib
    provideToastr(), // required for toast lib
  ]
}).then((ref) => {
  // Ensure Angular destroys itself on hot reloads.
    // @ts-ignore
    if (window['ngRef']) {
        // @ts-ignore
      window['ngRef'].destroy();
    }
      // @ts-ignore
    window['ngRef'] = ref;
  })
  .catch((err) => console.error(err));
