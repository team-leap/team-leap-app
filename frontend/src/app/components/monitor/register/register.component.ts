import { Component } from '@angular/core';
import {AuthService} from "../../../services/monitor/auth/auth.service";
import {Router, RouterOutlet} from "@angular/router";
import {TranslocoModule} from "@ngneat/transloco";
import {AsyncPipe, NgClass, NgIf} from "@angular/common";
import { map, Observable} from "rxjs";
import { AppStateDTO} from "../../../model/model";
import {HeaderComponent} from "../../root/header/header.component";
import {TranslationService} from "../../../services/root/translation/translation.service";
import {MonitorStateService} from "../../../services/monitor/monitor-state/monitor-state.service";

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css'],
    imports: [
        TranslocoModule,
        NgIf,
        NgClass,
        AsyncPipe,
        HeaderComponent,
        RouterOutlet
    ],
  standalone: true
})
export class RegisterComponent {

  private readonly _keyCardDetected$:Observable<boolean|undefined>;
  constructor(
    private readonly auth:AuthService,
    private readonly router:Router,
    private readonly monitorStateService: MonitorStateService,
    private readonly translationService:TranslationService
  ) {
    this.monitorStateService.connectToWebSocketChannel();
    this._keyCardDetected$ = monitorStateService.currentAppState$.pipe(
      map((res:AppStateDTO|undefined) => {
        return res?.state.adminCard
      })
    )
  }

  public retrieveAdminToken(){
    this.auth.retrieveAdminToken();
  }

  public startLearning(){
    this.router.navigate(['learning']);
  }

  public startBuilding(){
    this.router.navigate(['building'])
  }

  public switchLang(){
    this.translationService.switchLang()
  }

  get keyCardDetected$(): Observable<boolean | undefined> {
    return this._keyCardDetected$;
  }
}
