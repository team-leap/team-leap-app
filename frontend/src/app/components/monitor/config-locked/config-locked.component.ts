import {Component, CUSTOM_ELEMENTS_SCHEMA} from '@angular/core';
import {AsyncPipe, NgClass, NgForOf, NgIf} from "@angular/common";
import {HeaderComponent} from "../../root/header/header.component";
import {RocketComponent} from "../rocket/rocket.component";
import {map, Observable, Subscription, switchMap, take, tap, timer} from "rxjs";
import {capitalizeString, sleep} from "../helper-functions";
import {TranslocoModule} from "@ngneat/transloco";
import {ChatbotComponent} from "../chatbot/chatbot.component";
import {TranslationService} from "../../../services/root/translation/translation.service";
import "@ui5/webcomponents/dist/Icon.js";
import "@ui5/webcomponents-icons/dist/AllIcons.js";
import {BuildingBlock} from "../../../model/model";
import {QrComponent} from "../qr/qr.component";
import {MonitorStateService} from "../../../services/monitor/monitor-state/monitor-state.service";
import {LoggerService} from "../../../services/root/logger/logger.service";


@Component({
  selector: 'app-config-locked',
  templateUrl: './config-locked.component.html',
  styleUrls: ['./config-locked.component.css'],
  imports: [
    NgIf,
    HeaderComponent,
    RocketComponent,
    AsyncPipe,
    NgForOf,
    TranslocoModule,
    NgClass,
    ChatbotComponent,
    QrComponent
  ],
  standalone: true,
  schemas: [CUSTOM_ELEMENTS_SCHEMA]

})
export class ConfigLockedComponent {

  public installing = [false, false, false];
  public installed = [false, false, false];
  private _subs = new Subscription();

  constructor(
    private readonly monitorStateService: MonitorStateService,
    private readonly translationService: TranslationService,
    private readonly logger: LoggerService
  ) {
    this.monitorStateService.connectToWebSocketChannel();
    this.initTimer();
    this.monitorStateService.appDeploying$.subscribe(res => {
      if (res) {
        let isDeployed = false;
        setTimeout(() => {
          const launchAnimationVideo: HTMLVideoElement = document.getElementById('launch-animation') as HTMLVideoElement;
          if (launchAnimationVideo) {
            launchAnimationVideo.addEventListener('loadeddata', (e) => {
              if (launchAnimationVideo.readyState >= 3) {
                this.monitorStateService.setAppDeployed();
                isDeployed = true;
              }
            });
          } else {
            this.logger.standardLogger("ConfigLockedComponent.constructor", "launchAnimationVideo not found, showing app as deployed now");
            this.monitorStateService.setAppDeployed();
            isDeployed = true;
          }
        }, 0)

        setTimeout(() => {
          if (!isDeployed) {
            this.logger.standardLogger("ConfigLockedComponent.constructor", "something went wrong during launch animation, showing app as deployed now");
            this.monitorStateService.setAppDeployed();
          }
        }, 4000)
      }
    })
  }

  private initTimer() {
    this._subs.add(timer(600, 3500)
      .pipe(
        map(index => 2 - index),
        switchMap(index => {
          this.installing[index] = true;
          return this.monitorStateService.activeBlocks$.pipe(
            map(blocks => {
              return {index: index.toString(), block: blocks[index]}
            })
          )
        }),
        tap(res => {
          this.showTextLineByLine([res.block.info], res.index, res.block.category)
        }),
        take(3)
      )
      .subscribe()
    )
  }

  private showTextLineByLine(translationKey: string[], elementId: string, category: string) {
    setTimeout(async () => {
      const translation = translationKey
        .map(key => this.translationService.translateSync(key), {category: capitalizeString(category)})
        .join('\n');
      this.revealTextStepByStep(translation, elementId);
    }, 0)
  }

  private async revealTextStepByStep(text: string, elementId: string) {
    const textBox = document.getElementById(elementId);
    let wholeText = [];
    text = text.replaceAll('<b>', '').replaceAll('</b>', '');
    for (let letter of [...text]) {
      if (letter === '\n') {
        letter = '<br>';
      }
      wholeText.push(letter)
      if (textBox) {
        textBox.innerHTML = wholeText.join('')
      }
      await sleep(18);
    }
    this.installing[Number(elementId)] = false;
    this.installed[Number(elementId)] = true;
  }

  get activeBlocks$(): Observable<BuildingBlock[]> {
    return this.monitorStateService.activeBlocks$;
  }

  get appDeploying$(): Observable<boolean> {
    return this.monitorStateService.appDeploying$;
  }

  get appDeployed$() {
    return this.monitorStateService.appDeployed$;
  }

}
