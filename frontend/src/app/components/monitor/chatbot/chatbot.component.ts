import {Component, Input, OnDestroy, OnInit} from '@angular/core';
import {TranslocoModule,} from "@ngneat/transloco";
import {sleep} from "../helper-functions";
import {distinctUntilChanged, map, Subscription, switchMap, tap} from "rxjs";
import {AsyncPipe, NgClass, NgIf} from "@angular/common";
import {TranslationService} from "../../../services/root/translation/translation.service";
import {MonitorStateService} from "../../../services/monitor/monitor-state/monitor-state.service";
import {BuildingBlock} from "../../../model/model";

@Component({
  selector: 'app-chatbot',
  templateUrl: './chatbot.component.html',
  styleUrls: ['./chatbot.component.css'],
  standalone: true,
  imports: [NgClass, AsyncPipe, TranslocoModule, NgIf]
})
export class ChatbotComponent implements OnDestroy, OnInit {
  private _subs = new Subscription();
  @Input() chatbotTranslationKey: string | undefined;

  private uniqueDialogId = 0;
  private static instancesCount = 0;
  private readonly instanceId: number;

  constructor(
    private readonly translationService: TranslationService,
    private readonly monitorStateService: MonitorStateService
  ) {
    ChatbotComponent.instancesCount++;
    this.instanceId = ChatbotComponent.instancesCount;
  }

  ngOnInit() {
    this._subs.add(this.monitorStateService.appConfigIsValid$
      .pipe(
        distinctUntilChanged(),
        switchMap(isValid => {
          return this.monitorStateService.activeBlocks$.pipe(
            map(res => {
              return {isValid: isValid, blocks: res}
            }),
          )
        }),
        switchMap(previous => {
          return this.monitorStateService.appDeployed$.pipe(
            map(res => {
              return {
                ...previous,
                deployed: res
              }
            }),
          )
        }),
        tap(res => {
          if (res.blocks.length <= 2 && res.deployed) {
            return;
          }
          const baseTranslationKey = this.translationKeyFromActiveBlocks(res.blocks, res.deployed);
          let params: { [key: string]: string } = {};
          if (baseTranslationKey === 'building.chatboxTexts.locked' || res.isValid) {
            const appTypeKey = this.translationKeyForAppType(res.blocks);
            params['appType'] = this.translationService.translateSync(appTypeKey);
          }
          this.showText(res.isValid, baseTranslationKey, params);
        })
      )
      .subscribe()
    )
  }

  private translationKeyFromActiveBlocks(blocks: BuildingBlock[], deployed: boolean): string | undefined {
    if (blocks.length === 0) {
      return 'building.chatboxTexts.start';
    } else if (deployed) {
      return undefined;
    }

    const hasImplementation = blocks.some(block => block.category === 'implementation');
    const hasContext = blocks.some(block => block.category === 'context');
    const hasCustomization = blocks.some(block => block.category === 'customization');

    const hasTooManyImplementation = blocks.filter(block => block.category === 'implementation').length > 1;
    const hasTooManyContext = blocks.filter(block => block.category === 'context').length > 1;
    const hasTooManyCustomization = blocks.filter(block => block.category === 'customization').length > 1;

    if (hasTooManyImplementation) {
      return 'building.chatboxTexts.tooManyImplementation';
    } else if (hasTooManyContext) {
      return 'building.chatboxTexts.tooManyContext';
    } else if (hasTooManyCustomization) {
      return 'building.chatboxTexts.tooManyCustomization';
    }

    if (!hasImplementation && !hasContext && !hasCustomization) {
      return 'building.chatboxTexts.start';
    } else if (!hasImplementation) {
      return 'building.chatboxTexts.implementationNext';
    } else if (!hasContext) {
      return 'building.chatboxTexts.contextNext';
    } else if (!hasCustomization) {
      return 'building.chatboxTexts.customizationNext';
    }

    return undefined;
  }

  private translationKeyForAppType(blocks: BuildingBlock[]): string {
    const implementation = blocks.find(block => block.category === 'implementation')?.id;
    const context = blocks.find(block => block.category === 'context')?.id;
    if (implementation && context) {
      return 'invalidRoute.appConfigButton.' + implementation + '-' + context;
    }
    return 'invalidRoute.appConfigButton.default';
  }

  private showText(isValid: boolean, overwriteDefault: string | undefined = undefined, additionalParameters: {
    [key: string]: string
  } = {}) {
    let translationKey = this.chatbotTranslationKey ? this.chatbotTranslationKey : '';
    if (isValid && location.pathname.includes('building')) {
      translationKey = 'building.chatboxTexts.turnKey'
    } else if (overwriteDefault) {
      translationKey = overwriteDefault;
    }

    setTimeout(async () => {
      const translation = await this.translationService.translate(translationKey, additionalParameters);
      await this.revealTextStepByStep(translation);
    }, 0);
  }

  private async revealTextStepByStep(text: string) {
    this.uniqueDialogId = this.randomBetween(0, 1000000);
    const dialogId: number = this.uniqueDialogId;

    const textBox = document.getElementById('chat-text');
    const mascotImg = document.getElementById('chat-mascot');
    let wholeText = [];
    const splitText = this.splitTextIntoSyllables(text);
    let isBold = false;

    if (mascotImg) {
      mascotImg.classList.remove('idle');
      mascotImg.classList.add('talking');
    }

    for (const syllable of splitText) {
      if (syllable === '**') {
        if (isBold) {
          wholeText.push("</b>");
        } else {
          wholeText.push("<b>");
        }
        isBold = !isBold;
      } else {
        wholeText.push(syllable);
      }
      if (textBox) {
        textBox.innerHTML = wholeText.join('');
      }
      await sleep(35);
      if (this.uniqueDialogId !== dialogId || this.instanceId !== ChatbotComponent.instancesCount) {
        return;
      }
    }

    if (mascotImg) {
      mascotImg.classList.add('idle');
      mascotImg.classList.remove('talking');
    }
  }

  private splitTextIntoSyllables(text: string): string[] {
    let splitText = [];
    let i = 0;
    while (i < text.length) {
      let chunkSize = 1;
      if (text.slice(i, i + chunkSize).includes('*') && text.slice(i + chunkSize, i + chunkSize + 1) === '*') {
        chunkSize++;
      }
      let segment = text.slice(i, i + chunkSize);
      if (segment === '<') {
        segment = '&lt;';
      }
      splitText.push(segment);
      i += chunkSize;
    }
    return splitText;
  }

  private randomBetween(min: number, max: number) {
    return Math.floor(Math.random() * (max - min + 1) + min);
  }

  ngOnDestroy() {
    this._subs.unsubscribe();
  }

}
