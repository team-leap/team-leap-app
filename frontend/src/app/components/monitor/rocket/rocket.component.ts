import {Component} from '@angular/core';
import {Observable} from "rxjs";
import {AsyncPipe, NgClass, NgIf} from "@angular/common";
import {BuildingBlock} from "../../../model/model";
import {MonitorStateService} from "../../../services/monitor/monitor-state/monitor-state.service";

@Component({
  selector: 'app-rocket',
  templateUrl: './rocket.component.html',
  styleUrls: ['./rocket.component.css'],
  standalone: true,
  imports: [AsyncPipe, NgIf, NgClass]
})
export class RocketComponent {


  constructor(private readonly monitorStateService: MonitorStateService) {
  }

  get activeImplementationBlocks$(): Observable<BuildingBlock> {
    return this.monitorStateService.activeImplementationBlocks$;
  }

  get activeContextBlocks$(): Observable<BuildingBlock> {
    return this.monitorStateService.activeContextBlocks$;
  }

  get activeCustomizationBlocks$(): Observable<BuildingBlock> {
    return this.monitorStateService.activeCustomizationBlocks$;
  }


}
