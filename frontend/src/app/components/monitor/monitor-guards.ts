import {inject} from "@angular/core";
import {AuthService} from "../../services/monitor/auth/auth.service";
import {Router} from "@angular/router";


export const MonitorAuthGuard = async (): Promise<boolean> => {
  const auth = inject(AuthService);
  const router = inject(Router);
  const isAuthenticated = await auth.isAuthenticated();
  if(!isAuthenticated){
    router.navigate(['register'])
  }
  return isAuthenticated
}
