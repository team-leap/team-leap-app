import {Component, OnDestroy} from '@angular/core';
import {Observable, Subscription} from "rxjs";
import {AsyncPipe, NgClass, NgForOf, NgIf, NgStyle} from "@angular/common";
import {TranslocoModule} from "@ngneat/transloco";
import {HeaderComponent} from "../../root/header/header.component";
import {RouterOutlet} from "@angular/router";
import {BuildingBlock, Category} from "../../../model/model";
import {RocketComponent} from "../rocket/rocket.component";
import {capitalizeString} from "../helper-functions";
import {ChatbotComponent} from "../chatbot/chatbot.component";
import {MonitorStateService} from "../../../services/monitor/monitor-state/monitor-state.service";

@Component({
  selector: 'app-building',
  templateUrl: './building.component.html',
  styleUrls: ['./building.component.css'],
  imports: [
    AsyncPipe,
    TranslocoModule,
    NgForOf,
    NgStyle,
    NgClass,
    NgIf,
    HeaderComponent,
    RouterOutlet,
    RocketComponent,
    ChatbotComponent
  ],
  standalone: true
})
export class BuildingComponent implements OnDestroy{

  private _subs = new Subscription();
  constructor(
    private readonly monitorstateService: MonitorStateService,
  ) {
    this.monitorstateService.connectToWebSocketChannel()
  }

  getActiveBlockFromCategoryAndActiveBlocks(activeBlocks: BuildingBlock[]|null,category: Category):string{
    if(activeBlocks == null)
      return ''
    const activeBlock = activeBlocks.find(block => {
      if(block && block.category){
        return block.category === category
      }
      return undefined;
    })
    return activeBlock ? activeBlock.info : '';
  }


  get activeBlocks$(){
    return this.monitorstateService.activeBlocks$;
  }
  get appState$(){
    return this.monitorstateService.currentAppState$;
  }
  get blocksByCategory$(){
    return this.monitorstateService.blocksByCategory$;
  }

  get appConfigIsValid$():Observable<boolean>{
    return this.monitorstateService.appConfigIsValid$;
  }

  protected readonly capitalizeString = capitalizeString;

  ngOnDestroy() {
    this._subs.unsubscribe();
  }
}
