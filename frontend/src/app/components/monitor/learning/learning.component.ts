import {Component, OnInit} from '@angular/core';
import {AppStateDTO} from "../../../model/model";
import {AsyncPipe, NgClass, NgForOf, NgIf, NgStyle} from "@angular/common";
import {TranslocoModule} from "@ngneat/transloco";
import {map, Observable} from "rxjs";
import {HeaderComponent} from "../../root/header/header.component";
import {RouterOutlet} from "@angular/router";
import {MonitorStateService} from "../../../services/monitor/monitor-state/monitor-state.service";
import {buildingBlocks} from "../../../model/buildingBlocks";



@Component({
  selector: 'app-learning',
  templateUrl: './learning.component.html',
  styleUrls: ['./learning.component.css'],
  imports: [
    NgIf,
    TranslocoModule,
    AsyncPipe,
    NgForOf,
    NgStyle,
    NgClass,
    HeaderComponent,
    RouterOutlet
  ],
  standalone: true
})
export class LearningComponent implements OnInit {

  private readonly _blockToLearn$: Observable<number>;

  constructor(
    private readonly monitorStateService: MonitorStateService,
  ) {
    this._blockToLearn$ = this.monitorStateService.currentAppState$.pipe(
      map((res: AppStateDTO | undefined) => {
        return res?.state?.learningBlock!
      })
    )

  }

  ngOnInit() {
    this.monitorStateService.connectToWebSocketChannel();
  }

  public startLearning() {
    this.monitorStateService.startLearning();
  }

  get appState$(): Observable<AppStateDTO | undefined> {
    return this.monitorStateService.currentAppState$;
  }

  get blockToLearn$() {
    return this._blockToLearn$;
  }

  protected readonly buildingBlocks = buildingBlocks;
}
