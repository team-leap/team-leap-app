import {Component} from '@angular/core';
import {QRCodeModule} from "angularx-qrcode";
import {environment} from "../../../../environments/environment";
import {NgIf} from "@angular/common";
import {LoggerService} from "../../../services/root/logger/logger.service";
import {Router} from "@angular/router";
import {ChatbotComponent} from "../chatbot/chatbot.component";
import {MonitorStateService} from "../../../services/monitor/monitor-state/monitor-state.service";

@Component({
  selector: 'app-qr',
  templateUrl: './qr.component.html',
  styleUrls: ['./qr.component.css'],
  imports: [QRCodeModule, NgIf, ChatbotComponent],
  standalone: true,
})
export class QrComponent {

  public qrCode: string | null

  constructor(
    private readonly logger: LoggerService,
    private readonly monitorStateService: MonitorStateService,
    private readonly router: Router
  ) {
    this.monitorStateService.connectToWebSocketChannel();
    this.monitorStateService.setAppDeployingFlagToFalse();
    const storedAppConfig = window.localStorage.getItem('appConfig');
    if (storedAppConfig) {
      const appConfig = JSON.parse(storedAppConfig)
      this.qrCode = `${environment.frontendUrl}/app/${appConfig.id}/${appConfig.implementation}/${appConfig.context}/${appConfig.addon}`;
      logger.standardLogger('QrComponent.contructor', `qrCode: ${this.qrCode}`)
      return;
    } else {
      this.router.navigate(['building'])
      this.logger.standardLogger('QrComponent.contructor', 'couldnt retrieve appConfig from local storage - check backend')
    }
    this.qrCode = null
  }
}
