export const capitalizeString = (str: string):string => {
  return str.charAt(0).toUpperCase() + str.slice(1);
}

export const sleep = (ms: number): Promise<void> => {
  return new Promise(resolve => {
    setTimeout(resolve, ms);
  });
}
