import { Component } from '@angular/core';
import {LoadIndicatorService} from "../../../services/root/load-indicator/load-indicator.service";
import {NgIf} from "@angular/common";

@Component({
  selector: 'app-loading-spinner',
  templateUrl: './loading-spinner.component.html',
  styleUrls: ['./loading-spinner.component.css'],
  imports: [
    NgIf
  ],
  standalone: true
})
export class LoadingSpinnerComponent {
  constructor(public readonly loadIndicatorService: LoadIndicatorService) {
  }
}
