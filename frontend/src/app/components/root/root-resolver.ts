import {inject} from "@angular/core";
import {ToastService} from "../../services/root/toast/toast.service";
import {MonitorStateService} from "../../services/monitor/monitor-state/monitor-state.service";

export const AppStateResolver = async () => {
  const monitorStateService = inject(MonitorStateService);
  const toaster = inject(ToastService)
  const appState = await monitorStateService.retrieveAppState();
  monitorStateService.setAdminRoute();
  if(appState.state === 'LEARNING' && !location.pathname.includes('learning')){
    toaster.showWarningToast('learning.leavingLearningRoute')
  }
  return appState.successiveDeployButtonPresses;
}
