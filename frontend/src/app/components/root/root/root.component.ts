import { Component } from '@angular/core';
import {RouterOutlet} from "@angular/router";
import {LoadingSpinnerComponent} from "../loading-spinner/loading-spinner.component";
import {AlarmComponent} from "../alarm/alarm.component";

@Component({
  selector: 'app-root',
  templateUrl: './root.component.html',
  styleUrls: ['./root.component.css'],
  imports: [
    RouterOutlet,
    LoadingSpinnerComponent,
    AlarmComponent,
  ],
  standalone: true
})
export class RootComponent {
  title = 'rocket-deployer-frontend';
}
