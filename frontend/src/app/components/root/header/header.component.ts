import { Component } from '@angular/core';
import {TranslocoModule} from "@ngneat/transloco";

@Component({
  selector: 'app-root-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css'],
  imports: [
    TranslocoModule,
    HeaderComponent
  ],
  standalone: true
})
export class HeaderComponent {

}
