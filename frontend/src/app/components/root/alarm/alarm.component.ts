import {Component, OnInit} from '@angular/core';
import {AsyncPipe, NgClass, NgIf} from "@angular/common";
import {MonitorStateService} from "../../../services/monitor/monitor-state/monitor-state.service";

@Component({
  selector: 'app-alarm',
  templateUrl: './alarm.component.html',
  styleUrls: ['./alarm.component.css'],
  standalone: true,
  imports: [
    AsyncPipe,
    NgIf,
    NgClass
  ]
})
export class AlarmComponent implements OnInit{

  constructor(
    private readonly monitorStateService: MonitorStateService,
  ) {}

    ngOnInit() {
    }

  get isAdminRoute$(){
    return this.monitorStateService.isAdminRoute$
  }
  get appState$(){
    return this.monitorStateService.currentAppState$;
  }
}
