import {Component, Input, OnInit} from '@angular/core';
import {LoadIndicatorService} from "../../../services/root/load-indicator/load-indicator.service";
import {NgIf, NgStyle} from "@angular/common";

@Component({
  selector: 'emob-apps-swap-mini-loading-spinner',
  templateUrl: './mini-loading-spinner.component.html',
  styleUrls: ['./mini-loading-spinner.component.css'],
  imports: [
    NgIf,
    NgStyle
  ],
  standalone: true
})
export class MiniLoadingSpinnerComponent implements OnInit {

  constructor(public readonly loadIndicatorService: LoadIndicatorService) {}

  ngOnInit(): void {}
}
