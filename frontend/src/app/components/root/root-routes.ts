import {Route} from '@angular/router';
import {MonitorAuthGuard} from "../monitor/monitor-guards";
import {AppStateResolver} from "./root-resolver";

export const ROOT_ROUTES: Route[] = [
  {
    path:'',
    redirectTo: 'register',
    pathMatch: "full"
  },
  {
    path:'register',
    loadComponent: () => import('../monitor/register/register.component').then(mod => mod.RegisterComponent),
    resolve:[AppStateResolver]
  },
  {
    path:'learning',
    loadComponent: () => import('../monitor/learning/learning.component').then(mod => mod.LearningComponent),
    canActivate: [MonitorAuthGuard],
    resolve: [AppStateResolver],
    pathMatch: "full"
  },
  {
    path:'building',
    loadComponent: () => import('../monitor/building/building.component').then(mod => mod.BuildingComponent),
    canActivate: [MonitorAuthGuard],
    resolve: [AppStateResolver],
    pathMatch: "full"
  },
  {
    path: 'config-locked',
    loadComponent: () => import('../monitor/config-locked/config-locked.component').then(mod => mod.ConfigLockedComponent),
    canActivate: [MonitorAuthGuard],
    resolve:[AppStateResolver]
  },
  {
    path:'app/:id/:implementation/:context/:addon',
    loadChildren: () => import('../apps/apps-routes').then(mod => mod.APPS_ROUTES),
  },
  {
    path:'invalid-route',
    loadComponent: () => import('../root/invalid-route/invalid-route.component').then(mod => mod.InvalidRouteComponent),
  },
  {
    path:'**',
    redirectTo: 'invalid-route'
  },
];

