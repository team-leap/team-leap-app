import {Component, CUSTOM_ELEMENTS_SCHEMA} from '@angular/core';
import {AppConfig} from "../../../model/model";
import {NgForOf, NgIf, NgOptimizedImage} from "@angular/common";
import {Router} from "@angular/router";
import {TranslocoModule} from "@ngneat/transloco";

import "@ui5/webcomponents-icons/dist/AllIcons.js";
import "@ui5/webcomponents/dist/Card.js";
import "@ui5/webcomponents-fiori/dist/SideNavigationItem.js";
import "@ui5/webcomponents-fiori/dist/SideNavigation.js";
import "@ui5/webcomponents-fiori/dist/ShellBar.js";
import "@ui5/webcomponents-fiori/dist/ShellBarItem.js";
import "@ui5/webcomponents-fiori/dist/Bar.js";
import "@ui5/webcomponents/dist/Icon.js";
import "@ui5/webcomponents/dist/Popover.js";
import "@ui5/webcomponents/dist/Input.js";
import "@ui5/webcomponents/dist/TextArea.js";
import "@ui5/webcomponents/dist/Select.js";
import "@ui5/webcomponents/dist/ComboBox.js";
import "@ui5/webcomponents/dist/Tab.js";
import "@ui5/webcomponents/dist/Title.js";

@Component({
  schemas: [CUSTOM_ELEMENTS_SCHEMA],
  selector: 'app-invalid-route',
  templateUrl: './invalid-route.component.html',
  styleUrls: ['./invalid-route.component.css'],
  imports: [
    NgForOf,
    NgIf,
    NgOptimizedImage,
    TranslocoModule
  ],
  standalone: true
})
export class InvalidRouteComponent {

  private _potentialAppConfigs: AppConfig[] = [];

  constructor(private readonly router: Router) {
    const storageKeys = Object.keys(window.localStorage);
    storageKeys.forEach(k => {
      if (k.startsWith('appConfig')) {
        this._potentialAppConfigs.push(JSON.parse(window.localStorage.getItem(k)!))
      }
    })
  }

  public routeToAppConfig(id: number) {
    let appConfig = JSON.parse(window.localStorage.getItem(`appConfig-${id}`)!);
    let route = 'invalid-route'
    if (appConfig) {
      route = `/app/${appConfig.id}/${appConfig.implementation}/${appConfig.context}/${appConfig.addon}`
    }
    this.router.navigate([route])
  }

  get potentialAppConfigs(): AppConfig[] {
    return this._potentialAppConfigs;
  }
}
