import {Component, CUSTOM_ELEMENTS_SCHEMA} from '@angular/core';
import {TranslocoModule} from "@ngneat/transloco";
import {AsyncPipe, NgClass, NgForOf, NgIf, NgOptimizedImage} from "@angular/common";
import {Observable} from "rxjs";
import {Article, NewsfeedService} from "../../../services/apps/newsfeed/newsfeed.service";
import "@ui5/webcomponents/dist/Card.js";
import "@ui5/webcomponents-fiori/dist/Timeline";
import "@ui5/webcomponents-fiori/dist/TimelineItem";
import {AppHeaderComponent} from "../app-header/app-header.component";

@Component({
  selector: 'app-information-leisure',
  templateUrl: './newsfeed.component.html',
  styleUrls: ['./newsfeed.component.css'],
  imports: [
    TranslocoModule,
    NgForOf,
    NgClass,
    AsyncPipe,
    NgIf,
    NgOptimizedImage,
    AppHeaderComponent,
  ],
  standalone: true,
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class NewsfeedComponent {
  constructor(
    private readonly newsfeedService: NewsfeedService
  ) {
    this.newsfeedService.retrieveProviders();
    this.newsfeedService.retrieveNews();
  }
  filterNewsByProvider$(provider: string): Observable<Article[]> {
    return this.newsfeedService.filterNewsByProvider$(provider)
  }
  get provider$() {
    return this.newsfeedService.newsProvider$;
  }

}
