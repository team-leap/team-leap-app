import {inject} from "@angular/core";
import {Router} from "@angular/router";
import {AppConfig, Block} from "../../model/model";
import {HttpClient, HttpErrorResponse} from "@angular/common/http";
import {environment} from "../../../environments/environment";
import {catchError, of} from "rxjs";
import {SecurityService} from "../../services/apps/security/security.service";
import {buildingBlocks} from "../../model/buildingBlocks";


interface TargetRoute{
  params: AppConfig
}


export const AppConfigGuard = async (state: TargetRoute) => {
  const auth = inject(SecurityService)
  const router = inject(Router);
  const appConfig: AppConfig = state.params;
  if(!await configIsValid(appConfig!)){
    return router.createUrlTree(['invalid-route'])
  }
  window.localStorage.setItem(`appConfig-${appConfig.id}`, JSON.stringify(appConfig));
  const addonId:Block = buildingBlocks[appConfig!.addon-1].name;
  if(addonId === 'Security' && !await isAuthenticated(auth)){
    return router.createUrlTree([routeBuilder(appConfig,'login')])
  }
  return true;
}

export const AppLoginGuard = async (state: TargetRoute) => {
  const router = inject(Router);
  const auth = inject(SecurityService)
  const appConfig = state.params;
  if(!await configIsValid(appConfig)){
    return router.createUrlTree(['invalid-route'])
  }
  if(await isAuthenticated(auth)){
    return router.createUrlTree([routeBuilder(appConfig)])
  }
  return true;
}

const isAuthenticated = async (auth: SecurityService) => {
  const securityToken = window.localStorage.getItem('securityToken')
  const user = window.localStorage.getItem('user')
  return await auth.isAuthenticated(user, securityToken)
}
const configIsValid = async (_appConfig: AppConfig): Promise<boolean> => {
  const http = inject(HttpClient);
  return new Promise<boolean>(resolve => {
    http.get<{appConfig: AppConfig, success:boolean}>(`${environment.backendUrl}/appconfig/${_appConfig.id}`)
      .pipe(
        catchError((error: HttpErrorResponse) => {
          return of(error)
        })
      )
      .subscribe(
      res => {
        if(res instanceof HttpErrorResponse){
          resolve(false)
        }else{
          if(res.success){
           const isValid = Object.keys(_appConfig).every(k => {
             return _appConfig[k as keyof AppConfig] == res.appConfig[k as keyof AppConfig]
           })
            resolve(isValid);
          }else{
            // TODO backend error
            resolve(false)
          }
        }
      }
    )
  })
}


const routeBuilder = (appConfig: AppConfig, route: string = '') => {
  return `/app/${appConfig.id}/${appConfig.implementation}/${appConfig.context}/${appConfig.addon}/${route}`
}
