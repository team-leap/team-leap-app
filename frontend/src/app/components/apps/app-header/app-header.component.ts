import {Component, CUSTOM_ELEMENTS_SCHEMA, Input} from '@angular/core';
import {NgIf, NgOptimizedImage} from "@angular/common";
import {TranslationService} from "../../../services/root/translation/translation.service";
import {SecurityService} from "../../../services/apps/security/security.service";

@Component({
  selector: 'app-header',
  templateUrl: './app-header.component.html',
  styleUrls: ['./app-header.component.css'],
  imports: [
    NgIf,
    NgOptimizedImage
  ],
  standalone: true,
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class AppHeaderComponent {
  @Input() public title: string = '';
  public hasSecurity: boolean;

  constructor(
    private readonly securityService: SecurityService,
    private readonly translationService: TranslationService
  ) {
    this.hasSecurity = !!this.securityService.hasSecurity();
  }

  public logout() {
    this.securityService.logout();
    window.location.reload();
  }

  switchLanguage() {
    this.translationService.switchLang()
  }
}
