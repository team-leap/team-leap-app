import {AfterContentInit, Component, CUSTOM_ELEMENTS_SCHEMA} from '@angular/core';
import {AsyncPipe, NgClass, NgForOf, NgIf, NgOptimizedImage} from "@angular/common";
import {TranslocoModule} from "@ngneat/transloco";
import {Chart} from 'chart.js/auto';
import {HttpClient} from "@angular/common/http";
import {environment} from "../../../../environments/environment";
import {LoadingSpinnerComponent} from "../../root/loading-spinner/loading-spinner.component";
import {LoadIndicatorService} from "../../../services/root/load-indicator/load-indicator.service";
import {BehaviorSubject} from "rxjs";

import "@ui5/webcomponents-icons/dist/AllIcons.js";
import "@ui5/webcomponents/dist/Card.js";
import "@ui5/webcomponents-fiori/dist/SideNavigationItem.js";
import "@ui5/webcomponents-fiori/dist/SideNavigation.js";
import "@ui5/webcomponents-fiori/dist/ShellBar.js";
import "@ui5/webcomponents-fiori/dist/ShellBarItem.js";
import "@ui5/webcomponents-fiori/dist/Bar.js";
import "@ui5/webcomponents/dist/Icon.js";
import "@ui5/webcomponents/dist/Popover.js";
import "@ui5/webcomponents/dist/Input.js";
import "@ui5/webcomponents/dist/TextArea.js";
import "@ui5/webcomponents/dist/Select.js";
import "@ui5/webcomponents/dist/ComboBox.js";
import "@ui5/webcomponents/dist/Tab.js";
import "@ui5/webcomponents/dist/Title.js";
import {AppHeaderComponent} from "../app-header/app-header.component";

interface DashboardDataAppsStatisticsEntry {
  context: number;
  implementation: number;
  addon: number;
  created_at: Date;
}

@Component({
  selector: 'app-information-business',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.css'],
  imports: [
    NgClass,
    NgIf,
    NgOptimizedImage,
    TranslocoModule,
    AppHeaderComponent,
    LoadingSpinnerComponent,
    AsyncPipe,
    NgForOf
  ],
  standalone: true,
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class DashboardComponent implements AfterContentInit {

  private readonly APP_TYPES = { // implementation -> context
    1: {
      3: {name: "Data dashboard", color: "#3aaa35"},
      4: {name: "News Feed", color: "#dabb65"},
    },
    2: {
      3: {name: "Kanban board", color: "#67e3d6"},
      4: {name: "To-do list", color: "#eeb9c0"},
    },
    5: {name: "Security", color: "#3aaa35"},
    6: {name: "Chatbot", color: "#f5a623"},
  }

  private appConfigOverviewChartElement!: HTMLCanvasElement;
  private appConfigOverviewChart!: Chart;

  private appConfigByTypeChartElement!: HTMLCanvasElement;
  private appConfigByTypeChart!: Chart<"pie", number[], unknown>;

  private appConfigByCustomizationChartElement!: HTMLCanvasElement;
  private appConfigByCustomizationChart!: Chart<"pie", number[], unknown>;

  private appConfigOverviewChartData: DashboardDataAppsStatisticsEntry[] = [];

  private hoursBack: BehaviorSubject<number> = new BehaviorSubject<number>(24);

  constructor(
    private readonly http: HttpClient,
    private readonly loadingSpinnerService: LoadIndicatorService,
  ) {
    this.hoursBack.subscribe(() => {
      this.updateCharts();
    });
  }

  ngAfterContentInit() {
    this.loadingSpinnerService.showLoadIndicator();
    const waitingForInitInterval = setInterval(() => {
      if (document.getElementById('appConfigOverviewChart')) {
        clearInterval(waitingForInitInterval);
        this.initializeChart();
      }
    }, 100);
  }

  initializeChart(): void {
    this.appConfigOverviewChartElement = document.getElementById('appConfigOverviewChart') as HTMLCanvasElement;
    const appConfigOverviewChartContext = this.appConfigOverviewChartElement.getContext('2d');

    if (appConfigOverviewChartContext) {
      this.appConfigOverviewChart = new Chart(appConfigOverviewChartContext, {
        type: 'line',
        data: {
          labels: ['Created apps'],
          datasets: [
            {
              label: 'Count',
              data: [],
              backgroundColor: 'rgba(75, 192, 192, 0.6)',
              borderColor: 'rgba(75, 192, 192, 1)',
              fill: false
            }
          ]
        },
        options: {
          scales: {
            y: {
              beginAtZero: true
            }
          },
          plugins: {
            title: {
              display: true,
              text: 'Created apps'
            },
            legend: {
              display: true,
              position: 'bottom'
            }
          }
        }
      });
    }

    this.appConfigByTypeChartElement = document.getElementById('appConfigByTypeChart') as HTMLCanvasElement;
    const appConfigByTypeChartContext = this.appConfigByTypeChartElement.getContext('2d'); // pie chart

    if (appConfigByTypeChartContext) {
      this.appConfigByTypeChart = new Chart(appConfigByTypeChartContext, {
        type: 'pie',
        data: {
          labels: [],
          datasets: [
            {
              label: 'Type',
              data: [],
              backgroundColor: [], // array of colors for each slice
            },
          ],
        },
        options: {
          plugins: {
            title: {
              display: true,
              text: 'App by Type',
            },
            legend: {
              display: true,
              position: 'bottom',
            },
          },
        },
      });
    }

    this.appConfigByCustomizationChartElement = document.getElementById('appConfigByCustomizationChart') as HTMLCanvasElement;
    const appConfigByCustomizationChartContext = this.appConfigByCustomizationChartElement.getContext('2d'); // pie chart

    if (appConfigByCustomizationChartContext) {
      this.appConfigByCustomizationChart = new Chart(appConfigByCustomizationChartContext, {
        type: 'pie',
        data: {
          labels: [],
          datasets: [
            {
              label: 'Customization',
              data: [],
              backgroundColor: [], // array of colors for each slice
            },
          ],
        },
        options: {
          plugins: {
            title: {
              display: true,
              text: 'App by Customization',
            },
            legend: {
              display: true,
              position: 'bottom',
            },
          },
        },
      });
    }

    const hoursBackInput = document.getElementById('time-span-selector') as HTMLInputElement;

    hoursBackInput.addEventListener('change', (event) => {
      const selectedOption = (event.target as HTMLInputElement).querySelector('[selected]') as HTMLOptionElement;
      const selectedValue = selectedOption.textContent || selectedOption.innerText;
      this.changeTimeSpan(selectedValue);
    });

    // select the option with the "24h" value
    {
      let items = hoursBackInput.querySelectorAll('ui5-option');
      items.forEach((item) => {
        if (item.getAttribute('data-value') === '2') {
          item.setAttribute('selected', '');
        } else {
          item.removeAttribute('selected');
        }
      });
    }

    this.refreshAppStatistics();
  }

  public refreshAppStatistics(): void {
    this.http.get<any>(`${environment.backendUrl}/app/dashboard/data`)
      .subscribe((data: any) => {
        this.appConfigOverviewChartData = data.apps;

        this.appConfigOverviewChartData = this.appConfigOverviewChartData.sort((a, b) => {
          return new Date(a.created_at).getTime() - new Date(b.created_at).getTime();
        });

        this.updateCharts();
      });
  }

  private updateCharts() {
    if (!this.appConfigOverviewChart) {
      return;
    }
    const filteredAppConfigOverviewChartData = this.filterDataForHoursBack(this.appConfigOverviewChartData);

    {
      const labels: string[] = [];
      const datasetData: number[] = [];

      const sumPerHour: { [key: string]: number } = {}; // date here is yyyy-mm-dd hh:00 to respect date changes
      for (let i = 0; i < this.hoursBack.value; i++) {
        const date = new Date();
        date.setHours(date.getHours() - i);
        const dateKey = this.formatDateLong(date)
        sumPerHour[dateKey] = 0;
      }

      for (let i = 0; i < filteredAppConfigOverviewChartData.length; i++) {
        const date = new Date(filteredAppConfigOverviewChartData[i].created_at);
        const dateKey = this.formatDateLong(date);
        sumPerHour[dateKey]++;
      }

      for (let i = 0; i < this.hoursBack.value; i++) {
        const date = new Date();
        date.setHours(date.getHours() - i);
        const dateKey = this.formatDateLong(date);
        // yyyy-mm-dd hh:00 to hh:00
        labels.push(dateKey.split(' ')[1]);
        datasetData.push(sumPerHour[dateKey]);
      }

      labels.reverse();
      datasetData.reverse();

      this.appConfigOverviewChart.data.labels = labels;
      this.appConfigOverviewChart.data.datasets[0].data = datasetData;
      this.appConfigOverviewChart.update();
    }

    {
      const labels: string[] = [];
      const datasetData: number[] = [];
      const colors: string[] = [];

      const appConfigByTypeChartData: { [key: string]: number } = {};
      const appConfigByTypeChartDataColors: { [key: string]: string } = {};
      for (let i = 0; i < filteredAppConfigOverviewChartData.length; i++) {
        const appConfig = filteredAppConfigOverviewChartData[i];
        // @ts-ignore
        const appType = this.APP_TYPES[appConfig.implementation][appConfig.context];
        if (!appConfigByTypeChartData[appType.name]) {
          appConfigByTypeChartData[appType.name] = 0;
          appConfigByTypeChartDataColors[appType.name] = appType.color;
        }
        appConfigByTypeChartData[appType.name]++;
      }

      for (const appName in appConfigByTypeChartData) {
        labels.push(appName);
        datasetData.push(appConfigByTypeChartData[appName]);
        colors.push(appConfigByTypeChartDataColors[appName]);
      }

      this.appConfigByTypeChart.data.labels = labels;
      this.appConfigByTypeChart.data.datasets[0].data = datasetData;
      this.appConfigByTypeChart.data.datasets[0].backgroundColor = colors;
      this.appConfigByTypeChart.update();
    }

    {
      const labels: string[] = [];
      const datasetData: number[] = [];
      const colors: string[] = [];

      const appConfigByCustomizationChartData: { [key: string]: number } = {};
      const appConfigByCustomizationChartDataColors: { [key: string]: string } = {};

      for (let i = 0; i < filteredAppConfigOverviewChartData.length; i++) {
        const appConfig = filteredAppConfigOverviewChartData[i];
        if (appConfig.addon == -1 || appConfig.addon == "-1") {
          continue;
        }
        // @ts-ignore
        const appType = this.APP_TYPES[appConfig.addon];
        if (!appConfigByCustomizationChartData[appType.name]) {
          appConfigByCustomizationChartData[appType.name] = 0;
          appConfigByCustomizationChartDataColors[appType.name] = appType.color;
        }
        appConfigByCustomizationChartData[appType.name]++;
      }

      for (const appName in appConfigByCustomizationChartData) {
        labels.push(appName);
        datasetData.push(appConfigByCustomizationChartData[appName]);
        colors.push(appConfigByCustomizationChartDataColors[appName]);
      }

      this.appConfigByCustomizationChart.data.labels = labels;
      this.appConfigByCustomizationChart.data.datasets[0].data = datasetData;
      this.appConfigByCustomizationChart.data.datasets[0].backgroundColor = colors;
    }

    // Hide the loading spinner and show the chart
    this.loadingSpinnerService.hideLoadIndicator();
    const chartElements = document.getElementsByClassName('chartHidden');
    for (let i = 0; i < chartElements.length; i++) {
      chartElements[i].classList.remove('chartHidden');
    }
  }

  private filterDataForHoursBack(values: any) {
    return values.filter((value: any) => {
      const date = new Date(value.created_at);
      const now = new Date();
      return now.getTime() - date.getTime() < this.hoursBack.value * 60 * 60 * 1000;
    });
  }

  private formatDateLong(date: Date): string {
    return `${date.getFullYear()}-${date.getMonth() + 1}-${date.getDate()} ${date.getHours()}:00`;
  }

  changeTimeSpan(priority: string) {
    const hoursBack = parseInt(priority.replaceAll(/[^0-9]+/g, ''));
    this.hoursBack.next(hoursBack);
  }
}
