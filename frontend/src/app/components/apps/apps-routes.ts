import {Route} from "@angular/router";
import {AppLoginComponent} from "./app-login/app-login.component";
import {AppConfigComponent} from "./app-config/app-config.component";
import {AppConfigGuard, AppLoginGuard} from "./apps-guards";

export const APPS_ROUTES:Route[] = [
  {
    path:'',
    component: AppConfigComponent,
    canActivate: [AppConfigGuard]
  },
  {
    path: 'login',
    component: AppLoginComponent,
    canActivate: [AppLoginGuard]
  }
]


