import {Component, CUSTOM_ELEMENTS_SCHEMA, Input, OnInit} from '@angular/core';
import {AsyncPipe, NgClass, NgForOf, NgIf} from "@angular/common";
import {sleep} from "../../monitor/helper-functions";
import {chatbotEvents} from "./chatbot-events";
import "@ui5/webcomponents-icons/dist/AllIcons.js";
import "@ui5/webcomponents/dist/Icon.js";
import "@ui5/webcomponents/dist/Button.js";
import {Block} from "../../../model/model";
import {TranslationService} from "../../../services/root/translation/translation.service";

export interface Config{
  implementation: Block,
  context: Block,
  addon?: Block
}
@Component({
  selector: 'app-conversational-ai',
  templateUrl: './app-conversational-ai.component.html',
  styleUrls: ['./app-conversational-ai.component.css'],
  standalone:true,
  imports: [NgClass, NgIf, NgForOf, AsyncPipe],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})


export class AppConversationalAiComponent implements OnInit{

  public chatIsOpen = false;
  public showAnswer = false;


  public events = [
    'Sovanta AG',
    'SAP BTP',
    'App'
  ]

  @Input() appConfig: Config|undefined;

  constructor(
    private readonly translationService:TranslationService
  ) {}

  ngOnInit() {
   this.showInitText();
  }

  public deleteEvent(){
    this.showAnswer = false;
    this.showChooseEventText()
  }
  public triggerEvent(event:string){
    this.showAnswer = true
    let translationKey = '';
    let addonTranslation: string | undefined;
    if(event === 'App'){
      if(this.appConfig){
        const base = chatbotEvents.get(`${this.appConfig.implementation}-${this.appConfig.context}`);
        translationKey = base ? base : ''
        if(this.appConfig.addon){
          const addon = chatbotEvents.get(this.appConfig.addon);
          addonTranslation = addon ? addon : undefined;
        }
      }
    }else{
      const other = chatbotEvents.get(event);
      translationKey = other ? other : ''
    }

    setTimeout(async () => {
      let translation = await this.translationService.translate(translationKey);
      if(addonTranslation){
        const addon = await this.translationService.translate(addonTranslation);
        translation = `${translation} ${addon}`
      }
      this.revealTextStepByStep(translation,'chat-text')
    },200)

  }

  private showChooseEventText(){
    setTimeout(async () => {
      const text = await this.translationService.translate('chatbot.chooseEvent');
      this.revealTextStepByStep(text,'choose-event')
    },0)
  }

  private showInitText(){
    setTimeout(async () => {
      const text = await this.translationService.translate('chatbot.help');
      this.revealTextStepByStep(text,'chat-init')
    },0)
  }
  public toggleChat(){
    this.chatIsOpen = !this.chatIsOpen
    if(!this.chatIsOpen){
      this.showAnswer = false
      this.showInitText()
    }else{
     this.showChooseEventText()
    }
  }

  private async revealTextStepByStep(text: string, elementId: string) {
    const textBox = document.getElementById(elementId);
    let wholeText = [];
    for (const letter of [...text]) {
      wholeText.push(letter)
      if(textBox){
        textBox.innerText = wholeText.join('')
      }
      await sleep(35);
    }
  }
}
