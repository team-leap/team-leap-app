﻿export const chatbotEvents: Map<string,string> = new Map<string,string>(
  [
    ['Sovanta AG','chatbot.sovanta'],
    ['SAP BTP','chatbot.btp'],
    ['Information-Leisure','chatbot.informationLeisure'],
    ['Information-Business','chatbot.informationBusiness'],
    ['Planning-Leisure','chatbot.planningLeisure'],
    ['Planning-Business','chatbot.planningLeisure'],
    ['Conversational AI','chatbot.ai'],
  ]
)




