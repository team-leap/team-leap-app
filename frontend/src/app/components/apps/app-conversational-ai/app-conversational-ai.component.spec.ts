import 'zone.js';
import 'zone.js/dist/zone-testing';
import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AppConversationalAiComponent } from './app-conversational-ai.component';
import {BrowserDynamicTestingModule, platformBrowserDynamicTesting} from "@angular/platform-browser-dynamic/testing";

describe('AppConversationalAiComponent', () => {
  let component: AppConversationalAiComponent;
  let fixture: ComponentFixture<AppConversationalAiComponent>;

  beforeEach(async () => {
    TestBed.initTestEnvironment(
      BrowserDynamicTestingModule,
      platformBrowserDynamicTesting()
    );
    await TestBed.configureTestingModule({
    })
    .compileComponents();

    fixture = TestBed.createComponent(AppConversationalAiComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
