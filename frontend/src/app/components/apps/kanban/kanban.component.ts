import {Component, CUSTOM_ELEMENTS_SCHEMA} from '@angular/core';
import {CommonModule, NgForOf, NgIf, NgOptimizedImage} from "@angular/common";
import {KanbanEntry, KanbanService} from "../../../services/apps/kanban/kanban.service";
import "@ui5/webcomponents-icons/dist/AllIcons.js";
import "@ui5/webcomponents/dist/Card.js";
import "@ui5/webcomponents-fiori/dist/SideNavigationItem.js";
import "@ui5/webcomponents-fiori/dist/SideNavigation.js";
import "@ui5/webcomponents-fiori/dist/ShellBar.js";
import "@ui5/webcomponents-fiori/dist/ShellBarItem.js";
import "@ui5/webcomponents-fiori/dist/Bar.js";
import "@ui5/webcomponents/dist/Icon.js";
import "@ui5/webcomponents/dist/Popover.js";
import "@ui5/webcomponents/dist/Input.js";
import "@ui5/webcomponents/dist/TextArea.js";
import "@ui5/webcomponents/dist/Select.js";
import "@ui5/webcomponents/dist/ComboBox.js";
import "@ui5/webcomponents/dist/Tab.js";
import "@ui5/webcomponents/dist/Title.js";
import {TranslocoModule} from "@ngneat/transloco";
import {ToastService} from "../../../services/root/toast/toast.service";
import {TodoService} from "../../../services/apps/todo/todo";
import {WebsocketService} from "../../../services/apps/websocket/websocket.service";
import {AppHeaderComponent} from "../app-header/app-header.component";

@Component({
  selector: 'app-planning-business',
  templateUrl: './kanban.component.html',
  styleUrls: ['./kanban.component.css'],
  imports: [
    NgForOf,
    NgIf,
    CommonModule,
    TranslocoModule,
    NgOptimizedImage,
    AppHeaderComponent
  ],
  standalone: true,
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class KanbanComponent {

  private readonly MAX_TITLE_LENGTH = 255;

  private popover: any;

  constructor(
    private readonly todoService: TodoService,
    private readonly kanbanService: KanbanService,
    private readonly toastService: ToastService,
    private readonly websocketService: WebsocketService
  ) {
    this.websocketService.registerUserToWebsocketChannel(() => {
      this.kanbanService.listKanbanRows();
    });
  }

  ngOnInit() {
    this.kanbanService.listKanbanRows();
    this.initPopover();
  }

  private initPopover() {
    if (!this.popover) {
      this.popover = document.getElementById('kanban-entry-editor');
    }
  }

  get todoElements$() {
    return this.kanbanService.todoElements$;
  }

  get inProgressElements$() {
    return this.kanbanService.inProgressElements$;
  }

  get doneElements$() {
    return this.kanbanService.doneElements$;
  }

  /**
   * Transforms a number into a task id of the form:
   * SOV-001
   * SOV-1001
   * SOV-10001
   * @param id
   */
  formatTaskId(id: number) {
    return 'SOV-' + id.toString().padStart(3, '0');
  }

  /**
   * returns:
   * - 0: 'icon-expand-group'
   * - 1: 'icon-open-command-field'
   * - 2: 'icon-collapse-group'
   * @param priority number from 0 to 2
   */
  getPriorityIcon(priority: number): string {
    switch (priority) {
      case 0:
        return 'icon-expand-group';
      case 1:
        return 'icon-open-command-field';
      case 2:
        return 'icon-collapse-group';
      default:
        return 'icon-expand-group';
    }
  }

  openTaskPopover(id: number) {
    const task = this.kanbanService.getTaskById(id);
    this.initPopover();

    const popoverRef = document.getElementById('popover-ref');

    if (!this.popover) {
      this.toastService.showErrorToast("kanban.messages.unknownErrorDuringDataParsing");
      return;
    }
    if (!task) {
      this.toastService.showErrorToast("kanban.messages.taskDoesNotExist");
      return;
    }
    /*if (!clickedElement) {
      console.error("clickedElement not found")
      this.toastService.showErrorToast("kanban.messages.unknownErrorDuringDataParsing");
      return;
    }*/
    if (!popoverRef) {
      this.toastService.showErrorToast("kanban.messages.unknownErrorDuringDataParsing");
      return;
    }

    this.popover.setAttribute('header-text', this.formatTaskId(id));

    const title = document.getElementById('kanban-entry-editor-title');
    const description = document.getElementById('kanban-entry-editor-description');
    const priority = document.getElementById('kanban-entry-editor-priority');
    const status = document.getElementById('kanban-entry-editor-status');

    if (title) {
      title.setAttribute('value', task.title);
    }
    if (description) {
      description.setAttribute('value', task.description);
    }
    if (priority) {
      let items = priority.querySelectorAll('ui5-option');
      items.forEach((item) => {
        if (item.getAttribute('data-value') === task.priority.toString()) {
          item.setAttribute('selected', 'true');
        } else {
          item.removeAttribute('selected');
        }
      });
    }
    if (status) {
      let items = status.querySelectorAll('ui5-option');
      items.forEach((item) => {
        if (item.getAttribute('data-value') === task.status.toString()) {
          item.setAttribute('selected', 'true');
        } else {
          item.removeAttribute('selected');
        }
      });
    }

    this.popover.showAt(popoverRef);
  }

  saveTaskFromPopover() {
    const title = document.getElementById('kanban-entry-editor-title');
    const description = document.getElementById('kanban-entry-editor-description');
    const priority = document.getElementById('kanban-entry-editor-priority');
    const status = document.getElementById('kanban-entry-editor-status');

    if (!title || !description || !priority || !status) {
      this.toastService.showErrorToast("kanban.messages.unknownErrorDuringDataParsing");
      return;
    }

    const titleValue = title.getAttribute('value') || "";
    const descriptionValue = description.getAttribute('value') || "";

    let priorityValue = "0";
    {
      let items = priority.querySelectorAll('ui5-option');
      items.forEach((item) => {
        if (item.getAttribute('selected') === 'true' || item.getAttribute('selected') === '') {
          priorityValue = item.getAttribute('data-value') || "0";
        }
      });
    }

    let statusValue = "0";
    {
      let items = status.querySelectorAll('ui5-option');
      items.forEach((item) => {
        if (item.getAttribute('selected') === 'true' || item.getAttribute('selected') === '') {
          statusValue = item.getAttribute('data-value') || "0";
        }
      });
    }

    if (titleValue.length > this.MAX_TITLE_LENGTH) {
      this.toastService.showErrorToast("kanban.messages.titleTooLong");
      return;
    }

    const taskId = this.popover.getAttribute('header-text').replace('SOV-', '');

    const task = this.kanbanService.getTaskById(parseInt(taskId));
    if (!task) {
      this.toastService.showErrorToast("kanban.messages.taskDoesNotExist");
      return;
    }

    task.id = parseInt(taskId);
    task.title = titleValue;
    task.description = descriptionValue;
    task.priority = parseInt(priorityValue);
    task.status = parseInt(statusValue);

    this.kanbanService.updateKanbanRow(task.id, task);
    this.popover.close();
  }

  deleteTaskFromPopover() {
    const taskId = this.popover.getAttribute('header-text').replace('SOV-', '');
    this.kanbanService.deleteKanbanRow(parseInt(taskId));
    this.popover.close();
  }

  addTask(column: number, clickedElement: EventTarget | null) {
    const task: Partial<KanbanEntry> = {
      title: "New Task",
      description: "",
      priority: 1,
      status: column
    };

    if (clickedElement) {
      (<HTMLElement> clickedElement).setAttribute('icon', 'lateness');
    }

    this.kanbanService.createKanbanRow(task, () => {
      if (clickedElement) {
        (<HTMLElement> clickedElement).setAttribute('icon', 'add');
      }
    });
  }
}
