import {Component, OnDestroy, OnInit} from '@angular/core';
import {ActivatedRoute} from "@angular/router";
import {catchError, of, Subscription} from "rxjs";
import {NgIf, NgSwitch, NgSwitchCase} from "@angular/common";
import {AppConversationalAiComponent, Config} from "../app-conversational-ai/app-conversational-ai.component";
import { Block} from "../../../model/model";
import {buildingBlocks} from "../../../model/buildingBlocks";
import {TodoComponent} from "../todo/todo.component";
import {KanbanComponent} from "../kanban/kanban.component";
import {NewsfeedComponent} from "../newsfeed/newsfeed.component";
import {DashboardComponent} from "../dashboard/dashboard.component";

@Component({
  selector: 'app-app-config',
  templateUrl: './app-config.component.html',
  styleUrls: ['./app-config.component.css'],
  standalone: true,
  imports: [
    NgSwitchCase,
    NgSwitch,
    AppConversationalAiComponent,
    NgIf,
    DashboardComponent,
    NewsfeedComponent,
    KanbanComponent,
    TodoComponent
  ]
})
export class AppConfigComponent implements OnInit,OnDestroy {

  private _subs = new Subscription();
  private _appConfigId:string|undefined;
  private _implementationId:Block = '';
  private _contextId:Block = '';
  private _addonId: Block = '';
  public _appConfig: Config | undefined;

  constructor(
    private readonly activatedRoute: ActivatedRoute,
  ) {
  }

  ngOnInit(): void {
    this._subs.add(this.activatedRoute.paramMap
      .pipe(
        catchError((error) => {
          return of(error)
        })
      )
      .subscribe(params => {
        if(params){
          this._appConfigId = params.get('id');
          this._implementationId = buildingBlocks[params.get('implementation')-1].name;
          this._contextId = buildingBlocks[params.get('context')-1].name;
          this._addonId = buildingBlocks[params.get('addon')-1].name;
          this._appConfig = {
            implementation: this._implementationId,
            context: this._contextId,
            addon: this._addonId
          }
        }
    }));
  }
  get implementationId(): string | undefined {
    return this._implementationId;
  }

  get contextId(): string | undefined{
    return this._contextId;
  }
  get addonId(): string | undefined {
    return this._addonId;
  }

  ngOnDestroy(): void {
    this._subs.unsubscribe();
  }

}
