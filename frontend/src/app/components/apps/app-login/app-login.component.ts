import {Component} from '@angular/core';
import {Router} from "@angular/router";
import {SecurityService} from "../../../services/apps/security/security.service";
import {TranslocoModule} from "@ngneat/transloco";

@Component({
  selector: 'app-login',
  templateUrl: './app-login.component.html',
  styleUrls: ['./app-login.component.css'],
  imports: [
    TranslocoModule
  ],
  standalone: true
})
export class AppLoginComponent {
  constructor(
    private readonly securityService: SecurityService,
    private readonly router: Router
  ) {
  }

  public register(username: string, password: string) {
    this.securityService.createUser(username, password, () => {
    });
  }

  public async login(username: string, password: string) {
    await this.securityService.login(username, password)
    let route = window.location.pathname.split('/');
    route.pop();
    this.router.navigate([route.join('/')])
  }

  public registerAndLogin(username: string, password: string) {
    this.securityService.createUser(username, password, sucess => {
      if (sucess) {
        this.login(username, password);
      }
    });
  }
}
