import {Component, CUSTOM_ELEMENTS_SCHEMA} from '@angular/core';
import {AsyncPipe, NgForOf, NgIf, NgOptimizedImage} from "@angular/common";
import "@ui5/webcomponents-icons/dist/AllIcons.js";
import "@ui5/webcomponents/dist/Card.js";
import "@ui5/webcomponents-fiori/dist/SideNavigationItem.js";
import "@ui5/webcomponents-fiori/dist/SideNavigation.js";
import "@ui5/webcomponents-fiori/dist/ShellBar.js";
import "@ui5/webcomponents-fiori/dist/ShellBarItem.js";
import "@ui5/webcomponents-fiori/dist/Bar.js";
import "@ui5/webcomponents/dist/Icon.js";
import "@ui5/webcomponents/dist/Popover.js";
import "@ui5/webcomponents/dist/Input.js";
import {TranslocoModule} from "@ngneat/transloco";
import {MiniLoadingSpinnerComponent} from "../../root/mini-loading-spinner/mini-loading-spinner.component";
import {TodoService} from "../../../services/apps/todo/todo";
import {SecurityService} from "../../../services/apps/security/security.service";
import {WebsocketService} from "../../../services/apps/websocket/websocket.service";
import {AppHeaderComponent} from "../app-header/app-header.component";

@Component({
  selector: 'app-planning-leisure',
  templateUrl: './todo.component.html',
  styleUrls: ['./todo.component.css'],
    imports: [
        NgIf,
        AsyncPipe,
        NgForOf,
        TranslocoModule,
        MiniLoadingSpinnerComponent,
        NgOptimizedImage,
        AppHeaderComponent,
    ],
  standalone: true,
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class TodoComponent {

  public todoEditing: any;
  public context: any;
  private popover: any;
  private popoverRef: any;
  public hasSecurity: boolean;


  constructor(
    private readonly todo: TodoService,
    private readonly securityService: SecurityService,
    private readonly websocketService: WebsocketService
  ) {
    this.hasSecurity = !!this.securityService.hasSecurity();
    this.todo.retrieveTodos();

    this.websocketService.registerUserToWebsocketChannel(() => {
      this.todo.retrieveTodos();
    });
  }

  public openModal(context: string, todo: any = undefined) {
    this.context = context;
    this.todoEditing = todo;
    this.popoverRef = document.getElementById("popoverRef");
    this.popover = document.getElementById("popover");
    this.popover.setAttribute('header-text', context)
    if (todo) {
      this.popover.setAttribute('placeholder', todo.text)

      const todoInput = document.getElementById('todoInput');
      if (todoInput) {
        todoInput.setAttribute('value', todo.text);
      }
    } else {
      this.popover.setAttribute('placeholder', 'Todo Text ...')

      const todoInput = document.getElementById('todoInput');
      if (todoInput) {
        todoInput.setAttribute('value', '');
      }
    }
    this.popover.showAt(this.popoverRef);
  }

  public closeModal() {
    if (this.popover) {
      this.popover.close();
    }
  }

  private getPopoverValue(): string | undefined {
    if (this.popover) {
      const inputElement = document.getElementById('todoInput');
      if (inputElement) {
        const value = inputElement.getAttribute('value');
        if (value) {
          return value
        }
      }
    }
    return undefined
  }

  public editTodo() {
    const newTodo = this.getPopoverValue();
    if (newTodo && this.todoEditing) {
      this.todo.editTodo(this.todoEditing.id, newTodo);
      this.closeModal();
    }
  }

  public createTodo() {
    const newTodo = this.getPopoverValue();
    if (newTodo) {
      this.todo.createTodo(newTodo);
      this.closeModal()
    }
  }

  public deleteTodo(id: number) {
    this.todo.deleteTodo(id);
  }


  public setTodoDone(id: number) {
    this.todo.setTodoDone(id);
  }

  public setTodoInProgress(id: number) {
    this.todo.setTodoInProgress(id);
  }
  get todosInProgress$() {
    return this.todo.todosInProgress$;
  }

  get todosDone$() {
    return this.todo.todosDone$;
  }
}
