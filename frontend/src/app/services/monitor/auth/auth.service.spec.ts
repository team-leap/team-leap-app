import 'zone.js';
import 'zone.js/dist/zone-testing';
import { TestBed } from '@angular/core/testing';
import { AuthService } from './auth.service';
import {BrowserDynamicTestingModule, platformBrowserDynamicTesting} from "@angular/platform-browser-dynamic/testing";
import {HttpClient} from "@angular/common/http";
import {HttpClientTestingModule} from "@angular/common/http/testing";
import {LoggerService} from "../../root/logger/logger.service";
import {RouterTestingModule} from "@angular/router/testing";
import {ErrorHandlerService} from "../../root/error-handler/error-handler.service";
import {ToastService} from "../../root/toast/toast.service";
import {Router} from "@angular/router";

describe('AuthService', () => {
  let service: AuthService;

  beforeEach(() => {
    TestBed.initTestEnvironment(
      BrowserDynamicTestingModule,
      platformBrowserDynamicTesting()
    );
    TestBed.configureTestingModule({
      providers:[
        {provide: HttpClient, useClass: HttpClientTestingModule},
        {provide: LoggerService, useValue: {}},
        {provide: Router, useClass: RouterTestingModule},
        {provide: ErrorHandlerService, useValue: {}},
        {provide: ToastService, useValue: {}},
      ]
    });
    service = TestBed.inject(AuthService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
