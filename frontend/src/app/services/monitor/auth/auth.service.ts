import { Injectable } from '@angular/core';
import {HttpClient, HttpErrorResponse} from "@angular/common/http";
import {environment} from "../../../../environments/environment";
import {catchError, of} from "rxjs";
import {LoggerService} from "../../root/logger/logger.service";
import {Router} from "@angular/router";
import {ErrorHandlerService} from "../../root/error-handler/error-handler.service";
import {ToastService} from "../../root/toast/toast.service";

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  constructor(
    private readonly http:HttpClient,
    private readonly logger: LoggerService,
    private readonly router: Router,
    private readonly errorHandlerService:ErrorHandlerService,
    private readonly toastService:ToastService
  ) { }

  // check if given token is valid
  public async isAuthenticated():Promise<boolean>{
    this.logger.standardLogger('AuthService.AuthService.isAuthenticated', 'method called')
    return new Promise<boolean>((resolve) => {
        this.http.get<any>(`${environment.backendUrl}/monitor/validate`)
          .pipe(
            catchError((error: HttpErrorResponse) => {
              return of(error)
            })
          )
          .subscribe(res => {
            if(res instanceof HttpErrorResponse){
              this.errorHandlerService.handleError(res,'AuthService.isAuthenticated')
              resolve(false);
            }else{
              this.logger.standardLogger('AuthService.isAuthenticated','method called successfully', res)
              resolve(res.success)
            }
          })
    }
    )
  }

  // retrieves admin token which grants access for all monitor routes
  public async retrieveAdminToken(): Promise<void> {
    this.logger.standardLogger('AuthService.retrieveAdminToken', 'method called')
    return new Promise<void>((resolve) => {
      this.http.get<any>(`${environment.backendUrl}/monitor/register`)
        .pipe(
          catchError((error: HttpErrorResponse) => {
            return of(error)
          })
        )
        .subscribe(res => {
            if(res instanceof HttpErrorResponse){
              this.errorHandlerService.handleError(res,'AuthService.retrieveAdminToken')
              resolve()
            }else{
              if(res.success){
                window.localStorage.setItem('adminToken', res.token);
                this.toastService.showSuccessToast('success.retrieveAdminToken')
                this.logger.standardLogger('AuthService.retrieveAdminToken', 'retrieved admin token successfully', res);
                resolve();
              }else{
                let translationKey = 'error.retrieveAdminToken'
                if(res.detail == 'VHlib255TmNjeXZwbmd2YmFGZ25nclpuYW50cmU6MjE0'){
                  // token already retrieved
                  translationKey = 'error.retrieveAdminTokenAlreadyRetrieved'
                }
                // server error retrieving token
                this.toastService.showErrorToast(translationKey)
                this.logger.standardLogger('AuthService.retrieveAdminToken', 'failed retrieving admin token', res)
                resolve()
              }
            }
          }
        )
    })
  }

  public getAdminToken(): string {
    const token = window.localStorage.getItem('adminToken');
    return token ? token : '';
  }
}
