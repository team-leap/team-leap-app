import {Injectable, OnDestroy} from '@angular/core';
import {HttpClient, HttpErrorResponse} from "@angular/common/http";
import {environment} from "../../../../environments/environment";
import {
  BehaviorSubject,
  catchError,
  map,
  Observable,
  of,
  Subscription
} from "rxjs";
import {AppConfig, AppState, AppStateDTO, BuildingBlock, Category, State} from "../../../model/model";
import {Router} from "@angular/router";
import {WebSocketSubject} from "rxjs/internal/observable/dom/WebSocketSubject";
import {webSocket} from "rxjs/webSocket";
import {buildingBlocks, emptyBlock} from "../../../model/buildingBlocks";
import {LoggerService} from "../../root/logger/logger.service";
import {ErrorHandlerService} from "../../root/error-handler/error-handler.service";
import {ToastService} from "../../root/toast/toast.service";


@Injectable({
  providedIn: 'root'
})
export class MonitorStateService implements OnDestroy{
  private _currentAppState$ = new BehaviorSubject<AppStateDTO | undefined>(undefined);
  private _monitorWebSocket$: WebSocketSubject<any> | undefined;
  private _subs = new Subscription();
  private _blocksByCategory$ = new BehaviorSubject<Array<Array<BuildingBlock>>>([])
  private _categories: Category[] = ['implementation', 'context', 'customization', 'none'];
  private _isAdminRoute$ = new BehaviorSubject<boolean>(false);
  private _appDeploying$ = new BehaviorSubject<boolean>(false);
  constructor(
    private readonly http:HttpClient,
    private readonly logger:LoggerService,
    private readonly errorHandler:ErrorHandlerService,
    private readonly router: Router,
    private readonly toastService: ToastService,
  ) {
    this.initBlocksByCategories();
  }
  public setAdminRoute(){
    this._isAdminRoute$.next(true);
  }
  public async retrieveAppState(): Promise<AppState>{
    this.logger.standardLogger('AppStateService.retrieveAppState', 'method called')
    return new Promise<AppState>(resolve => {
      this.http.get<AppStateDTO>(`${environment.backendUrl}/monitor/state`)
        .pipe(
          catchError((error: HttpErrorResponse) => {
            return of(error)
          })
        )
        .subscribe(res => {
          if(res instanceof HttpErrorResponse){
            this.errorHandler.handleError(res, 'AppStateService.retrieveAppState')
          }else{
            if(res.success){
              this.logger.standardLogger('AppStateService.retrieveAppState', 'retrieved app state successfully',res.state)
              this._currentAppState$.next(res);
              if(res.state.activeBlocks){
                const blocks = res.state.activeBlocks;
                if(blocks.length === 0){
                  this.shiftToCategory('none')
                }else if(buildingBlocks[blocks[blocks.length-1]-1]){
                  const category = buildingBlocks[blocks[blocks.length-1]-1].category;
                  this.shiftToCategory(category);
                }
              }
              resolve(res.state)
            }else{
              this.logger.standardLogger('AppStateService.retrieveAppState', 'failed retrieving app state', res)
            }
          }
        })
    })
  }
  private initBlocksByCategories(){
    let newValue:Array<Array<BuildingBlock>> = [];
    this._categories.forEach(category => {
      if(category === 'none'){
        newValue.push([emptyBlock]);
        return;
      }
      const blocksByCategory: BuildingBlock[] = buildingBlocks.filter(block => {
        return block.category === category
      })
      let result:BuildingBlock[] = []
      blocksByCategory.forEach((block:BuildingBlock) => {
        result.push(block)
      })
      newValue.push(result);
    })
    this._blocksByCategory$.next(newValue);
  }
  private insertLearningBlock(learningBlock: number){
    let newState = this._currentAppState$.value;
    if(newState){
      newState.state.learningBlock = learningBlock
      this._currentAppState$.next(newState);
    }
  }
  private removeBlockFromActiveBlocksById(id: number, error:string){
    let newState = this._currentAppState$.value;
    if(newState){
      newState.state.activeBlocks = newState.state.activeBlocks.filter(block => {
        return block !== id
      })
      if(newState.state.activeBlocks.length === 0){
        this.shiftToCategory('none')
      }
      newState.state.invalidConfiguration = error
      this._currentAppState$.next(newState);
    }
    this.updateCategoryOnRemove();
  }
  private insertBlockIntoActiveBlocks(id: number, error: string){
    let newState = this._currentAppState$.value;
    if(newState){
      if(newState.state.activeBlocks){
        newState.state.activeBlocks.push(id);
        newState.state.invalidConfiguration = error
        this._currentAppState$.next(newState);
      }
    }
  }
  private updateAppState(state: State){
    let newState = this._currentAppState$.value;
    if(newState){
      newState.state.state = state
      this._currentAppState$.next(newState);
    }
  }
  private updateButtonState(count: number){
    let newState = this._currentAppState$.value;
    if(newState){
      newState.state.successiveDeployButtonPresses = count
      if(count === 1){
        this.router.navigate(['config-locked'])
      }else if(count === 0){
        this.router.navigate(['building'])
      }
      this._currentAppState$.next(newState);
    }
  }
  public connectToWebSocketChannel(): void {
    this.logger.standardLogger('BuildingLearningService.connectToWebSocketChannel', 'method called ...')
    if(this._monitorWebSocket$){
      return;
    }
    this._monitorWebSocket$ = webSocket(environment.websocketUrl);
    this._subs.add(
      this._monitorWebSocket$
      .subscribe(message => {
        this.handleMessage(message)
      }))
    this.logger.standardLogger('BuildingLearningService.connectToWebSocketChannel', 'connected successfully')
  }
  // routes back to /building if all blocks are removed from deployment board after deployment
  private resetFromDeployment(){
    this.updateAppState('BUILDING_WAITING');
    this.updateButtonState(0);
    this.router.navigate(['building'])
  }
  private updateKeyCardDetection(detected: boolean) {
    let newState = this._currentAppState$.value;
    if (newState) {
      newState.state.adminCard = detected
      this._currentAppState$.next(newState);
    }
  }
  // handles all incoming websocket messages
  private handleMessage(message: any){
    this.logger.standardLogger('AppStateService','websocket message received',message)
    switch (message.action){
      case 'showLearningBlock':
        this.insertLearningBlock(message.type)
        break;
      case 'monitorShowMessage':
        // trying to remove a block which doesnt exist makes no sense in prod
        if(message.messageId === 'block_not_active')
          return;
        this.showWarning(message.messageId)
        break;
      case 'stateChanged':
        switch (message.change) {
          case 'block_added':
            // 7 means admin card -> dont insert into active blocks
            if(message.block === 7){
              this.updateKeyCardDetection(true);
              return;
            }
            this.insertBlockIntoActiveBlocks(message.block, message.error);
            this.updateCategory(message.block);
            break;
          case 'block_removed':
            if(message.block === 7) {
              // 7 means admin card -> dont insert into active blocks
              this.updateKeyCardDetection(false);
              return;
            }
            this.removeBlockFromActiveBlocksById(message.block, message.error);
            break;
          case 'all_blocks_removed':
            this.resetFromDeployment();
            break;
          case 'button_pressed':
            this.updateButtonState(message.count)
            break;
          case 'app_deployed':
            this.deployApp(message.appconfig)
            break;
        }
        break;
      case 'monitorInvalidConfiguration':
        this.showWarning(message.messageId)
        break;
      case 'stopLearning':
        this.router.navigate(['building'])
        this.toastService.showSuccessToast('learning.learningSucceeded')
        break;
      case 'deviceStateChanged':
        this.updateDeviceState(message.data)
        break;

    }
  }
  private updateCategory(learningBlock: number){
    const category = buildingBlocks[learningBlock-1].category;
    this.shiftToCategory(category);
  }
  private updateCategoryOnRemove(){
    const value = this._currentAppState$.value;
    if(value){
      const activeBlocks = value.state.activeBlocks;
      const category = buildingBlocks[activeBlocks[activeBlocks.length-1]-1]?.category || 'none';
      this.shiftToCategory(category)
    }
  }
  // /building page -> shifts card of last selected block (its category) to main card position
  private shiftToCategory(category: Category){
    let shiftComplete = false;
    while(!shiftComplete){
      let value = this._blocksByCategory$.value;
      if(value){
        const lastElement = value.pop();
        if(lastElement){
          value.unshift(lastElement)
          this._blocksByCategory$.next(value)
          if(value[0][0].category === category){
            shiftComplete = true;
          }
        }
      }
    }
  }
  private updateDeviceState(state: boolean){
    let newState = this._currentAppState$.value;
    if (newState) {
      newState.state.devices.raspberry_pi.active = state;
      this._currentAppState$.next(newState);
    }
  }
  private showWarning(messageId: string){
    this.toastService.showWarningToast(`warnings.websocketMessages.${messageId}`)
  }
  public startLearning(): void{
    this.logger.standardLogger('BuildingLearningService.startLearning', 'method called');
    this.http.get<any>(`${environment.backendUrl}/monitor/learning`)
      .pipe(
        catchError((error: HttpErrorResponse) => {
          return of(error)
        })
      )
      .subscribe(res => {
        if(res instanceof HttpErrorResponse){
          this.errorHandler.handleError(res, 'AppStateLearning.startLearning')
        }else{
          if(res.success){
            this.updateAppState('LEARNING')
            this.logger.standardLogger('startLearning', 'start learning mode successfully', res)
          }else{
            this.logger.standardLogger('startLearning', 'failed to start learning mode', res);
          }
        }
      })
  }
  public setAppDeployingFlagToFalse(){
    this._appDeploying$.next(false)
  }

  public setAppDeployed(){
    setTimeout(() => {
        this.updateAppState('APP_DEPLOYED')
        // qr code is shown as soon as app state is APP_DEPLOYED - rocket animation is shown while _appDeploying$ == true
        // to esnure that qr code appears in disappearing smoke animation make is visible shortly before animation is hidden again due to state change
      }, environment.deploymentTimer-(environment.deploymentTimer/5))
  }
  private deployApp(appConfig: AppConfig){
    if(this._currentAppState$.value?.state.state !== 'APP_DEPLOYED'){
      window.localStorage.setItem('appConfig', JSON.stringify(appConfig));
      this._appDeploying$.next(true);
    }
  }
  get currentAppState$(): Observable<AppStateDTO | undefined>{
    return this._currentAppState$.asObservable();
  }
  get blocksByCategory$(): Observable<BuildingBlock[][]>{
    return this._blocksByCategory$.asObservable();
  }
  get activeBlocks$(): Observable<BuildingBlock[]>{
    return this._currentAppState$.pipe(
      map(state => {
        if(state){
          const noAdminBlocks = state?.state.activeBlocks.filter(block => {
            // filter out admin blocks from active blocks which are used on configLocked page
            return block !== 7
          })
          return noAdminBlocks.map(block => {
              return buildingBlocks[block-1]
          }).sort((a,b) => {
            // sorting for configLocked page -> that rocket base is implementation - fuel is context and tip is customization always
            if((a.category === 'implementation') || (a.category === 'context' && b.category === 'customization')){
              return 1
            }
            return -1
          })
        }
        return []
      })
    )
  }
  // returns active block of implementation customization (always shows last added block of this category)
  get activeImplementationBlocks$(): Observable<BuildingBlock>{
    return this.activeBlocks$.pipe(
      map(blocks => {
        return blocks.filter((block) => block.category === 'implementation')[0]
      })
    );
  }
  // returns active block of category context (always shows last added block of this category)
  get activeContextBlocks$(): Observable<BuildingBlock>{
    return this.activeBlocks$.pipe(
      map(blocks => {
        return blocks.filter((block) => block.category === 'context')[0]
      })
    );
  }
  // returns active block of category customization (always shows last added block of this category)
  get activeCustomizationBlocks$(): Observable<BuildingBlock>{
    return this.activeBlocks$.pipe(
      map(blocks => {
        return blocks.filter((block) => block.category === 'customization')[0]
      })
    );
  }

  // returns if app config consisting of all current active blocks is valid and ready to deploy
  get appConfigIsValid$():Observable<boolean>{
    return this._currentAppState$.pipe(
      map(state => {
        if(state){
          return state?.state.invalidConfiguration == ''
        }
        return false;
      }),
    )
  }
  get isAdminRoute$(){
    return this._isAdminRoute$.asObservable();
  }
  get appDeploying$(): Observable<boolean> {
    return this._appDeploying$.asObservable();
  }
  get appDeployed$(): Observable<boolean>{
    return this._currentAppState$.pipe(
      map(state => {
        if(state){
          return state.state.state === 'APP_DEPLOYED'
        }
        return false;
      })
    )
  }
  ngOnDestroy() {
    this._subs.unsubscribe();
  }

}
