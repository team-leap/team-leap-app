import { Injectable } from '@angular/core';

/**
 * central loading indicator service
 */
@Injectable({
  providedIn: 'root',
})
export class LoadIndicatorService {
  private counter = 0;
  private counter2 = 0;
  private _isVisible = false;
  private _miniSpinnerIsVisible = false;

  constructor() {
    //
  }

  public showMiniLoadIndicator(): void {
    this.counter++;
    if (this.counter > 0 && !this._isVisible) {
      this._miniSpinnerIsVisible = true;
    }
  }

  public hideMiniLoadIndicator(): void {
    this.counter--;
    if (this.counter === 0) {
      this._miniSpinnerIsVisible = false;
    }
  }

  public showLoadIndicator(): void {
    this.counter2++;
    if (this.counter2 > 0 && !this._isVisible) {
      this._isVisible = true;
    }
  }

  public hideLoadIndicator(): void {
    this.counter2--;
    if (this.counter2 === 0) {
      this._isVisible = false;
    }
  }

  get isVisible(): boolean {
    return this._isVisible;
  }

  get miniSpinnerIsVisible(): boolean {
    return this._miniSpinnerIsVisible;
  }
}
