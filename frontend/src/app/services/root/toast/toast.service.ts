import {Injectable, OnDestroy} from '@angular/core';
import {ToastrService} from "ngx-toastr";
import {TranslocoService} from "@ngneat/transloco";
import {ToastConfig} from "./toast-config";
import {Subscription} from "rxjs";

@Injectable({
  providedIn: 'root'
})
export class ToastService {

  private _subs = new Subscription();
  private _warningCount = 0;
  constructor(
    private readonly toastrService: ToastrService,
    private readonly translocoService: TranslocoService
  ) { }


  public showSuccessToast(translationKey: string){
    this._subs.add(this.translocoService.selectTranslate(translationKey).subscribe(
      translation => {
        this.toastrService.success(translation,'Success',ToastConfig);
        this._subs.unsubscribe()
      }
    ))
  }

  public showErrorToast(translationKey: string){
    this._subs.add(this.translocoService.selectTranslate(translationKey).subscribe(
      translation => {
        this.toastrService.error(translation,'Error',ToastConfig);
        this._subs.unsubscribe()
      }
    ))
  }

  public showWarningToast(translationKey: string){
    if(this._warningCount === 0){
      this._warningCount++;
      this._subs.add(this.translocoService.selectTranslate(translationKey).subscribe(
        translation => {
          this.toastrService.warning(translation,'Warning',ToastConfig);
          this._subs.unsubscribe();
          this._warningCount = 0;
        }
      ))
    }

  }


}
