import 'zone.js';
import 'zone.js/dist/zone-testing';
import { TestBed } from '@angular/core/testing';
import { ToastService } from './toast.service';
import {BrowserDynamicTestingModule, platformBrowserDynamicTesting} from "@angular/platform-browser-dynamic/testing";
import {ToastrService} from "ngx-toastr";
import {TranslocoService} from "@ngneat/transloco";

describe('ToastService', () => {
  let service: ToastService;

  beforeEach(() => {
    TestBed.initTestEnvironment(
      BrowserDynamicTestingModule,
      platformBrowserDynamicTesting()
    );
    TestBed.configureTestingModule({
      providers: [
        {provide: ToastrService, useValue: {}},
        {provide: TranslocoService, useValue: {}}
      ]
    });
    service = TestBed.inject(ToastService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
