﻿import {IndividualConfig} from "ngx-toastr";

export const ToastConfig: Partial<IndividualConfig> = {
  progressAnimation: undefined,
  progressBar: true,
  tapToDismiss: false,
  timeOut: 5000,
  closeButton: true,
  positionClass: 'toast-bottom-center',

}
