import 'zone.js';
import 'zone.js/dist/zone-testing';
import { TestBed } from '@angular/core/testing';
import { LoggerService } from './logger.service';
import {NGXLogger} from "ngx-logger";
import {BrowserDynamicTestingModule, platformBrowserDynamicTesting} from "@angular/platform-browser-dynamic/testing";

describe('LoggerService', () => {
  let service: LoggerService;

  beforeEach(() => {
    TestBed.initTestEnvironment(
      BrowserDynamicTestingModule,
      platformBrowserDynamicTesting()
    );
    TestBed.configureTestingModule({
      providers:[
        {provide: NGXLogger, useValue: {}}
      ]
    });
    service = TestBed.inject(LoggerService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
