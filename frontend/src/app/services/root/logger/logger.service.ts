import { Injectable } from '@angular/core';
import {NGXLogger} from "ngx-logger";

@Injectable({
  providedIn: 'root'
})
export class LoggerService {

  constructor(private readonly logger: NGXLogger) { }

  public standardLogger (functionName: string ,message?: string , data?: any){
    const logData = data ? data : '';
    this.logger.debug(`${functionName} - ${message}`,`${logData ? '-' : ''}`, logData);
  }
}
