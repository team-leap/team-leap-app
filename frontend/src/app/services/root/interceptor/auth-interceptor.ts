import {HttpInterceptorFn} from "@angular/common/http";
import {tap} from "rxjs";
import {inject} from "@angular/core";
import {SecurityService} from "../../apps/security/security.service";
import {AuthService} from "../../monitor/auth/auth.service";

export const AuthInterceptor: HttpInterceptorFn = (req, next) => {
  if (req.url.includes('monitor')) {
    const auth = inject(AuthService);
    const adminToken = auth.getAdminToken();
    const headers = req.headers.set('Authorization', adminToken);
    req = req.clone({
      headers
    });
  } else if (req.url.includes('app')) {
    const auth = inject(SecurityService);
    const securityToken = auth.getSecurityToken();
    const headers = req.headers.set('Authorization', securityToken);
    req = req.clone({
      headers
    });
  }
  return next(req).pipe(
    tap(() => {
    }),
  );
}
