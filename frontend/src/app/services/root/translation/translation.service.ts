import { Injectable } from '@angular/core';
import {TranslocoService} from "@ngneat/transloco";
import {Subscription} from "rxjs";

@Injectable({
  providedIn: 'root'
})
export class TranslationService {

  private _subs = new Subscription();
  constructor(private readonly translocoService:TranslocoService) { }

  public setLang(lang:string){
    window.localStorage.setItem('lang',lang);
    this.translocoService.setActiveLang(lang);
    location.reload()
  }

  public switchLang(){
    const currentLang = this.getLang();
    const newLang = currentLang === 'en' ? 'de' : 'en';
    this.setLang(newLang);
  }

  public getLang(){
    const lang = window.localStorage.getItem('lang');
    if(lang != null){
      return lang;
    }
    return 'de'
  }

  public async translate(key: string, params?: any){
    return new Promise<string>(resolve => {
      this._subs.add(this.translocoService.selectTranslate(key,params ? params : undefined).subscribe(
        res => {
          resolve(res);
          this._subs.unsubscribe();
        }
      ))
    })
  }

  public translateSync(key: string, params?: any) {
    return this.translocoService.translate(key, params ? params : undefined);
  }
}
