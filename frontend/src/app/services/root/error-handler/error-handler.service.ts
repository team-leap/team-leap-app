import { Injectable } from '@angular/core';
import {HttpErrorResponse} from "@angular/common/http";
import {ToastService} from "../toast/toast.service";

@Injectable({
  providedIn: 'root'
})
export class ErrorHandlerService {

  constructor(private readonly toastService: ToastService) { }

  public handleError(
    response:any,
    source: string
  ):void{
    if(response instanceof HttpErrorResponse){
      if(response.error instanceof ErrorEvent){
        this.toastService.showErrorToast('error.clientSideError')
      }else{
        this.toastService.showErrorToast('error.serverSideError')
      }
    }
  }

}
