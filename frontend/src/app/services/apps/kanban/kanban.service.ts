import {Injectable} from '@angular/core';
import {BehaviorSubject, catchError, Observable, of} from "rxjs";
import {HttpClient, HttpErrorResponse} from "@angular/common/http";
import {environment} from "../../../../environments/environment";
import {ToastService} from "../../root/toast/toast.service";
import {LoggerService} from "../../root/logger/logger.service";
import {ErrorHandlerService} from "../../root/error-handler/error-handler.service";
import {SecurityService} from "../security/security.service";

export interface KanbanEntry {
  id: number;
  title: string;
  description: string;
  status: number;
  priority: number;
  user_name: string;
  created_timestamp: string;
}

interface GeneralResponse {
  success: boolean;
}

interface ResponseKanbanList extends GeneralResponse {
  kanbanRows: KanbanEntry[];
}

@Injectable({
  providedIn: 'root'
})
export class KanbanService {

  private _kanbanElements$ = new BehaviorSubject<KanbanEntry[]>([]);
  private _todoElements$ = new BehaviorSubject<KanbanEntry[]>([]);
  private _inProgressElements$ = new BehaviorSubject<KanbanEntry[]>([]);
  private _doneElements$ = new BehaviorSubject<KanbanEntry[]>([]);

  constructor(
    private http: HttpClient,
    private readonly securityService: SecurityService,
    private readonly toastService: ToastService,
    private readonly logger: LoggerService,
    private readonly errorHandlerService: ErrorHandlerService
  ) {
    this._kanbanElements$.subscribe((data: KanbanEntry[]) => {
      this._todoElements$.next(data.filter(row => row.status === 0));
      this._inProgressElements$.next(data.filter(row => row.status === 1));
      this._doneElements$.next(data.filter(row => row.status === 2));
    });
  }

  createKanbanRow(row: Partial<KanbanEntry>, callback?: () => void) {
    return this.http.post<any>(`${environment.backendUrl}/app/kanban/create`, row, {})
      .pipe(
        catchError((error:HttpErrorResponse) => {
          return of(error)
        })
      )
      .subscribe(data => {
        if(data instanceof HttpErrorResponse){
          this.errorHandlerService.handleError(data, 'KanbanService.createKanbanRow')
        }else{
          if(data.success){
            this.logger.standardLogger('KanbanService.deleteKanbanRow', 'method called successfully')
            this.listKanbanRows();
            if (callback) {
              callback();
            }
          }
        }
      });
  }

  listKanbanRows() {
    this.logger.standardLogger('KanbanService.listKanbanRows', 'method called ...')
    this.http.get<ResponseKanbanList>(`${environment.backendUrl}/app/kanban/list`, {})
      .pipe(
        catchError((error: HttpErrorResponse) => {
          return of(error)
        })
      )
      .subscribe(data => {
        if(data instanceof HttpErrorResponse){
          this.errorHandlerService.handleError(data, 'KanbanService.listKanbanRows')
        }else{
          if(data.kanbanRows){
            this.logger.standardLogger('KanbanService.listKanbanRows', 'method called successfullly',data)
            this._kanbanElements$.next(data.kanbanRows);
          }
        }
      });
  }

  updateKanbanRow(id: number, updatedRow: KanbanEntry) {
    this.logger.standardLogger('KanbanService.updateKanbanRow', 'method called ...')
    this.http.post<any>(`${environment.backendUrl}/app/kanban/update/${id}`, updatedRow, {})
      .pipe(
        catchError((error:HttpErrorResponse) => {
          return of(error)
        })
      )
      .subscribe(data => {
        if(data instanceof HttpErrorResponse){
          this.errorHandlerService.handleError(data, 'KanbanService.updateKanbanRow')
        }else{
          if (data.success) {
            this.logger.standardLogger('KanbanService.deleteKanbanRow', 'method called successfully')
            this.listKanbanRows();
          } else {
            this.toastService.showErrorToast("kanban.messages.couldNotUpdate");
          }
        }

      }
    );
  }

  deleteKanbanRow(id: number) {
    this.logger.standardLogger('KanbanService.deleteKanbanRow', 'method called ...')
    this.http.get<any>(`${environment.backendUrl}/app/kanban/delete/${id}`, {})
      .pipe(catchError((error:HttpErrorResponse) => {
        return of(error)
      }))
      .subscribe(data => {
        if(data instanceof HttpErrorResponse){
          this.errorHandlerService.handleError(data, 'KanbanService.deleteKanbanRow')
        }else{
          if(data.success){
            this.listKanbanRows();
            this.logger.standardLogger('KanbanService.deleteKanbanRow', 'method called successfully')
          } else {
            this.toastService.showErrorToast("kanban.messages.couldNotDelete");
          }
        }
      }
    )
  }

  get todoElements$(): Observable<KanbanEntry[]> {
    return this._todoElements$.asObservable();
  }

  get inProgressElements$(): Observable<KanbanEntry[]> {
    return this._inProgressElements$.asObservable();
  }

  get doneElements$(): Observable<KanbanEntry[]> {
    return this._doneElements$.asObservable();
  }

  getTaskById(id: number) {
    return this._kanbanElements$.getValue().find(row => row.id === id);
  }
}
