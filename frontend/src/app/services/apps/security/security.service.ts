import {Injectable} from '@angular/core';
import {HttpClient, HttpErrorResponse} from "@angular/common/http";
import {LoggerService} from "../../root/logger/logger.service";
import {environment} from "../../../../environments/environment";
import {catchError, of} from "rxjs";
import {ToastService} from "../../root/toast/toast.service";
import {ErrorHandlerService} from "../../root/error-handler/error-handler.service";

@Injectable({
  providedIn: 'root'
})
export class SecurityService {

  constructor(
    private readonly http: HttpClient,
    private readonly logger: LoggerService,
    private readonly errorHandlerService: ErrorHandlerService,
    private readonly toastService: ToastService
  ) {
  }

  public createUser(username: string, password: string, callback: (success: boolean) => void) {
    this.logger.standardLogger('SecurityBlockAuthService.createUser', 'method called ...')
    this.http.post<{ success: boolean }>(`${environment.backendUrl}/auth/user/create`, JSON.stringify({
      name: username,
      password: password,
    }))
      .pipe(
        catchError((error: HttpErrorResponse) => {
          callback(false)
          return of(error)
        })
      )
      .subscribe(res => {
          if (res instanceof HttpErrorResponse) {
            this.errorHandlerService.handleError(res, 'SecurityBlockAuthService.createUser')
            callback(false)
          } else {
            if (res.success) {
              this.logger.standardLogger('SecurityBlockAuthService.createUser', 'method called successfully', res)
              this.toastService.showSuccessToast('success.createUser')
              callback(true)
            } else {
              this.toastService.showErrorToast('error.createUser');
              this.logger.standardLogger('SecurityBlockAuthService.createUser', 'method call failed', res)
              callback(false)
            }
          }
        }
      )
  }

  public login(username: string, password: string) {
    this.logger.standardLogger('SecurityBlockAuthService.login', 'method called ...')
    return new Promise<void>(resolve => {
      this.http.post<{
        success: boolean,
        token: string
      }>(`${environment.backendUrl}/auth/user/token/create`, JSON.stringify({
        name: username,
        password: password
      }))
        .pipe(
          catchError((error: HttpErrorResponse) => {
            return of(error)
          })
        )
        .subscribe((res: any) => {
            if (res instanceof HttpErrorResponse) {
              this.errorHandlerService.handleError(res, 'SecurityBlockAuthService.login')
              resolve()
            } else {
              if (res.success) {
                this.logger.standardLogger('SecurityBlockAuthService.login', 'method called successfully', res)
                const appId = location.pathname.split('/')[2]
                if (appId) {
                  window.localStorage.setItem(`securityToken`, res.token);
                  window.localStorage.setItem('user', username);
                }
              } else {
                this.toastService.showWarningToast('warnings.notAuthenticated')
                this.logger.standardLogger('SecurityBlockAuthService.login', 'method call failed', res)
              }
              resolve()
            }
          }
        )
    })
  }

  public isAuthenticated(username: string | null, securityToken: string | null): Promise<boolean> {
    this.logger.standardLogger('SecurityAuthBlockService.isAuthenticated', 'method called ...')
    return new Promise<boolean>(resolve => {
      if (!securityToken || !username) {
        resolve(false)
      }
      this.http.post<{ success: boolean }>(`${environment.backendUrl}/auth/user/token/validate`, JSON.stringify({
        name: username,
        token: securityToken
      }))
        .pipe(
          catchError((error: HttpErrorResponse) => {
            return of(error)
          })
        )
        .subscribe(res => {
            if (res instanceof HttpErrorResponse) {
              this.errorHandlerService.handleError(res, 'SecurityBlockAuthService.isAuthenticated')
              resolve(false)
            } else {
              this.logger.standardLogger('SecurityAuthBlockService.isAuthenticated', 'method called successfully', res)
              resolve(res.success)
            }
          }
        )
    })
  }

  public changePassword(username: string, password: string, newPassword: string) {
    this.http.post(`${environment.backendUrl}/auth/user/changepw`, JSON.stringify({
      name: username,
      password: password,
      newPassword: newPassword
    }))
      .pipe(
        catchError((error: HttpErrorResponse) => {
          return of(error)
        })
      )
      // TODO add type to res
      .subscribe((res: any) => {
          if (res instanceof HttpErrorResponse) {
            // TODO handle error
          } else {
            if (res.success) {
              // TODO IMPLEMENT
              //window.localStorage.setItem()
              // TODO maybe success toast msg or something...
            } else {
              // TODO server error
            }
          }
        }
      )
  }

  public logout() {
    window.localStorage.removeItem('user');
    window.localStorage.removeItem('securityToken');
  }

  public hasSecurity() {
    return this.isSecurityEnabled() && window.localStorage.getItem('securityToken');
  }

  public getSecurityToken() {
    if (!this.isSecurityEnabled()) {
      return '825665aa-8018-44ed-bf8d-027cb83a0a35';
    }
    const securityToken = window.localStorage.getItem('securityToken');
    if (securityToken) {
      return securityToken;
    }
    return '825665aa-8018-44ed-bf8d-027cb83a0a35'
  }

  /**
   * Checks if the security feature of the application is enabled or not.
   * This function analyses the application's URL and makes the decision based on the presence and structure
   * of the /app/D/D/D/6 pattern, where D is a digit. Specifically, it checks if:
   * 1. The URL contains the substring 'app'
   * 2. The three parts of the URL following 'app' can be converted to numbers
   * 3. The fourth part following 'app' is '6'
   * If these conditions are met, it considers the security feature to be disabled, i.e. it returns 'false'.
   * If any of these conditions fails, it considers the security feature to be enabled and returns 'true'.
   *
   * @returns {boolean} - returns 'false' if the URL contains the /app/D/D/D/6 pattern and all D can be
   *                      converted to a number, otherwise returns 'true'.
   */
  public isSecurityEnabled() {
    const path = location.pathname.split('/');
    const appIndex = path.findIndex(p => p === 'app');
    if (appIndex === -1 || appIndex > path.length - 5) {
      return true;
    }

    // check if the next three parts are digits and the last part is '-1'
    const appPath = path.slice(appIndex + 1, appIndex + 5);
    return appPath.length < 4 ||
      isNaN(Number(appPath[0])) ||
      isNaN(Number(appPath[1])) ||
      isNaN(Number(appPath[2])) ||
      appPath[3] === '6';
  }

}
