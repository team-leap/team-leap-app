import { Injectable } from '@angular/core';
import {webSocket} from "rxjs/webSocket";
import {environment} from "../../../../environments/environment";
import {WebSocketSubject} from "rxjs/internal/observable/dom/WebSocketSubject";
import {LoggerService} from "../../root/logger/logger.service";
import {SecurityService} from "../security/security.service";
import {Subscription} from "rxjs";

@Injectable({
  providedIn: 'root'
})
export class WebsocketService {
  private _appsWebSocket$: WebSocketSubject<any> | undefined;
  private _subs = new Subscription();

  constructor(
    private readonly logger:LoggerService,
    private readonly securityService: SecurityService,
  ) { }

  public registerUserToWebsocketChannel(callback: () => void = () => {}) {
    this.logger.standardLogger('PlanningService.registerUserToWebsocketChannel', 'method called')
    if (this._appsWebSocket$) {
      return;
    }
    this._appsWebSocket$ = webSocket(environment.websocketUrl);
    const securityToken = this.securityService.getSecurityToken();
    this._appsWebSocket$.next({
      "action": "register",
      "userToken": securityToken
    })
    this._subs.add(
      this._appsWebSocket$
        .subscribe(message => {
          this.logger.standardLogger('AppStateService.registerUserToWebsocketChannel', 'received websocket message',message)
          callback()
        }))
    this.logger.standardLogger('PlanningService.registerUserToWebsocketChannel', 'connected successfully')
  }
}
