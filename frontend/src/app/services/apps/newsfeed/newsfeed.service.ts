import {Injectable} from '@angular/core';
import {HttpClient, HttpErrorResponse} from "@angular/common/http";
import {BehaviorSubject, catchError, finalize, map, of} from "rxjs";
import {environment} from "../../../../environments/environment";
import {ErrorHandlerService} from "../../root/error-handler/error-handler.service";
import {LoggerService} from "../../root/logger/logger.service";
import {LoadIndicatorService} from "../../root/load-indicator/load-indicator.service";

export interface Article {
  link: string,
  description: string,
  thumbnail: string,
  title: string,
  pubDate: string
}
@Injectable({
  providedIn: 'root'
})
export class NewsfeedService {

  private _newsProviders = new BehaviorSubject<string[]>([]);
  private newsByProvider: { [key: string]: Article[] } = {};

  constructor(
    private readonly http: HttpClient,
    private readonly errorHandlerService: ErrorHandlerService,
    private readonly logger: LoggerService,
    private readonly loadIndicatorService: LoadIndicatorService
  ) {
    this._newsProviders.subscribe(newsProviders => {
        newsProviders.forEach((newsProvider: string) => {
          this.retrieveNews(newsProvider);
        })
      }
    )
  }

  retrieveProviders() {
    this.logger.standardLogger('NewsfeedService.retrieveProviders','method called ...')
    this.http.get<any>(`${environment.backendUrl}/app/news/list`)
      .pipe(
        map(res => {
          return {
            success: res.success,
            newsProviders: Object.values(res.newsProviders).reduce((acc: any, value) => acc.concat(value), [])
          };
        }),
        catchError(
          (error: HttpErrorResponse) => {
            return of(error)
          }
        ),
      )
      .subscribe(res => {
          if (res instanceof HttpErrorResponse) {
            this.errorHandlerService.handleError(res, 'NewsfeedService.retrieveProviders')
          } else {
            if (res.success) {
              const providers = res.newsProviders as string[];
              this._newsProviders.next(providers);
              this.logger.standardLogger('NewsfeedService.retrieveProviders','method called successfully', providers)
            }
          }
      })
  }

  retrieveNews(providerId: string = 'tagesschau') {
    this.logger.standardLogger('NewsfeedService.retrieveNews','method called ... for', providerId)
    this.loadIndicatorService.showLoadIndicator();
    this.http.get(`${environment.backendUrl}/app/news/list/${providerId}`)
      .pipe(
        catchError(error => {
          return of(error);
        }),
        finalize(() => {
          this.loadIndicatorService.hideLoadIndicator();
        })
      )
      .subscribe(res => {
          if (res instanceof HttpErrorResponse) {
            this.errorHandlerService.handleError(res, 'NewsfeedService.retrieveNews');
          } else {
            if (res.success) {
              res = res as Article[];
              this.newsByProvider[providerId] = res.newsList;
              this.logger.standardLogger('NewsfeedService.retrieveNews','method called successfully', res)
            }
          }
      });
  }

  get newsProvider$() {
    return this._newsProviders.asObservable();
  }

  filterNewsByProvider$(providerId: string) {
    return this._newsProviders.asObservable().pipe(
      map(newsProviders => {
        return this.newsByProvider[providerId] || [];
      })
    )
  }
}
