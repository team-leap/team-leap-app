import {Injectable} from '@angular/core';
import {environment} from "../../../../environments/environment";
import {BehaviorSubject, catchError, finalize, map, Observable, of, tap} from "rxjs";
import {HttpClient, HttpErrorResponse} from "@angular/common/http";
import {LoggerService} from "../../root/logger/logger.service";
import {LoadIndicatorService} from "../../root/load-indicator/load-indicator.service";
import {ErrorHandlerService} from "../../root/error-handler/error-handler.service";
import {SecurityService} from "../security/security.service";

@Injectable({
  providedIn: 'root'
})
export class TodoService {
  private _todosInProgress$ = new BehaviorSubject([]);
  private _todosDone$ = new BehaviorSubject([]);

  constructor(
    private http: HttpClient,
    private logger: LoggerService,
    private securityBlockAuthService: SecurityService,
    private loadIndicatorService: LoadIndicatorService,
    private readonly errorHandlerService: ErrorHandlerService
  ) {
  }


  public retrieveTodos() {
    this.logger.standardLogger('PlanningService.retrieveTodos','method called ...')
    this.http.get<any>(`${environment.backendUrl}/app/todo/list`)
      .pipe(
        map((res: any) => {
          return res.todos
        }),
        tap((todos: any) => {
          if (todos) {
            const done = todos.filter((todo: any) => todo.done === true);
            const inProgress = todos.filter((todo: any) => todo.done === false);
            this._todosDone$.next(done);
            this._todosInProgress$.next(inProgress);
          }
          this.logger.standardLogger('PlanningService.retrieveTodos','method called successfully', todos)
        }),
        catchError((error: HttpErrorResponse) => {
            return of(error)
          }
        ),
      )
      .subscribe(res => {
          if(res instanceof HttpErrorResponse){
            this.errorHandlerService.handleError(res,'PlanningService.retrieveTodos')
          }
        }
      )
  }

  public createTodo(text: string) {
    this.logger.standardLogger('PlanningService.createTodo','method called ...')
    this.http.post<any>(`${environment.backendUrl}/app/todo/create`, JSON.stringify({
      text: text
    }))
      .pipe(catchError((error:HttpErrorResponse) => {
        return of(error)
      }))
      .subscribe(res => {
        if(res instanceof HttpErrorResponse){
          this.errorHandlerService.handleError(res, 'PlanningService.createTodo')
        }else{
          if(res.success){
            this.retrieveTodos();
            this.logger.standardLogger('PlanningService.createTodo','method called successfully',res)
          }
        }
    })
  }

  public setTodoDone(id: number) {
    this.logger.standardLogger('PlanningService.setToDone','method called ...')
    this.http.post<any>(`${environment.backendUrl}/app/todo/update/${id}`, JSON.stringify({
      done: true
    }))
      .pipe(
        catchError((error: HttpErrorResponse) => {
          return of(error)
        })
      )
      .subscribe(res => {
        if(res instanceof HttpErrorResponse){
          this.errorHandlerService.handleError(res, 'PlanningService.createTodo')
        }else {
          if(res.success){
            this.retrieveTodos();
            this.logger.standardLogger('PlanningService.setToDone','method called successfully',res)
          }
        }
      });
  }

  public setTodoInProgress(id: number) {
    this.logger.standardLogger('PlanningService.setTodoInProgress','method called ...')
    this.http.post<any>(`${environment.backendUrl}/app/todo/update/${id}`, JSON.stringify({
      done: false
    }))
      .pipe(
        catchError((error: HttpErrorResponse) => {
          return of(error)
        })
      )
      .subscribe(res => {
        if(res instanceof HttpErrorResponse){
          this.errorHandlerService.handleError(res, 'PlanningService.setTodoInProgress')
        }else{
          if(res.success){
            this.retrieveTodos();
            this.logger.standardLogger('PlanningService.setTodoInProgress','method called successfully',res)
          }
        }
      });
  }

  public editTodo(id: number, text: string) {
    this.logger.standardLogger('PlanningService.editTodo','method called ...')
    this.http.post<any>(`${environment.backendUrl}/app/todo/update/${id}`, JSON.stringify({
      text: text,
    }))
      .pipe(
        catchError((error: HttpErrorResponse) => {
          return of(error)
        })
      )
      .subscribe(res => {
        if(res instanceof HttpErrorResponse){
          this.errorHandlerService.handleError(res, 'PlanningService.setTodoInProgress')
        }else{
          if(res.success){
            this.retrieveTodos();
            this.logger.standardLogger('PlanningService.editTodo','method called successfully',res)
          }
        }
    });
  }

  public deleteTodo(id: number) {
    this.logger.standardLogger('PlanningService.delete','method called ...')
    this.http.get<any>(`${environment.backendUrl}/app/todo/delete/${id}`)
      .pipe(
        catchError((error: HttpErrorResponse) => {
          return of(error)
        })
      )
      .subscribe(res => {
        if(res instanceof HttpErrorResponse){
          this.errorHandlerService.handleError(res, 'PlanningService.setTodoInProgress')
        }else{
          if(res.success){
            this.retrieveTodos();
            this.logger.standardLogger('PlanningService.delete','method called successfully',res)
          }
        }
      });
  }

  get todosInProgress$(): Observable<any> {
    return this._todosInProgress$.asObservable();
  }

  get todosDone$(): Observable<any> {
    return this._todosDone$.asObservable();
  }
}
