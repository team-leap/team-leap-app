﻿export type Customization = 'Security' | 'Conversational AI';
export type Implementation = 'Information' | 'Planning'
export type Context = 'Business' | 'Leisure'
export type Empty = ''

export type Block = Customization | Implementation | Context | Empty

export interface AppStateDTO {
  success: boolean
  state: AppState
}
export interface AppState {
  state: State
  activeBlocks: number[],
  successiveDeployButtonPresses: number // 0 | 1
  learningBlock: number // id | -1,
  learnedBlocks:number[],
  adminCard?: boolean,
  devices: Device,
  invalidConfiguration: string
}
export type State = 'INITIALIZING' | 'LEARNING' | 'BUILDING_WAITING' | 'BUILDING_RUNNING' | 'APP_DEPLOYED'
export type Category = 'implementation' | 'context' | 'customization' | 'none';
export interface AppConfig {
  id: number,
  implementation: number,
  context: number,
  addon: number
}
interface Device{
  raspberry_pi:{
    active: boolean
  }
}
export interface BuildingBlock {
  id: number,
  name: Block,
  nameTranslationKey: string,
  color: string,
  info: string,
  infoTitle: string,
  imgUrl: string,
  threeDUrl: string,
  iconUrl: string,
  category: Category,
  categoryTranslationKey: string,
}

