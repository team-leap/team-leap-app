﻿import {BuildingBlock} from "./model";

export const buildingBlocks: BuildingBlock[] = [
  {
    id: 1,
    name: 'Information',
    nameTranslationKey: "buildingBlocksName.information",
    color: 'rgb(0, 178, 0)',
    info: "buildingBlocks.information",
    infoTitle: "buildingBlocksTitle.information",
    imgUrl: "assets/images/rocket/rocket_baseA.png",
    threeDUrl: "assets/images/rocket/rocket_baseA_3d.png",
    iconUrl: "assets/images/information.png",
    category: 'implementation',
    categoryTranslationKey: "buildingBlocksCategory.implementation"
  },
  {
    id: 2,
    name: 'Planning',
    nameTranslationKey: "buildingBlocksName.planning",
    color: 'rgb(0, 120, 0)',
    info: "buildingBlocks.planning",
    infoTitle: "buildingBlocksTitle.planning",
    imgUrl: "assets/images/rocket/rocket_baseB.png",
    threeDUrl: "assets/images/rocket/rocket_baseB_3d.png",
    iconUrl: "assets/images/calendar.png",
    category: 'implementation',
    categoryTranslationKey: "buildingBlocksCategory.implementation"
  },
  {
    id: 3,
    name: 'Business',
    nameTranslationKey: "buildingBlocksName.business",
    color: 'rgb(0, 80, 0)',
    info: "buildingBlocks.business",
    infoTitle: "buildingBlocksTitle.business",
    imgUrl: "assets/images/rocket/rocket_fuelA.png",
    threeDUrl: "assets/images/rocket/rocket_fuelA_3d.png",
    iconUrl: "assets/images/stats.png",
    category: 'context',
    categoryTranslationKey: "buildingBlocksCategory.context"
  },

  {
    id: 4,
    name: 'Leisure',
    nameTranslationKey: "buildingBlocksName.leisure",
    color: 'rgb(110, 178, 0)',
    info: "buildingBlocks.leisure",
    infoTitle: "buildingBlocksTitle.leisure",
    imgUrl: "assets/images/rocket/rocket_fuelB.png",
    threeDUrl: "assets/images/rocket/rocket_fuelB_3d.png",
    iconUrl: "assets/images/bike.png",
    category: 'context',
    categoryTranslationKey: "buildingBlocksCategory.context"
  },
  {
    id: 5,
    name: 'Conversational AI',
    nameTranslationKey: "buildingBlocksName.ai",
    color: 'rgb(20, 100, 0)',
    info: "buildingBlocks.ai",
    infoTitle: "buildingBlocksTitle.ai",
    imgUrl: "assets/images/rocket/rocket_topA.png",
    threeDUrl: "assets/images/rocket/rocket_topA_3d.png",
    iconUrl: "assets/images/ai.png",
    category: 'customization',
    categoryTranslationKey: "buildingBlocksCategory.customization"
  },
  {
    id: 6,
    name: 'Security',
    nameTranslationKey: "buildingBlocksName.security",
    color: 'rgb(50, 178, 20)',
    info: "buildingBlocks.security",
    infoTitle: "buildingBlocksTitle.security",
    imgUrl: "assets/images/rocket/rocket_topB.png",
    threeDUrl: "assets/images/rocket/rocket_topB_3d.png",
    iconUrl: "assets/images/verified.png",
    category: 'customization',
    categoryTranslationKey: "buildingBlocksCategory.customization"
  }
]

function preLoadImages() {
  buildingBlocks.forEach((block) => {
    const img = new Image();
    img.src = block.imgUrl;
  })
}

preLoadImages();

export const emptyBlock: BuildingBlock = {
  id: -1,
  name: '',
  nameTranslationKey: '',
  color: '',
  info: "",
  infoTitle: "",
  imgUrl: "",
  threeDUrl: "",
  iconUrl: "",
  category: 'none',
  categoryTranslationKey: ""
}

