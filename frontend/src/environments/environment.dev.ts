export const environment = {
  prod: false,
  backendUrl: "https://rocket-deployer-backend-dev.cfapps.eu10-004.hana.ondemand.com",
  frontendUrl: "https://rocket-deployer-frontend-dev.cfapps.eu10-004.hana.ondemand.com",
  websocketUrl: "wss://rocket-deployer-backend-dev.cfapps.eu10-004.hana.ondemand.com/ws",
  deploymentTimer: 4500
}
