export const environment = {
  prod: true,
  backendUrl: "https://rocket-deployer-backend.cfapps.eu10-004.hana.ondemand.com",
  frontendUrl: "https://rocket-deployer-frontend.cfapps.eu10-004.hana.ondemand.com",
  websocketUrl: "wss://rocket-deployer-backend.cfapps.eu10-004.hana.ondemand.com/ws",
  deploymentTimer: 4500
}
