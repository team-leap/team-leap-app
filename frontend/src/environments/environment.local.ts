export const environment = {
  prod: false,
  backendUrl: "http://localhost:8080",
  frontendUrl: "http://localhost:4200",
  websocketUrl: "ws://localhost:8080/ws",
  deploymentTimer: 4500
}
