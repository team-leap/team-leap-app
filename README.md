# [Rocket Deployer by Team leap](https://gitlab.com/team-leap/team-leap-app)

Der Rocket Deployer ist ein Web-Applikations-Baukasten, der im Rahmen des Semesterprojekts an der Hochschule Mannheim
entstanden ist. Er läuft auf der SAP Business Technology Platform (SAP BTP) und ist in der Lage, verschiedene
Web-Applikationen zu hosten und zu verwalten.

Die nachfolgenden Dokumente beschreiben, wie man die Applikation baut, deployt, konfiguriert und nutzt. Die Dokumente
sind auf Englisch verfasst, da diese Sprache in der IT-Branche üblich ist.

- [Set up the BTP](doc/btp.md)
- [Build and Deploy](doc/build_and_deploy.md)
- [Using the application](doc/use_application.md)
- [Raspberry Pi Setup](doc/raspberry_pi_setup.md)

A project created by [Team Leap](https://team-leap.gitlab.io/team-leap-pages/)

### Frontend Fiori Resources

- https://www.npmjs.com/package/@ui5/webcomponents-fiori
- https://www.npmjs.com/package/@ui5/webcomponents
- https://sap.github.io/ui5-webcomponents/playground/components

