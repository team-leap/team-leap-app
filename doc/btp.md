# Setting up and managing the SAP BTP

This chapter will cover how to set up your SAP BTP space to use this application.

## Services

Navigating to the SAP BTP cockpit and select `Services` on your space.

We will need at least a production PostgreSQL database, optionally a development one on top of that.

To create them, go to the `Services Marketplace` and select `PostgreSQL`. In the [manifest.yml](../manifest.yml) file,
the IDs are:

- `rocket-deployer-postgresql-db`
- `rocket-deployer-postgresql-db-dev`

Which will look like this after creation:

![PostgreSQL services](img/PostgreSQL_service.png)

If, after pushing the backend application, the service bindings are not automatically registered, create one manually
for each service.

![Service bindings](img/PostgreSQL_binding.png)
