# Raspberry PI Related

## Prerequisites

- Any Raspberry Pi
- Python3 Module
    - MFRC522/SimpleMFRC522
    - RPi.GPIO

## Internet

To connect to the internet, you need to know the local IP of the Raspberry Pi. Once you are connected via SSH, you can
look for available WAPs with:

```
sudo iwlist wlan0 scan | grep ESSID
```

Once you know the `SSID` of the WAP that you want to use, you can add the following lines in the `wpa_supplicant.conf`
file, which you can find at:

```
sudo nano /etc/wpa_supplicant/wpa_supplicant.conf
```

the following lines:

```conf
network={
    ssid="YOUR_NETWORK_NAME"
    psk="YOUR_PASSWORD"
}
```

## Configuration

The file `config.json` controls what backend the Raspberry Pi is connecting to and which ports are used for the different
components.

```json
{
  "server_url": "example.com",
  "board_pins": {
    "kontext": 5,
    "implementation": 6,
    "addon": 13
  },
  "button_pin": 17,
  "key_pin": 22,
  "status_pin": 27
}
```

The Python Script can be run from any Raspberry Pi. `server_url` is your
backend endpoint. We are using the `GPIO.BCM` Mode, which means that you are
referring to the pins by the "Broadcom SOC channel" number, for example,
`GPIO.17,GPIO.22, etc`. In `board_pins`, you will specify the pins that you
want to use for the RFID reader. `button_pin` is the launch button.
`key_pin` is the lock mechanism. `status_pin` is the pin that shows a signal
of internet connectivity.

## RFID Reader

The MFRC522 is using the SPI Interface to read and write. But the Raspberry
Pi only has one SPI Interface. To let multiple MFRC522 read, the reset pin of
each MFRC522 is used to close and reinitialize the different MFRC522 reader.

### GPIO.BOARD

- SDA connects to Pin 24.
- SCK connects to Pin 23.
- MOSI connects to Pin 19.
- MISO connects to Pin 21.
- GND connects to Pin 6.
- RST connects to Pin 29,31,33.
- 3.3v connects to Pin 1.

![Service bindings](img/RFID-GPIO-Connection.webp)

![Service bindings](img/fritzingraspy.png)
