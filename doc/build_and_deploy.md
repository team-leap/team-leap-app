# Build and Deploy

The Rocket Deployer is split into two applications: the Java Spring Boot Backend and the Angular Frontend. This page
describes how to build and deploy both to the SAP BTP.

## Prerequisites

- [Maven](https://maven.apache.org/)
- [Node.js](https://nodejs.org/en/)
- Angular CLI: `npm i -g @angular/cli`
- [Cloud Foundry CLI](https://docs.cloudfoundry.org/cf-cli/install-go-cli.html)

For development:

- [PostgreSQL](https://www.postgresql.org/download/)

## Build

### Backend

Build using maven in the `backend` directory. The `admin-cards` profile will be explained in more detail in the backend
POM section below.

Select the `local`, `dev` or `prod` profile to correctly set up the endpoints and other data:

```bash
mvn clean package -Padmin-cards,local
mvn clean package -Padmin-cards,dev
mvn clean package -Padmin-cards,prod
```

### Frontend

Build using Angular CLI in the `frontend` directory.

Select the `build-development` or `build` profile to switch between development and production builds.

```bash
npm run build-development
npm run build
```

## Deploy

Make sure to be logged into your cloud foundry account using `cf login --sso` and run in the repository root directory:

```bash
cf push rocket-deployer-backend
cf push rocket-deployer-frontend
```

Or to the development environment:

```bash
cf push rocket-deployer-backend-dev
cf push rocket-deployer-frontend-dev
```

This will use the [manifest.yml](../manifest.yml) file to deploy the applications. The manifest contains all necessary
information to deploy the applications to the SAP BTP.

You can find more information on how the application is built and deployed in the [Gitlab CI file](../.gitlab-ci.yml).

## Java Backend POM

The [Java Backend POM](../backend/pom.xml) is the main configuration file for the Java backend. It contains all
dependencies and build configurations needed to build the backend.

### Admin Cards

Inside your Maven `settings.xml` (`C:\Users\<user>\.m2` on windows) add the following, where `1234, 5678, ...` is a
comma-separated list of admin card IDs:

```xml

<settings>
    <profiles>
        <profile>
            <id>admin-cards</id>
            <properties>
                <app.admincards>1234, 5678, ...</admin-cards.client-id>
            </properties>
        </profile>
    </profiles>
</settings>
```

When building your app, add the profile `admin-cards` using the `-P` profile flag. You can also add the parameter as
flag when building the application: `-Dapp.admincards="$ADMIN_CARDS"`

The reason this is done in the settings.xml instead of the POM is that the settings.xml is not committed to the
repository, so the admin card IDs are not public. If you want to do this for testing purposes, you can of course also
add the property to the POM directly:

```xml

<app.admincards></app.admincards>
<app.admincards.addon></app.admincards.addon>
```

## Local Development Setup

The backend of the project uses a few mechanisms to automatically detect whether it is running in a local development
environment or in the cloud. If you are running it locally, make sure that the `app.database.local.uri` property is set
in the POM to a value like this: `jdbc:postgresql://localhost:5432/postgres?user=postgres&amp;password=postgres`. This
will use a local database, instead of trying to connect to the PostgreSQL instance on the SAP BTP.

Then, run the backend using the `local` profile:

```bash
mvn spring-boot:run -Plocal
```

The frontend can be run using the `start` script in the `frontend` directory:

```bash
npm run start
```

The frontend application will then be available on the `http://localhost:4200` address.

In case you do not have the Rocket Deployer hardware ready, you can use the custom development UI to simulate the
requests: [endpoint_caller.html](../backend/src/test/resources/custom/endpoint_caller.html). It contains several buttons
to insert or remove cards and more, such as inserting the admin card or simulating the heartbeat from the Raspberry Pi
to remove the warning banner.
