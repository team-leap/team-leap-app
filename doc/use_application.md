# Using the application

After [setting up the application](build_and_deploy.md) and accessing the frontend UI, you will be greeted with the
admin panel.

![Admin Panel](img/admin_panel_base.png)

## Registering a monitor

There, you will first have to register the monitor. This is done to prevent other users from accessing the
monitor-specific endpoints like building or learning mode.

To do this, place an RFID tag that's ID is registered as admin card in the Maven POM file and click the topmost button,
which should become active after a few seconds. After that, remove the card. The monitor is now registered and you can
start interacting with the rest of the application.

> Note that this is an action that can only be performed once per backend application start, to assert that the monitor
> at the booth is the only one that is active, no matter what. To register a different monitor, you will have to restart
> the backend application.

## Learning mode

To enter learning mode, use the second button from the top. This will redirect you to a second screen, where you will be
prompted to confirm your decision to enter learning mode. This is, because the mode cannot be cancelled once it has been
started.

The application will prompt you to insert one card after another. Make sure to insert - remove - insert the card, as the
application will only read the next card after the previous one has been removed to prevent accidental double-reads.

After you have inserted all cards, the application will redirect you to building mode.

## Building mode

This is the main mode of the application, that the user will interact with. This screen mostly explains itself, so there
will not be much explanation here.
